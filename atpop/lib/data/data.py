# Setup logging
from ..log_func import logger


########################################
import pandas as pd
import portalocker
import os
import time
import random


################################################################################
class Data():
    ####################################
    def __init__( self
                , setup
                , path = '.'
                ):
        self.setup = setup
        self.path  = path


    ####################################
    def Is_Empty( self ):
        return self.df.empty


    ####################################
    def Get_CSV_File_Name( self ):
        return self.setup.ID + ".csv"


    ####################################
    def Get_CSV_File_Path( self ):
        return os.path.join( self.path
                           , self.Get_CSV_File_Name()
                           )


    ####################################
    def Read_CSV( self
                , file_name = None
                ):
        file_path = file_name
        if file_name is None:
            file_path = self.Get_CSV_File_Path()

        while True:
            try:
                logger.debug( "Trying to lock data file %s"
                            , file_path
                            )
                with portalocker.Lock( file_path
                                     , mode    = 'r'
                                     , timeout = 10
                                     ) as fh:
                    logger.debug( "Locked data file, starting to read %s"
                                , file_path
                                )
                    self.df = pd.read_csv( fh )
                    logger.debug( "Finished reading data file %s"
                                , file_path
                                )
                logger.debug( "Unlocked data file %s"
                            , file_path
                            )
                break
            except portalocker.exceptions.LockException:
                logger.debug( "Data file already locked, retrying later %s"
                            , file_path
                            )
                time.sleep( random.random() )


    ####################################
    def To_CSV( self
              , file_name = None
              ):
        os.makedirs( os.path.abspath(self.path)
                   , exist_ok = True
                   )

        file_path = file_name
        if file_name is None:
            file_path = self.Get_CSV_File_Path()

        while True:
            try:
                logger.debug( "Trying to lock data file %s"
                            , file_path
                            )
                with portalocker.Lock( file_path
                                     , mode    = 'w'
                                     , timeout = 10
#                                     , newline = ''
                                     ) as fh:
                    logger.debug( "Locked data file %s, writing"
                                , file_path
                                )
                    self.df.to_csv( fh
                                  , index = False
                                  )
                    # flush and sync to filesystem
                    logger.debug( "Writing data file %s finished, flushing started"
                                , file_path
                                )
                    fh.flush()
                    logger.debug( "Flushing data file %s finished, fsync started"
                                , file_path
                                )
                    os.fsync( fh.fileno() )
                    logger.debug( "Fsync data file %s finished"
                                , file_path
                                )
                logger.debug( "Unlocked data file %s"
                            , file_path
                            )
                break
            except portalocker.exceptions.LockException:
                logger.debug( "Data file %s already locked, retrying later"
                            , file_path
                            )
                time.sleep( random.random() )
