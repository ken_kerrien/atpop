from .exchange.interface       import Exchange_Interface
from .utils.names              import QUOTE_NAME,\
                                      BASE_NAME ,\
                                      FROM_NAME ,\
                                      TO_NAME


################################################################################
class Exchanger_Base():
    qn = QUOTE_NAME
    bn = BASE_NAME
    bvn = bn + 'Volume'
    qvn = qn + 'Volume'
    fn = FROM_NAME
    tn = TO_NAME


    ############################################################################
    def __init__( self
                , **kwargs
                ):
        self.Exchange_Setup       = kwargs["Exchange_Setup"]
        self.DEBUG_ARTIFACTS_PATH = kwargs["DEBUG_ARTIFACTS_PATH"]

        ###################################
        self.ex = None

        if not self.Exchange_Setup is None:
            self.ex = Exchange_Interface( self.Exchange_Setup.name
                                        , keys            = self.Exchange_Setup.keys
                                        , save_debug_data = self._Get_Param_True('debug_exchange_interface')
                                        , debug_path      = self.DEBUG_ARTIFACTS_PATH
                                        )


    ############################################################################
    def _Get_Param_True(self, attr):
        return hasattr(self.Exchange_Setup, attr) and getattr(self.Exchange_Setup, attr) == True
