from .data import Data
import pandas as pd


########################################
# Portfolio value fraction
class Portfolio_Frac_Setup():
    ID            = "portfolio_frac"

    time_name     = "timestamp"


################################################################################
class Portfolio_Frac_Data( Data ):
    ####################################
    def __init__( self
                , Setup = Portfolio_Frac_Setup
                , path  = '.'
                ):
        self.df = pd.DataFrame( columns = [ Setup.time_name ] )

        self.setup = Setup
        self.path  = path


    ####################################
    def Get_Latest_Entry( self ):
        if self.Is_Empty():
            return None

        d = self.df.drop( columns = [ self.setup.time_name ]
                        ).iloc[-1].to_dict()
        s = self.df[ self.setup.strategy_name ].iloc[-1]

        return { key: float(val) for key, val in d.items() }, s


    ####################################
    def Add_Entry( self
                 , portfolio_frac_dict
                 , timestamp
                 ):
# So, this is not the right approach, because there is value
# in an empty portfolio. It tells us that we should not own
# anything. And there is a value in restating the same portfolio
# multiple times, because underlying asset values may have changed,
# so adjusting their values make sense too.
#        if len( portfolio_frac_dict ) == 0:
#            return False

        d = { key : [ val ] for key, val in portfolio_frac_dict.items() }
        d[ self.setup.time_name ] = [ timestamp ]

        self.df = self.df.append( pd.DataFrame.from_dict( d )
                                , ignore_index = True
                                )
        return True


    ####################################
    def Get_Portfolio_Frac_Series( self ):
        t = []
        portfolio_frac_series = []
        for _idx, row in self.df.iterrows():
            t.append( row[ self.setup.time_name ] )
            portfolio_frac = row.drop( labels = [ self.setup.time_name ]
                                     ).dropna().to_dict()
            portfolio_frac_series.append( portfolio_frac )

        return t, portfolio_frac_series


########################################
# Desired portfolio value fraction
class Portfolio_Frac_Precursor_Setup():
    ID            = "portfolio_frac_precursor"

    time_name     = "timestamp"
    strategy_name = "strategy"

    less          = "<"
    equal         = "="
    greater       = ">"


def Contains_Symbol(s):
    if not isinstance(s, str):
        return False

    return \
        Portfolio_Frac_Precursor_Setup.less        in s \
        or                                              \
        Portfolio_Frac_Precursor_Setup.equal       in s \
        or                                              \
        Portfolio_Frac_Precursor_Setup.greater     in s


def Is_L_Sym(s):
    if not isinstance(s, str):
        return False

    return \
        Portfolio_Frac_Precursor_Setup.less        in s \
        and                                             \
        Portfolio_Frac_Precursor_Setup.greater not in s


def Is_G_Sym(s):
    if not isinstance(s, str):
        return False

    return \
        Portfolio_Frac_Precursor_Setup.less    not in s \
        and                                             \
        Portfolio_Frac_Precursor_Setup.greater     in s


def Is_LG_Sym(s):
    if not isinstance(s, str):
        return False

    return \
        Portfolio_Frac_Precursor_Setup.less        in s \
        and                                             \
        Portfolio_Frac_Precursor_Setup.greater     in s


def Is_EQ_Sym(s):
    if not isinstance(s, str):
        return False

    return \
        Portfolio_Frac_Precursor_Setup.less    not in s \
        and                                             \
        Portfolio_Frac_Precursor_Setup.equal       in s \
        and                                             \
        Portfolio_Frac_Precursor_Setup.greater not in s


def Sanitize_Symbol(s):
    assert isinstance(s, str)
    assert Contains_Symbol(s)

    if Is_L_Sym(s):
        return Portfolio_Frac_Precursor_Setup.less

    if Is_G_Sym(s):
        return Portfolio_Frac_Precursor_Setup.greater

    if Is_L_Sym(s):
        return Portfolio_Frac_Precursor_Setup.less

    if Is_LG_Sym(s):
        return Portfolio_Frac_Precursor_Setup.less + Portfolio_Frac_Precursor_Setup.greater

    if Is_EQ_Sym(s):
        return Portfolio_Frac_Precursor_Setup.equal


def Is_Number(x):
    return \
        isinstance( x, ( int, float ) ) \
        and                             \
        not pd.isna( x )


def Bust_BS(p_frac):
    d = {}
    for key, val in p_frac.items():
        if Contains_Symbol( val ):
            d[key] = Sanitize_Symbol( val )
            continue

        try:
            val = float(val)
        except ValueError:
            continue

        if pd.isna( val ) or val <= 0:
            continue

        d[key] = val
    return d

Portfolio_Frac_Precursor_Setup.Contains_Symbol = Contains_Symbol
Portfolio_Frac_Precursor_Setup.Is_L_Sym        = Is_L_Sym
Portfolio_Frac_Precursor_Setup.Is_G_Sym        = Is_G_Sym
Portfolio_Frac_Precursor_Setup.Is_LG_Sym       = Is_LG_Sym
Portfolio_Frac_Precursor_Setup.Is_EQ_Sym       = Is_EQ_Sym
Portfolio_Frac_Precursor_Setup.Sanitize_Symbol = Sanitize_Symbol
Portfolio_Frac_Precursor_Setup.Is_Number       = Is_Number
Portfolio_Frac_Precursor_Setup.Bust_BS         = Bust_BS


################################################################################
class Portfolio_Frac_Precursor_Data( Data ):
    ####################################
    def __init__( self
                , Setup = Portfolio_Frac_Precursor_Setup
                , path  = '.'
                ):
        self.df = pd.DataFrame( columns = [ Setup.time_name ] )

        self.setup = Setup
        self.path  = path


    ####################################
    def Get_Latest_Strategies( self ):
        if self.Is_Empty():
            return {}

        timestamp_series = self.df[ self.setup.time_name ]
        df_last_group = self.df[ timestamp_series == timestamp_series.iloc[-1] ]

        portfolio_frac_strategies = {}
        for _idx, row in df_last_group.iterrows():
            strategy_name = row[ self.setup.strategy_name ]
            portfolio_frac_precursor = Bust_BS( row.drop( labels = [ self.setup.time_name
                                                                   , self.setup.strategy_name
                                                                   ]
                                                        ).to_dict()
                                              )
            portfolio_frac_strategies[strategy_name] = portfolio_frac_precursor

        return portfolio_frac_strategies


    ####################################
    def Add_Entry( self
                 , portfolio_frac_dict
                 , timestamp
                 , strategy
                 ):
# So, this is not the right approach, because there is value
# in an empty portfolio. It tells us that we should not own
# anything. And there is a value in restating the same portfolio
# multiple times, because underlying asset values may have changed,
# so adjusting their values make sense too.
#        if len( portfolio_frac ) == 0:
#            return False

        d = { key : [ val ] for key, val in portfolio_frac_dict.items() }
        d[ self.setup.time_name     ] = [ timestamp ]
        d[ self.setup.strategy_name ] = [ strategy ]

        self.df = self.df.append( pd.DataFrame.from_dict( d )
                                , ignore_index = True
                                )
        return True


    ####################################
    def Add_Entries( self
                   , portfolio_frac_strategies
                   , timestamp
                   ):
        for strategy, portfolio_frac in portfolio_frac_strategies.items():
            self.Add_Entry( portfolio_frac
                          , timestamp
                          , strategy
                          )
        return True


    ####################################
    def Add_Entries_If_Different( self
                                , portfolio_frac_strategies
                                ):
        deBS = lambda pfs : \
            { s: Bust_BS( pfp ) for (s,pfp) in pfs.items() }

        # Does this new set differ from the last entry?
        No_BS_new_portfolio_frac_strategies = \
            deBS( portfolio_frac_strategies )

        No_BS_last_portfolio_frac_strategies = \
            deBS( self.Get_Latest_Strategies() )

        if No_BS_new_portfolio_frac_strategies == No_BS_last_portfolio_frac_strategies:
            return False

        return self.Add_Entries( No_BS_new_portfolio_frac_strategies
                               , pd.Timestamp.now('UTC').timestamp()
                               )


    ####################################
    def Get_Strategies_Series( self ):
        timestamp_series                      = []
        portfolio_frac_prec_strategies_series = []

        for _idx, row in self.df.iterrows():
            if len(timestamp_series) == 0 or timestamp_series[-1] != row[ self.setup.time_name ]:
                timestamp_series.append( row[ self.setup.time_name ] )
                portfolio_frac_prec_strategies_series.append( {} )

            strategy_name = row[ self.setup.strategy_name ]
            portfolio_frac_precursor = Bust_BS( row.drop( labels = [ self.setup.time_name
                                                                   , self.setup.strategy_name
                                                                   ]
                                                        ).to_dict()
                                              )
            portfolio_frac_prec_strategies_series[-1][strategy_name] = portfolio_frac_precursor

        return timestamp_series, portfolio_frac_prec_strategies_series


#    ####################################
#    def Add_New_Entry_If_Different( self
#                                  , new_portfolio_frac_precursor
#                                  , new_strategy_name
#                                  ):
#        # Does this new set differ from the last entry?
#        No_BS_new_portfolio_frac_precursor = Bust_BS( new_portfolio_frac_precursor )
#
#        No_BS_last_portfolio_frac_precursor, last_strategy_name = self.Get_Last_Entry()
#
#        if No_BS_new_portfolio_frac_precursor == No_BS_last_portfolio_frac_precursor \
#           and                                                                       \
#           new_strategy_name == last_strategy_name:
#            return False
#
#        return self.Add_New_Entry( No_BS_new_portfolio_frac_precursor
#                                 , pd.Timestamp.now('UTC').timestamp()
#                                 , new_strategy_name
#                                 )


#    ####################################
#    def Get_Last_Entry( self ):
#        if self.df.empty:
#            return {}, ""
#
#        df_lean = self.df.drop( columns = [ self.setup.time_name
#                                          , self.setup.strategy_name
#                                          ]
#                              )
#        portfolio_frac_precursor = Bust_BS( df_lean.iloc[-1].to_dict() )
#
#        strategy_name = self.df[ self.setup.strategy_name ].iloc[-1]
#
#        return portfolio_frac_precursor, strategy_name
