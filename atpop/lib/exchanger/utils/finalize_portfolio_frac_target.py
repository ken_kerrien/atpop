import numpy as np
from scipy.optimize import linprog
from ...data.portfolio_frac_data import Portfolio_Frac_Precursor_Setup as ps


###############################################################################
# Any fraction lower than this will be regarded as 0
# Also if two fractions are within this diff, they are considered equal
PORTFOLIO_FRACTION_MIN = 1e-4
ERROR_TOLERANCE = PORTFOLIO_FRACTION_MIN * 1e-2

def Round_Fraction( frac
                  , min_level = ERROR_TOLERANCE
                  ):
    if min_level is None:
        return frac

    return np.round( frac
                   , -int( np.floor( np.log10(min_level) ) )
                   ) # Round below the portfolio fraction min


def Get_Portfolio_Sum( p_frac
                     , include_list = None
                     , exclude_list = []
                     ):
    if include_list is None:
        include_list = list( p_frac.keys() )

    return \
        sum( [ val for asset, val in p_frac.items()
               if (asset in include_list) and (not asset in exclude_list)
             ]
           )


def Portfolio_Sums_To_One( p_frac ):
    return np.abs(Get_Portfolio_Sum( p_frac ) - 1) < PORTFOLIO_FRACTION_MIN


def Normalize_Portfolio( p_frac
                       , round_min_level = ERROR_TOLERANCE
                       ):
    p_sum = Get_Portfolio_Sum( p_frac )
    new_p_frac = {}
    for asset in p_frac:
        val = p_frac[asset] / p_sum
        if val > PORTFOLIO_FRACTION_MIN:
            new_p_frac[asset] = val

    # It is conceivable that new_p_frac has no assets at all!
    # Imagine a portfolio that has like a million assets with 1e-10
    # fraction for every asset. None of those made it into the new
    # portfolio...
    new_p_sum = Get_Portfolio_Sum( new_p_frac )

    for asset in new_p_frac:
        new_p_frac[asset] = \
            Round_Fraction( new_p_frac[asset] / new_p_sum
                          , round_min_level
                          )

    return new_p_frac


###############################################################################
# Conversion graph example
#                  
# BTC --- USDT --- ETH
#          |        
#          +------ XRP

# Define an asset order
# BTC
# USDT
# ETH
# XRP

# Define fractions
# Before transactions
#  BTC : b0
# USDT : b1
#  ETH : b2
#  XRP : b3
# After transactions
#  BTC : a0
# USDT : a1
#  ETH : a2
#  XRP : a3

########################################
# Directed conversion graph example
#      f0        f1
# BTC <--- USDT ---> ETH
#           |  f2
#           +------> XRP

# Flow vector and anti-flow vector
# The flow is considered positive if it flows "outwards" carrying wealth
# away from the starting node
# f0 : USDT -> BTC   f3 : BTC -> USDT
# f1 : USDT -> ETH   f4 : ETH -> USDT
# f2 : USDT -> XRP   f5 : XRP -> USDT

# KCL for all the nodes
#  BTC : -1*f0 +  0*f1 +  0*f2 +  1*f3 +  0*f4 +  0*f5 =  b0 - a0
# USDT :  1*f0 +  1*f1 +  1*f2 + -1*f3 + -1*f4 + -1*f5 =  b1 - a1
#  ETH :  0*f0 + -1*f1 +  0*f2 +  0*f3 +  1*f4 +  0*f5 =  b2 - a2
#  XRP :  0*f0 +  0*f1 + -1*f2 +  0*f3 +  0*f4 +  1*f5 =  b3 - a3

# Connectivity matrix (C)
# Flow - Anti-flow
# -1  0  0  1  0  0
#  1  1  1 -1 -1 -1
#  0 -1  0  0  1  0
#  0  0 -1  0  0  1

# The equations
#            Cf = b - a
#        Cf + a = b
#       Cf + Ia = b
# [C I][fT aT]T = b

# for equality constraints and numbers
#                0 = b_en - a_en
#             a_en = b_en
#      0f + I_en a = b_en
# [0 I_en][fT aT]T = b_en

# for less or equal constraints
#               0 <= b_l - a_l
#             a_l <= b_l
#      0f + I_l a <= b_l
# [0 I_l][fT aT]T <= b_l

# for greater or equal constraints
#               0 => b_g - a_g
#             a_g => b_g
#            -a_g <= -b_g
#      0f - I_g a <= -b_g
#[0 -I_g][fT aT]T <= -b_g


################################################################################
def Get_Portfolio_Frac_Target( portfolio_frac_precursor # portfolio fraction precursor for the future
                             , portfolio_frac           # current portfolio fraction
                             , reference_asset
                             , error_tolerance = ERROR_TOLERANCE
                             ):
    ####################################
    # Helper functions
    assert_str = lambda :\
          "portfolio_frac_precursor:\n%s\n"%str( portfolio_frac_precursor ) \
        + "portfolio_frac:\n%s\n"          %str( portfolio_frac           ) \
        + "reference_asset:\n%s\n"         %reference_asset                 \
        + "error_tolerance:\n%g\n"         %error_tolerance


    ####################################
    # Check
    assert Portfolio_Sums_To_One( portfolio_frac ), assert_str()

    # Portfolio precursor can only have certain values
    for v in portfolio_frac_precursor.values():
        assert ps.Contains_Symbol( v ) or ps.Is_Number( v ), assert_str()


    ####################################
    # Helper variables
    portfolio_frac_norm = Normalize_Portfolio( portfolio_frac
                                             , None
                                             )
    ordered_assets = \
        list( set(      portfolio_frac_norm.keys() ) \
            | set( portfolio_frac_precursor.keys() ) \
            | { reference_asset }                    \
            )


    ####################################
    # Helper functions
    Get_Precur_Assets = lambda f:\
        [ asset for asset, val in portfolio_frac_precursor.items() if f( val ) ]

    Get_Cur_Frac = lambda asset:\
        portfolio_frac_norm[asset] if asset in portfolio_frac_norm else 0

    Cat = lambda l, r :\
        np.concatenate( ( l
                        , r
                        )
                      , axis=1 # put next to each other
                      )

    Stack = lambda a, b :\
        np.concatenate( ( a
                        , b
                        )
                      , axis=0 # stack on top of each other
                      )

    def Get_Base_Matrix(assets):
        m = np.zeros( ( len(assets)
                      , len(ordered_assets)
                      )
                    )
        for r_idx, asset in enumerate(assets):
            m[ r_idx, ordered_assets.index(asset) ] = 1
        return m


    ####################################
    # Helper variables
    num_assets = list( set(ordered_assets) - set(Get_Precur_Assets( ps.Contains_Symbol )) )
    eq_assets  = Get_Precur_Assets( ps.Is_EQ_Sym )
    g_assets   = Get_Precur_Assets( ps.Is_G_Sym  )
    l_assets   = Get_Precur_Assets( ps.Is_L_Sym  )

    portfolio_frac_precursor_norm = dict(portfolio_frac_precursor)
    # This is to capture the assets not mentioned in the precursor,
    # which are understood to be 0
    for asset in num_assets:
        if asset in portfolio_frac_precursor:
            continue
        portfolio_frac_precursor_norm[asset] = 0


    ####################################
    # If precursor is not reasonable, fix it
    # Behold this fraction precursor:
    #
    # { "BTC" : 10
    # , "XRP" : "<"
    # , "LTC" : "<>"
    # , "ETH" : ">"
    # , "USDT": "="
    # }
    #
    # Clearly 10 is not a reasonable fraction. It is more than the portfolio
    # can provide. Also, we can't decrease > and = assets! So, we look at all
    # the available fractions (not > and not =), and scale the numbers down
    # to something viable. < and <> assets will be zeroed out.
    precur_num_sum   = Get_Portfolio_Sum( portfolio_frac_precursor
                                        , num_assets
                                        )
    cur_not_eq_g_sum = Get_Portfolio_Sum( portfolio_frac_norm
                                        , exclude_list= eq_assets + g_assets
                                        )

    if precur_num_sum > cur_not_eq_g_sum:
        for asset in num_assets:
            portfolio_frac_precursor_norm[asset] *= cur_not_eq_g_sum / precur_num_sum


    ####################################
    # Linprog equality matrices
    ################
    # KCL
    # [C    I][fT aT]T = b

    # Connectivity matrix
    # rows    -> ordered assets
    # columns -> flows
    # We will define all flows to go out from the reference asset
    # But we have anti-flows going the reverse direction, so it's
    # double that
    C = []
    for asset in ordered_assets:
        flow = []
        for asset_target in ordered_assets:
            if asset_target == reference_asset:
                continue

            if reference_asset == asset: # flow_start
                flow.append(1)
            elif asset_target == asset: # flow_stop
                flow.append(-1)
            else:
                flow.append(0)

        flow += [-f for f in flow]
        C.append( flow )

    num_of_assets, num_of_flows = np.shape(C)

    I = np.identity( num_of_assets )

    A_KCL = Cat( C
               , I
               )

    ######
    b_KCL = [ Get_Cur_Frac(asset) for asset in ordered_assets ]

    ################
    # Equality constraints and numbers
    # [0 I_en][fT aT]T = b_en
    Zero_en = np.zeros( ( len(eq_assets) + len(num_assets)
                        , num_of_flows
                        )
                      )

    I_en  = Get_Base_Matrix( eq_assets + num_assets )

    A_en = Cat( Zero_en
              , I_en
              )

    ######
    b_en  = [ Get_Cur_Frac(asset)                  for asset in  eq_assets ] \
          + [ portfolio_frac_precursor_norm[asset] for asset in num_assets ]

    ################
    A_linprog_eq = Stack( A_KCL, A_en )
    b_linprog_eq = b_KCL + b_en


    ####################################
    # Linprog inequality matrices
    ################
    # Less or equal constraints
    # [0 I_l][fT aT]T <= b_l
    # Greater or equal constraints
    #[0 -I_g][fT aT]T <= -b_g
    Zero_lg = np.zeros( ( len(l_assets) + len(g_assets)
                        , num_of_flows
                        )
                      )

    I_lg = Stack(  Get_Base_Matrix( l_assets )
                , -Get_Base_Matrix( g_assets )
                )

    A_lg = Cat( Zero_lg
              , I_lg
              )

    ######
    b_lg = [  Get_Cur_Frac(asset) for asset in l_assets ] \
         + [ -Get_Cur_Frac(asset) for asset in g_assets ]

    ################
    A_linprog_ub = A_lg
    b_linprog_ub = b_lg


    ####################################
    # Linprog objective
    c = np.concatenate( ( np.ones(num_of_flows)
                        , np.zeros(num_of_assets)
                        )
                      )


    ####################################
    # Linear Programming
    try:
#    minimize:
#        c @ x
#
#    such that:
#        A_ub @ x <= b_ub
#        A_eq @ x == b_eq
#        lb <= x <= ub
#
#    By default "lb = 0" and "ub = None" unless specified with "bounds".

        ret = linprog( c
                     , A_ub    = A_linprog_ub
                     , b_ub    = b_linprog_ub
                     , A_eq    = A_linprog_eq
                     , b_eq    = b_linprog_eq
#                     , bounds  = bounds
#                     , method  = 'interior-point'
#                     , method  = 'revised simplex'
                     , method  = 'simplex'
                     , options = { 'tol'      : error_tolerance
                                 , 'maxiter'  : 100000
#                                 , 'bland'    : True
#                                 , 'presolve' : True
#                                 , 'pivot'    : 'bland'
                                 }
                     )
    except ValueError:
#        Debug_Log()
        raise

    if not ret.success:
#        Debug_Log()
        raise ValueError

    ordered_fracs = ret.x[-num_of_assets:]

    portfolio_frac_target = {}
    for asset, frac in zip( ordered_assets, ordered_fracs ):
        if not frac:
            continue
        portfolio_frac_target[asset] = Round_Fraction( frac, error_tolerance )

    assert Portfolio_Sums_To_One( portfolio_frac_target ), assert_str()
    return portfolio_frac_target


################################################################################
def Get_Compound_Portfolio_Frac_Target( portfolio_frac_strategies
                                      , portfolio_frac
                                      , reference_asset
                                      ):
    ####################################
    # Helper functions
    assert_str = lambda :\
          "portfolio_frac_strategies:\n%s\n"%str( portfolio_frac_strategies ) \
        + "portfolio_frac:\n%s\n"           %str( portfolio_frac            ) \
        + "reference_asset:\n%s\n"          %reference_asset


    ####################################
    # Calculation
    compound_portfolio_frac_target = {}
    for portfolio_frac_precursor in portfolio_frac_strategies.values():
        portfolio_frac_target = \
            Get_Portfolio_Frac_Target( portfolio_frac_precursor
                                     , portfolio_frac
                                     , reference_asset
                                     )
        for asset, frac in portfolio_frac_target.items():
            try:
                compound_portfolio_frac_target[asset] += frac
            except KeyError:
                compound_portfolio_frac_target[asset] = frac

    try:
        compound_portfolio_frac_target = Normalize_Portfolio( compound_portfolio_frac_target )
    except ZeroDivisionError as e:
        raise Exception( assert_str() ) from e

    assert Portfolio_Sums_To_One( compound_portfolio_frac_target ), assert_str()

    return compound_portfolio_frac_target
