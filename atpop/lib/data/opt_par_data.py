from .data import Data
import pandas as pd


########################################
# Trade method optimized parameters
class Opt_Par_Setup():
    ID              = "opt_par"
    time_name       = "timestamp"
    price_time_name = "price_timestamp"
    strategy_name   = "strategy"


################################################################################
class Opt_Par_Data(Data):
    ####################################
    def __init__( self
                , setup = Opt_Par_Setup
                , path  = '.'
                ):
        self.df = pd.DataFrame( columns = [ setup.time_name
                                          , setup.price_time_name
                                          , setup.strategy_name
                                          ]
                              )

        self.setup = setup
        self.path  = path


    ####################################
    def _Filter_DF( self
                  , strategy_prefix = None
                  ):
        if strategy_prefix is None:
            return self.df

        strategy_series = self.df[ self.setup.strategy_name ]
        return self.df[ strategy_series.str.startswith( strategy_prefix, na=False ) ]


    ####################################
    def Get_Par_Dict_Series_And_Time_Series( self
                                           , strategy
                                           ):
        if self.Is_Empty():
            return {}, {}

        df_strategy = self.df[ self.df[self.setup.strategy_name] == strategy ]
        df_sort = df_strategy.sort_values( by = self.setup.time_name )

        par_dict_series = []
        time_series     = []
        for _idx, row in df_sort.iterrows():
            t        = row[ self.setup.time_name ]
            par_dict = row.drop( labels = [ self.setup.time_name
                                          , self.setup.price_time_name
                                          , self.setup.strategy_name
                                          ]
                               ).to_dict()

            par_dict_series.append( par_dict )
            time_series.append( t )

        return par_dict_series, time_series


    ####################################
    def Get_Par_Dict_Strategies_Series_And_Time_Series( self ):
        if self.Is_Empty():
            return {}, {}

        df_sort = self.df.sort_values( by = self.setup.time_name )

        par_dict_strategies_series = []
        time_series                = []
        for _idx, row in df_sort.iterrows():
            t        = row[ self.setup.time_name ]
            strategy = row[ self.setup.strategy_name ]
            par_dict = row.drop( labels = [ self.setup.time_name
                                          , self.setup.price_time_name
                                          , self.setup.strategy_name
                                          ]
                               ).to_dict()
            if not time_series or time_series[-1] != t:
                par_dict_strategies_series.append( {} )
                time_series.append( t )

            par_dict_strategies_series[-1][ strategy ] = par_dict

        return par_dict_strategies_series, time_series


    ####################################
    def Get_Latest_Par_Dict( self
                           , strategy
                           ):
        if self.Is_Empty():
            return {}

        df_strategy = self.df[ self.df[self.setup.strategy_name] == strategy ]
        df_sort = df_strategy.sort_values( by = self.setup.time_name )
        df_drop = df_sort.drop( columns = [ self.setup.time_name
                                          , self.setup.price_time_name
                                          , self.setup.strategy_name
                                          ]
                              )
        row = df_drop.iloc[-1]
        par_dict = row.to_dict()

        return par_dict


    ####################################
    def Get_Strategies( self ):
        if self.Is_Empty():
            return []

        return set( self.df[ self.setup.strategy_name ] )


    ####################################
    def Get_Latest_Par_Dict_Strategies_And_Price_Time_Strategies( self
                                                                , strategy_prefix = None
                                                                ):
        if self.Is_Empty():
            return {}, {}

        df_strategy = self._Filter_DF( strategy_prefix )

        timestamp_series = df_strategy[ self.setup.time_name ]
        df_last_group = df_strategy[ timestamp_series == timestamp_series.iloc[-1] ]

        par_dict_strategies = {}
        price_time_strategies = {}
        for _idx, row in df_last_group.iterrows():
            strategy = row[ self.setup.strategy_name ]

            par_dict = row.drop( labels = [ self.setup.time_name
                                          , self.setup.price_time_name
                                          , self.setup.strategy_name
                                          ]
                               ).to_dict()
            par_dict_strategies[ strategy ] = par_dict

            price_time_strategies[ strategy ] = row[ self.setup.price_time_name ]

        return par_dict_strategies, price_time_strategies


    ####################################
    def Add_Entry( self
                 , par_dict
                 , timestamp
                 , price_time
                 , strategy
                 ):
        d = { key : [ val ] for key, val in par_dict.items() }
        d[ self.setup.time_name       ] = [ timestamp  ]
        d[ self.setup.price_time_name ] = [ price_time ]
        d[ self.setup.strategy_name   ] = [ strategy   ]

        self.df = self.df.append( pd.DataFrame.from_dict( d )
                                , ignore_index=True
                                )
        return True


    ####################################
    def Add_Entries( self
                   , price_time_strategies
                   , par_dict_strategies
                   , timestamp
                   ):
        for strategy, par_dict in par_dict_strategies.items():
            self.Add_Entry( par_dict
                          , timestamp
                          , price_time_strategies[strategy]
                          , strategy
                          )
        return True


    ####################################
    def Add_Entries_If_Different( self
                                , price_time_strategies
                                , par_dict_strategies
                                ):
        last_par_dict_strategies, _last_price_time_strategies =\
            self.Get_Latest_Par_Dict_Strategies_And_Price_Time_Strategies()

        if par_dict_strategies == last_par_dict_strategies:
            return False

        return self.Add_Entries( price_time_strategies
                               , par_dict_strategies
                               , pd.Timestamp.now('UTC').timestamp()
                               )


    ####################################
    def Get_Last_Entries( self
                        , strategy
                        , N = 20
                        ):
        if self.Is_Empty():
            return []

        df_strategy = self.df[ self.df[ self.setup.strategy_name ] == strategy ]
        df_sort = df_strategy.sort_values( by = self.setup.time_name )
        df_tail = df_sort.tail(N)
        df_drop = df_tail.drop( columns = [ self.setup.time_name
                                          , self.setup.price_time_name
                                          , self.setup.strategy_name
                                          ]
                              )
        return [ df_drop.iloc[ii].to_dict()
                 for ii in range( len(df_drop) )
               ]


#    ####################################
#    def Get_Last_Entry( self
#                      , strategy = None
#                      ):
#        if self.df.empty:
#            return None
#
#        if strategy is None:
#            df_strategy = self.df
#        else:
#            df_strategy = self.df[ self.df[ self.setup.strategy_name ] == strategy ]
#        df_sort  = df_strategy.sort_values( by = self.setup.time_name )
#        df_drop  = df_sort.drop( columns = [ self.setup.time_name
#                                           , self.setup.price_time_name
#                                           , self.setup.strategy_name
#                                           ]
#                               )
#        return df_drop.iloc[-1].to_dict()


#    ####################################
#    def Get_Last_Strategy( self ):
#        if self.df.empty:
#            return None
#
#        return self.df.sort_values( by = self.setup.time_name )[self.setup.strategy_name].iloc[-1]


#    ####################################
#    def Get_Strategy_Queue_Oldest_First( self ):
#        if self.df.empty:
#            return []
#
#        df = self.df.sort_values( by        = self.setup.time_name
#                                , ascending = False
#                                )
#
#        strategy_queue = []
#        for _, row in df.iterrows():
#            strategy   = row[ self.setup.strategy_name   ]
#
#            if strategy in [a[0] for a in strategy_queue]:
#                continue
#
#            price_time = row[ self.setup.price_time_name ]
#
#            strategy_queue.append( (strategy, price_time) )
#
#        return strategy_queue[::-1]


#    ####################################
#    def Add_New_Entry_If_Different( self
#                                  , strategy
#                                  , price_time
#                                  , opt_par_dict
#                                  ):
#        # Does this new set differ from the last entry?
#        if strategy in self.df[ self.setup.strategy_name ]:
#            if opt_par_dict == self.Get_Last_Entry(strategy):
#                return False
#
#        d = {}
#        for key, value in opt_par_dict.items():
#            d[key] = [value]
#        d[ self.setup.time_name       ] = [ pd.Timestamp.now('UTC').timestamp() ]
#        d[ self.setup.price_time_name ] = [ price_time  ]
#        d[ self.setup.strategy_name   ] = [ strategy    ]
#
#        self.df = self.df.append( pd.DataFrame.from_dict( d )
#                                , ignore_index=True
#                                )
#
#        return True
