import numpy as np
from scipy.signal import lfilter, lfilter_zi

# Exponential
# Moving
# Average

################################################################################
def apply_EMA(signal, N=10):
#    lag = 1/alpha - 1 = (N+1)/2 - 1
    alpha = 2. / (N + 1.)

    a = [1., alpha-1.]
    b = [alpha]

    # Apply the filter. Use lfilter_zi to choose the initial condition
    # of the filter.
    zi = lfilter_zi( b, a )
    filtered_signal, _ = lfilter( b, a, signal, zi=zi*signal[0] )

    return filtered_signal


################################################################################
def Generate_Signals( c
                    , N_fast
                    , N_slow
                    , skip_first_samples = 3
                    , scaling_factor     = 1e2
                    ):
    skip_first_samples = max(0, skip_first_samples)

    fast = np.asarray( apply_EMA(c, N_fast) )
    slow = np.asarray( apply_EMA(c, N_slow) )

    diff = fast - slow
    mean = ( fast + slow ) / 2
    diff_rel = diff / mean * scaling_factor

    return fast, slow, diff_rel


################################################################################
def Calculate_Portfolio_Frac_Precursor( diff_rel
                                      , asset
                                      , reference_asset
                                      ):
    if diff_rel >= 0:
        return { asset: 1. }

    return { reference_asset: 1. }
