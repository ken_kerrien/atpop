import sys, os
sys.path.append( os.path.join( os.path.dirname(__file__)
                             , r'../..'
                             )
               )

from utils.common                       import *

from atpop.lib.exchanger.exchange.utils import timeframes

from atpop.lib.data.price_data          import Price_Data
from atpop.lib.data.opt_par_data        import Opt_Par_Data

from lib.trade_strategy.EMA.setup       import Setup
from lib.trade_strategy.EMA.backtest    import Calculate_And_Plot_Backtest_With_Util_Signals
from lib.trade_strategy.par_defs        import Convert_Par_Dict_To_Pars
from lib.trade_strategy.name_mangling   import Add_Suffix_To_Name

from lib.trade_strategy.EMA.Backtest_Utils.Filter_compute  import Calculate_Scaled_Diff

from services.config                    import Asset_Price_Setup,\
                                               Opt_Setup

import numpy as np


################################################################################
if __name__ == '__main__':
    asset = 'BTC'

    opt_par_data = Opt_Par_Data( path = STATE_ARTIFACTS_PATH )
    opt_par_data.Read_CSV()

    price_data = Price_Data( path = STATE_ARTIFACTS_PATH )
    price_data.Read_CSV()

    par_dict = opt_par_data.Get_Latest_Par_Dict( Add_Suffix_To_Name( Setup.strategy
                                                                   , asset
                                                                   )
                                               )
    pars = Convert_Par_Dict_To_Pars( Setup.par_defs, par_dict )

    assets_price_time, assets_price_record =\
        price_data.Get_Prices_For_Assets_On_Same_Time_Scale( timeframes[Asset_Price_Setup.timeframe]
                                                           , history = Asset_Price_Setup.history
                                                           )

    price = assets_price_record[asset]

    reference_util_scaled =\
        np.cumsum( Calculate_Scaled_Diff( price[1:] - price[:-1]
                                        , price
                                        , Opt_Setup.half_life
                                        )
                 )

    Calculate_And_Plot_Backtest_With_Util_Signals( assets_price_time
                                                 , assets_price_record
                                                 , pars
                                                 , asset
                                                 , "reference"
                                                 , Opt_Setup.trade_fee
                                                 , Opt_Setup.half_life
                                                 #, disp_hist             = int(2*Opt_Setup.half_life)
                                                 , reference_util_scaled = reference_util_scaled
                                                 , title                 = asset
                                                 )
