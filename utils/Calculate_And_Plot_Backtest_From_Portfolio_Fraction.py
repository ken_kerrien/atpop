import sys, os
sys.path.append( os.path.join( os.path.dirname(__file__)
                             , r'../..'
                             )
               )

from atpop.lib.exchanger.exchange.utils import timeframes

from atpop.lib.data.price_data          import Price_Data
from atpop.lib.data.portfolio_frac_data import Portfolio_Frac_Data

from atpop.backtest.compute             import Get_Portfolio_Frac_Target_Series,\
                                               Calculate_Backtest_From_Portfolio_Frac_Target_Series
from atpop.backtest.plot                import Plot_Portfolio_Value_Backtest

import datetime


################################################################################
import json
import os

def Get_Path( file_path = os.path.join( os.path.dirname(__file__)
                                       , "path.json"
                                       )
             ):
    with open(file_path) as fp:
        stuff = json.load(fp)

    return stuff['path']


################################################################################
if __name__ == '__main__':
  week_to_samp  = lambda x, samp_sec : int(x*7*24*60*60./samp_sec)
  timeframe     = '5m'
  history       = week_to_samp( 30.
                              , timeframes[timeframe]
                              )
  trade_fee     = 0.001
  initial_value = 1.

  common_path                = Get_Path()
  price_path                 = common_path + 'state'
  portfolio_frac_target_path = common_path + 'debug'

  reference     = "USDT"


  ####################################
  price_data = Price_Data( path = price_path )
  price_data.Read_CSV()

  portfolio_frac_target_data = Portfolio_Frac_Data( path = portfolio_frac_target_path )
  portfolio_frac_target_data.setup.ID = "portfolio_frac_target"
  portfolio_frac_target_data.Read_CSV()

  ####################################
  assets_price_record =\
    price_data.Get_Prices_For_Assets( timeframes[timeframe]
                                    , max_history = history
                                    )

  trade_time_series, portfolio_frac_target_series =\
    Get_Portfolio_Frac_Target_Series( portfolio_frac_target_data
                                    , assets_price_record.min_start_timestamp
                                    , assets_price_record.max_stop_timestamp
                                    )


  ####################################
  # Calculate the portfolio value
  portfolio_val,\
  time_and_portfolio_fracs = \
    Calculate_Backtest_From_Portfolio_Frac_Target_Series( assets_price_record
                                                        , trade_time_series
                                                        , portfolio_frac_target_series
                                                        , reference
                                                        , trade_fee
                                                        , portfolio_frac_threshold  = 0.03 # 3%
                                                        , portfolio_frac_tolerance  = 0.01 # 1%
#                                                        , initial_value            = 1.
#                                                        , disp_hist                = int(2*Opt_Setup.half_life)
#                                                        , save_portfolio_frac_data = False
                                                        , extend_by_samples        = 12
                                                        )

#  with open( 'debug_port.log'
#           , 'w'
#           ) as f:
#    for blah in [t[1] for t in time_and_portfolio_fracs]:
#      f.write( "%s\n"%str(blah) )
#    for blah in [t[0] for t in time_and_portfolio_fracs]:
#      f.write("%f\n"%blah)

  assets_price_record_to_plot =\
    price_data.Get_Prices_For_Assets( timeframes[timeframe]
                                    , max_history           = history
                                    , try_to_include_assets = ['BTC', 'ETH', 'HBAR']
                                    )


  ####################################
  # Plot
  t_start_opt = datetime.datetime.now()
  print( "Plotting started : ", t_start_opt )

  Plot_Portfolio_Value_Backtest( assets_price_record.timestamp_series
                               , portfolio_val
                               , initial_value
                               , disp_hist                   = 10 + int( (assets_price_record.max_stop_timestamp - trade_time_series[0]) / assets_price_record.time_step_s )
                               , trade_time_series           = [t[0] for t in time_and_portfolio_fracs]
                               , assets_price_record_to_plot = assets_price_record_to_plot
                               )

  t_stop_opt = datetime.datetime.now()
  print( "Plotting finished: ", t_stop_opt )
  print( "Plotting duration: ", t_stop_opt - t_start_opt)
