import numpy as np
from scipy.optimize import linprog
from .utils import Generate_Asset_Vector                             ,\
                   Generate_Connectivity_Matrix_And_Flow_Asset_Pairs

###############################################################################
# Our conversion graph
#  +----------------+
#  |                |
# BTC --- USDT --- ETH
#          |
#          +------ XRP

# Define a currency order
# BTC
# USDT
# ETH
# XRP

# Define states
# Before transactions
#  BTC : b0
# USDT : b1
#  ETH : b2
#  XRP : b3
# After transactions
#  BTC : a0
# USDT : a1
#  ETH : a2
#  XRP : a3
# Portfolio ratio after transactions
#  BTC : r0
# USDT : r1
#  ETH : r2
#  XRP : r3

################################################################################
# NOT NEEDED - Define adjacency matrix
#    from
# to
# (Only lower triangle, so matrix is directed)
#
#       BTC, USDT, ETH, XRP
# BTC     0     0    0    0
# USDT    1     0    0    0
# ETH     1     1    0    0
# XRP     0     1    0    0

################################################################################
# Directed conversion graph
#           f1
#  +------------------+
#  |   f0        f2   v
# BTC ---> USDT ---> ETH
#           |  f3     v f4
#           +------> XRP

# Flow vector
# f0 :  BTC -> USDT   f5 : USDT ->  BTC
# f1 :  BTC ->  ETH   f6 :  ETH ->  BTC
# f2 : USDT ->  ETH   f7 :  ETH -> USDT
# f3 : USDT ->  XRP   f8 :  XRP -> USDT
# f4 :  ETH ->  XRP   f9 :  XRP ->  ETH

# KCL for all the nodes
#  BTC :  1*f0 +  1*f1 +  0*f2 +  0*f3 +  0*f4   +  -g*f5 + -g*f6 +  0*f7 +  0*f8 +  0*f9   =  b0 - a0
# USDT : -g*f0 +  0*f1 +  1*f2 +  1*f3 +  0*f4   +   1*f5 +  0*f6 + -g*f7 + -g*f8 +  0*f9   =  b1 - a1
#  ETH :  0*f0 + -g*f1 + -g*f2 +  0*f3 +  1*f4   +   0*f5 +  1*f6 +  1*f7 +  0*f8 + -g*f9   =  b2 - a2
#  XRP :  0*f0 +  0*f1 +  0*f2 + -g*f3 + -g*f4   +   0*f5 +  0*f6 +  0*f7 +  1*f8 +  1*f9   =  b3 - a3

# Connectivity matrix (C)
# Flow - Anti flow
#  1  1  0  0  0  -g -g  0  0  0
# -g  0  1  1  0   1  0 -g -g  0
#  0 -g -g  0  1   0  1  1  0 -g
#  0  0  0 -g -g   0  0  0  1  1

# The equations
#            Cf = b - a
#        Cf + a = b
#       Cf + Ia = b
# [C I][fT aT]T = b



################################################################################
# State after transactions
#  BTC : a0 / total = r0
# USDT : a1 / total = r1
#  ETH : a2 / total = r2
#  XRP : a3 / total = r3

#  BTC : r0 * (a0 + a1 + a2 + a3) = a0
# USDT : r1 * (a0 + a1 + a2 + a3) = a1
#  ETH : r2 * (a0 + a1 + a2 + a3) = a2
#  XRP : r3 * (a0 + a1 + a2 + a3) = a3

# The equations
# r*1T*a = a

# Let's define M as
# r0-1   r0   r0   r0
#   r1 r1-1   r1   r1
#   r2   r2 r2-1   r2
#   r3   r3   r3 r3-1

#   (r*1T - I)a = 0
#            Ma = 0
# [0 M][fT aT]T = 0


###############################################################################
def Rebalance_Portfolio( portfolio_initial
                       , portfolio_final_percent
                       , asset_pairs
                       , transfer_gain
                       , qn, bn
                       , fn, tn
                       ):
    ####################
    # Define helper variables

    ordered_asset_names = list(set( [asset_pair[qn] for asset_pair in asset_pairs] + [asset_pair[bn] for asset_pair in asset_pairs] ))
    #ordered_asset_names = ['BTC', 'USDT', 'ETH', 'XRP']

    C, flow_asset_pairs =\
        Generate_Connectivity_Matrix_And_Flow_Asset_Pairs( asset_pairs
                                    , ordered_asset_names
                                    , transfer_gain
                                    , qn, bn
                                    , fn, tn
                                    )
    # incoming is negative, outgoing is positive
    #C = np.array( [ [ 1,  1,  0,  0,  0,  -g, -g,  0,  0,  0]
    #              , [ g,  0,  1,  1,  0,   1,  0, -g, -g,  0]
    #              , [ 0, -g, -g,  0,  1,   0,  1,  1,  0, -g]
    #              , [ 0,  0,  0, -g, -g,   0,  0,  0,  1,  1]
    #              ]
    #            )

    num_of_currencies, num_of_tradecons = np.shape(C)
    num_of_tradecons //= 2

    b = Generate_Asset_Vector(portfolio_initial, ordered_asset_names)
    r = Generate_Asset_Vector(portfolio_final_percent, ordered_asset_names)
    #r = [0., 0., 0.5, 0.5]
    #r = [0.1, 0.2, 0.4, 0.3] # This failed the optimizer without  options = {"bland":True}
    #r = [0.3, 0.5, 0.2, 0] # This failed the optimizer


    ####################
    # Define matrices
    ##########
    # [C I][fT aT]T = b
    I = np.identity(num_of_currencies)
    A1 = np.concatenate( (np.asarray(C), I), axis=1 ) # put next to each other

    ##########
    #   (r*1T - I)a = 0
    #            Ma = 0
    M = np.outer( r, np.ones(num_of_currencies) ) - I
    # [0 M][fT aT]T = 0
    Z = np.zeros((num_of_currencies, 2*num_of_tradecons))
    A2 = np.concatenate( (Z, M), axis=1 ) # put next to each other


    ####################
    # Define linprog matrices
    A_eq = np.concatenate( (A1, A2) )
    A_eq = A_eq[:-1] # Get rid of last equation to avoid overdefined condition

    b_eq = np.concatenate( (b, np.zeros(num_of_currencies)) )
    b_eq = b_eq[:-1] # Get rid of last equation to avoid overdefined condition

    c = np.concatenate( (np.zeros(2*num_of_tradecons), -np.ones(num_of_currencies)) )

    abs_max = np.sum( np.abs(b) )


    ####################
    # Linear Programming
    bounds = [ (0 , abs_max) ] * (2*num_of_tradecons) \
           + [ (0 , None   ) ] * num_of_currencies

    ret = linprog( c
                , A_eq    = A_eq
                , b_eq    = b_eq
                , bounds  = bounds
                , options = {"bland":True}
                )

    if ret.x is not None:
        flows = ret.x[:2*num_of_tradecons]
        vals  = ret.x[-num_of_currencies:]
#       flows = np.round(ret.x[:2*num_of_tradecons], 6)
#       vals  = np.round(ret.x[-num_of_currencies:], 6)
    else:
        flows = None
        vals  = None

    return flows, flow_asset_pairs, C, (vals, b, ordered_asset_names, ret.message)
