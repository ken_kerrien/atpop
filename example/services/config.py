from atpop.lib.exchanger.exchange.utils import timeframes
week_to_samp = lambda x, samp_sec : int(x*7*24*60*60./samp_sec)
from lib.trade_strategy.multi_asset_EMA.param_opter import Param_Opter
from lib.trade_strategy.multi_asset_EMA.strategist  import Strategist


################################################################################
import json
import os

def Get_Keys( file_path = os.path.join( os.path.dirname(__file__)
                                      , "keys.json"
                                      )
            ):
    with open(file_path) as fp:
        stuff = json.load(fp)

    return { 'apiKey': stuff['API']
           , 'secret': stuff['SECRET']
           }


################################################################################
class Exchange_Setup():
    ### Keys to the exchange
    name                             = 'hitbtc'
    keys                             = Get_Keys()

    ### Magical constants not to be touched by mere mortals
    # We need at least 10k trade vol in 5m on average to consider an asset
    # refvol/day: refvol/5m * (12 * 5m)/h * (24 * h)/day
    th_val_in_ref                    = 10e3 * 12 * 24
    OHLCV_page_sample_limit          = 1000

    ### Enable sim and debug stuff
#    dry_run                          = True
    sim_trade                        = True
#    debug_asset_data                 = True
    debug_sim_portfolio              = True
    debug_portfolio_rebalance        = True
    debug_calculate_order_precursors = True
#    debug_update_price_data          = True
#    ccxt_verbose                     = True
    debug_exchange_interface         = True


################################################################################
class Portfolio_Setup():
    # At startup, assets of at least this amount become immutable.
    # Any assets of the same type added later will be mutable.
    # E.g.: if immutable_starting_portfolio_request = {'USDT' : 50}, but
    # we only have 30 USDT at startup, that 30 USDT will be untouchable,
    # but any additional USDT made during trading will be fair game.
    immutable_starting_portfolio_request = \
        { 'USDT' : 50.
        }

    ### Magical constants not to be touched by mere mortals
    frac_tolerance      = 0.001
    rebalance_error_tol = 1e-6


################################################################################
class Asset_Price_Setup():
    reference_asset      = 'USDT'
    timeframe            = '5m'
    # Time series total length, anything before that don't matter
    history              = week_to_samp( 30.
                                       , timeframes[timeframe]
                                       )
    # Disregard any asset for which the most recent price is this many seconds old
    max_recent_price_age = 60*60


################################################################################
class Opt_Setup():
    # Trading penalty is the ratio of value to be paid upon transaction
    trade_fee   = 0.001
    # Number of iterations performed by optimizer
    evaluations = int(2e4)

#    debug_opt   = True

    # Filter-based strategies
    # Half life of importance measured in samples
    half_life   = int(Asset_Price_Setup.history / 10)
