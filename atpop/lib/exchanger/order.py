# Setup logging
from ..log_func import logger
from ..log_func import log_exception_info
from ..log_func import log_exception_error

from .utils.portfolio_rebalance_math                      import Rebalance_Portfolio
from .utils.portfolio_rebalance_math.utils_sanitize_trade import Sanitize_Flow_At_IDX,\
                                                                 Sanitize_Amount
from .utils.portfolio_rebalance_math.utils                import Generate_Conversion_Vector,\
                                                                 Generate_Gain_Vector
from .utils.portfolio_handler                             import Exlude_Assets_From_Portfolio,\
                                                                 Add_Portfolios,\
                                                                 Adjust_Simulated_Portfolio  ,\
                                                                 Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs ,\
                                                                 Check_Portfolio_Frac_Target_Within_Tolerance
from .utils.topologically_sort_flows                      import Topologically_Sort_Flows
from .utils.asset_pair_handler                            import Asset_Pair_Handler
from .exchange.utils                                      import Extract_Precisions_Mins_Maxs

from .base                                                import Exchanger_Base


from ccxt import InsufficientFunds
from ccxt import OrderNotFound

import time
import numpy as np


################################################################################
def Get_Portfolio_Not_Covered_By_Asset_Pairs( portfolio, asset_pairs ):
  not_covered_asset_set = set( portfolio.keys() )
  for ap in asset_pairs:
    not_covered_asset_set -= set( ap.values() )

  return { asset: val for asset, val in portfolio.items()
           if asset in not_covered_asset_set
         }


################################################################################
class Exchanger_Order( Exchanger_Base ):
  ############################################################################
  def __init__( self
              , **kwargs
              ):
    super().__init__( **kwargs )

    self.Asset_Price_Setup = kwargs["Asset_Price_Setup"]


  ############################################################################
  def _Get_Asset_Data( self
                     , immutable_portfolio
                     , reference_asset
                     , sink_assets
                     , portfolio = None
                     , tickers   = None
                     , markets   = None
                     ):
    immutable_portfolio = dict(immutable_portfolio) # because we may change this later

    ########################################################################
    # Query most up-to-date data from exchange

    # Get current asset portfolio
    # Can raise Portfolio_Fetch_Exception
    if portfolio is None:
      portfolio = self.ex.Get_Asset_Portfolio()

    # Fetch tickers for conversion prices
    if tickers is None:
      tickers = self.ex.Fetch_Tickers()

    # Fetch markets for precision and min/max values
    if markets is None:
      markets = self.ex.Get_Markets()


    if self._Get_Param_True('debug_asset_data'):
      import os
      from datetime import datetime

      os.makedirs( os.path.abspath(self.DEBUG_ARTIFACTS_PATH)
                  , exist_ok = True
                  )

      with open( os.path.join( self.DEBUG_ARTIFACTS_PATH, 'get_asset_data.log' )
               , 'a'
               ) as f:
        def log_local_var(var_name, vars_dict=locals()):
          f.write("#" + var_name + " = \\\n" )
          f.write( str( vars_dict[var_name] ) + "\n" )

        f.write("#"*20 + "\n")
        f.write( str(                       time.time() ) + "\n" )
        f.write( str(datetime.fromtimestamp(time.time())) + "\n" )
        log_local_var("immutable_assets")
        log_local_var( "reference_asset")
        log_local_var(     "sink_assets")
        f.write("#"*10 + "\n")
        log_local_var(       "portfolio")
        log_local_var(         "tickers")
        log_local_var(         "markets")
        f.write("#"*10 + "\n")


    ############################################################################
    # Preprocess

    # Get all markets showing all the possible trading pairs
    asset_pair_handler = Asset_Pair_Handler( self.bn, self.qn )
    asset_pair_handler.Generate_Asset_Pairs( tickers.keys() )

    # Filter them so we only have assets affiliated with the reference asset
    # - the value of any given asset is measured in the reference asset
    # - helps with circular dependencies later on
    asset_pair_handler.Filter_To_Contain_Asset( reference_asset )

    # It's possible that we own an asset, for which there is no exchange rate,
    # in which case that asset can not be changed, and becomes immutable
    not_covered_portfolio = \
      Get_Portfolio_Not_Covered_By_Asset_Pairs( portfolio, asset_pair_handler.asset_pairs )

    for asset, val in not_covered_portfolio.items():
      logger.warning( "Asset %s %g is in portfolio, but most recent ticker query did not have it anymore, so it became immutable"
                    , asset
                    , val
                    )

      immutable_portfolio[asset] = val

    # Exclude assets we want to save from current portfolio
    tradeable_portfolio, _ = \
      Exlude_Assets_From_Portfolio( portfolio
                                  , immutable_portfolio
                                  )

    asset_pair_handler.Filter_All_Possible_Flows( tradeable_portfolio.keys()
                                                , sink_assets
                                                )
    asset_pair_handler.Query_Exchange_Rate( self.ex
                                          , tickers
                                          , filter_flag = True
                                          )
    # Get fees for asset pairs trades, I think this will be just one number, but
    # potentially it may be different
    asset_pair_handler.Extract_Trade_Fees( markets )

    return tradeable_portfolio, markets, asset_pair_handler.asset_pairs


  ############################################################################
  def _Calculate_Order_Precursors( self
                                 , portfolio_frac_target
                                 , reference_asset
                                 , portfolio_frac_threshold
                                 , portfolio_frac_tolerance
                                 , error_tolerance
                                 , portfolio
                                 , markets
                                 , asset_pairs
                                 , save_debug_data                      = False
                                 , save_debug_data_at_benign_exceptions = True
                                 , debug_path                           = '.'
                                 ):
        if save_debug_data:
            import os
            from datetime import datetime

            with open( os.path.join( debug_path, 'calculate_order_precursors.log' )
                     , 'a'
                     ) as f:
                def log_local_var(var_name, vars_dict=locals()):
                    f.write("#" + var_name + " = \\\n" )
                    f.write( str( vars_dict[var_name] ) + "\n" )

                def log_expr(expr, expr_val):
                    f.write("#" + expr + " = \\\n" )
                    f.write( str( expr_val ) + "\n" )

                f.write("#"*20 + "\n")
                f.write( str(                       time.time() ) + "\n" )
                f.write( str(datetime.fromtimestamp(time.time())) + "\n" )
                log_local_var("portfolio_frac_target")
                log_local_var("reference_asset")
                log_local_var("portfolio_frac_threshold")
                log_local_var("portfolio_frac_tolerance")
                log_local_var("error_tolerance")
                log_local_var("portfolio")
                log_local_var("markets")
                log_local_var("asset_pairs")

        class Res():
            pass

        res = Res()
        res.ordered_flow_idxs         = []

        ########################################################################
        portfolio_in_ref = \
           Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( portfolio
                                                                 , reference_asset
                                                                 , asset_pairs
                                                                 , self.qn, self.bn
                                                                 )
        portfolio_sum_in_ref = sum( portfolio_in_ref.values() )
        portfolio_frac = { k:v/portfolio_sum_in_ref
                           for k,v in portfolio_in_ref.items() if v > 0
                         }

        if Check_Portfolio_Frac_Target_Within_Tolerance( portfolio_frac_target
                                                       , portfolio_frac
                                                       , portfolio_frac_threshold
                                                       ):
            return res

        ########################################################################
        try:
            ############################################################################
            # Generate sequential flows
            # The portfolio rebalancing is solved as a network flow problem
            res.flows, res.flow_asset_pairs, res.conn_matrix, misc =\
                Rebalance_Portfolio( portfolio
                                   , portfolio_frac_target
                                   , asset_pairs
                                   , reference_asset
                                   , portfolio_frac_tolerance
                                   , self.qn, self.bn
                                   , self.fn, self.tn
                                   , error_tolerance = error_tolerance
                                   , save_debug_data = self._Get_Param_True('debug_portfolio_rebalance')
                                   , debug_path      = self.DEBUG_ARTIFACTS_PATH
                                   )

            _, res.ordered_asset_names, _ = misc

            # Figure out in which order flows have to come
            res.ordered_flow_idxs =\
                Topologically_Sort_Flows( res.flows
                                        , res.flow_asset_pairs
                                        , self.fn, self.tn
                                        )

            # At this point we have trade_asset pairs looking like this:
            #[ {'to': 'USDT', 'from':  'BTC'}
            #, {'to':  'ETH', 'from':  'BTC'}
            #, {'to':  'XRP', 'from': 'USDT'}]


            ############################################################################
            # Postprocess
            # If we want to turn flows into trade actions, we have to follow
            # some limitations on trade actions with regards to precision and min/max values
            precisions, mins, maxs =\
                Extract_Precisions_Mins_Maxs( markets
                                            , res.flow_asset_pairs
                                            , self.qn, self.bn
                                            )
            res.precision_array = np.asarray(precisions)
            res.min_array       = np.asarray(mins)
            res.max_array       = np.asarray(maxs)

            # Precision, min, and max are measured in the base currency, but
            # flows are not neccesarily measured in base currency, because
            # half of the flows are in the quote currency. In those latter cases,
            # we have to convert the flow values to base currency to sanitize. Then
            # we convert them back to quote, to see their impact on the rest of the flows.
            # If flow starts from base currency, i.e. I'm selling base to get quote, conversion is 1.
            # If flow starts from quote currency, i.e. I'm buying base with quote, conversion is 1/exchange_rate
            # See the trick I did here: ap[fn] <-> ap[bn] Smart, ain't it?
            conversions = \
                Generate_Conversion_Vector( [ ap[self.fn] for ap in res.flow_asset_pairs ]
                                          , [ ap[self.bn] for ap in res.flow_asset_pairs ]
                                          , asset_pairs
                                          , self.qn, self.bn
                                          )
            res.conversion_to_base_array = np.asarray( conversions )

            gains = \
                Generate_Gain_Vector( [ ap[self.fn] for ap in res.flow_asset_pairs ]
                                    , [ ap[self.bn] for ap in res.flow_asset_pairs ]
                                    , asset_pairs
                                    , self.qn, self.bn
                                    )
            res.gain_to_base_array = np.asarray( gains )

            # flow starts from base, goes to quote; measured in base; sell base; amount in base; conversion = 1
            # flow starts from quote, goes to base; measured in quote; buy base; amount in base; conversion < 1
            res.adjusted_flow_array_base = np.asarray(res.flows) * res.conversion_to_base_array * res.gain_to_base_array
        except Exception as err:
            if not save_debug_data:
                raise

            benign_exceptions = [ "ValueError", "Portfolio_Rebalance_Exception" ]
            exception_type = type(err).__name__
            if exception_type in benign_exceptions and not save_debug_data_at_benign_exceptions:
                raise

            os.makedirs( os.path.abspath( debug_path )
                       , exist_ok = True
                       )

            with open( os.path.join( debug_path, 'calculate_order_precursors.log' )
                     , 'a'
                     ) as f:
                def log_local_var(var_name, vars_dict=locals()):
                    f.write("#" + var_name + " = \\\n" )
                    f.write( str( vars_dict[var_name] ) + "\n" )

                def log_expr(expr, expr_val):
                    f.write("#" + expr + " = \\\n" )
                    f.write( str( expr_val ) + "\n" )

                log_expr("err", err)

            raise

        return res


  ############################################################################
  def _Place_Market_Order( self
                         , res
                         , asset_pairs
                         , flow_idx = None
                         ):
    assert len(res.ordered_flow_idxs) > 0

    insuf_fund_tries = 10

    if flow_idx is None:
      flow_idx       = res.ordered_flow_idxs[ 0 ]

    trade_asset_pair = res.flow_asset_pairs[         flow_idx ]
    amount_in_base   = res.adjusted_flow_array_base[ flow_idx ]


    while insuf_fund_tries > 0:
      dirty_amount_in_base = amount_in_base

      res.adjusted_flow_array_base[ flow_idx ] = amount_in_base

      res.adjusted_flow_array_base = Sanitize_Flow_At_IDX( res
                                                         , flow_idx
                                                         )

      amount_in_base = res.adjusted_flow_array_base[ flow_idx ]

      order = self.__Generate_Order( trade_asset_pair
                                   , amount_in_base
                                   )

      symbol, _amount_in_base, buy_base_not_sell = order
      buy_not_sell_string = "buy" if buy_base_not_sell else "sell"

      logger.info( "Calculated order %s, %g (sanitized from %g), %s"
                 , symbol
                 , amount_in_base
                 , dirty_amount_in_base
                 , buy_not_sell_string
                 )

      if amount_in_base <= 0.:
        return

      # Trade!
      try:
        if self._Get_Param_True('sim_trade'):
          news_portfolio = Adjust_Simulated_Portfolio( self.tradeable_portfolio
                                                     , symbol
                                                     , amount_in_base
                                                     , buy_base_not_sell
                                                     , res
                                                     , self.Asset_Price_Setup.reference_asset
                                                     , asset_pairs
                                                     , save_debug_data = self._Get_Param_True('debug_sim_portfolio')
                                                     , debug_path      = self.DEBUG_ARTIFACTS_PATH
                                                     , qn = self.qn
                                                     , bn = self.bn
                                                     , fn = self.fn
                                                     , tn = self.tn
                                                     )
          immutable_portfolio = dict(self.immutable_starting_portfolio)

          not_covered_portfolio = \
            Get_Portfolio_Not_Covered_By_Asset_Pairs( self.simulated_portfolio, asset_pairs )

          for asset, val in not_covered_portfolio.items():
            immutable_portfolio[asset] = val

          news_portfolio = Add_Portfolios( news_portfolio, immutable_portfolio )

          self.simulated_portfolio = news_portfolio

          order_ID = 'no_trade_ID_sim_only'
        else:
          order_ID = self.ex.Create_Market_Order( symbol
                                                , amount_in_base
                                                , buy_base_not_sell
                                                )

        break
      except InsufficientFunds as e:
        # It's conceivable that between the time of calculating the order and
        # executing it the price had changed, and consequently we don't have
        # precisely enough to cover the transaction. This is not an error, but
        # rather an idiosyncrasy of the entire system. We just need to try a bit
        # smaller amount. If any transaction does go through, we will recalculate
        # the orders from start anyways.
        insuf_fund_tries -= 1
        logger.info( str(e) )
        logger.info( "Insufficient funds, let's decrease the amount, %d tries left"
                   , insuf_fund_tries
                   )

        amount_in_base *= 0.9
    else:
      return False # We'll just return and let the entire thing be recalculated from scratch


    # Success!
    order_str = "%s (%s, %g, %s)"%( order_ID
                                  , symbol
                                  , amount_in_base
                                  , buy_not_sell_string
                                  )

    logger.info( "Created market order %s"
               , order_str
               )

    return order_ID, order_str


  ############################################################################
  def _Verify_Order( self
                   , order_ID
                   , order_str
                   ):
        ##############################
        # Wait until order goes through
        for order_over_tries in range(10, 0, -1):
            try:
                if self._Get_Param_True('sim_trade'):
                    order_status = 'closed'
                else:
                    order_status = self.ex.Fetch_Order(order_ID)['status']
            except OrderNotFound:
                logger.info( "Order %s was not found, we'll assume it went through"
                           , order_str
                           )
                return True # So, one order went through, but there may be others, so the entire thing be recalculated from scratch

            if order_status == 'canceled':
                logger.info( "Order %s was canceled"
                           , order_str
                           )
                return False # We'll just return and let the entire thing be recalculated from scratch

            if order_status == 'closed':
                logger.info( "Order %s went through"
                           , order_str
                           )
                return True # So, one order went through, but there may be others, so the entire thing be recalculated from scratch

            logger.info( "Order %s is not over yet, %d tries left"
                       , order_str
                       , order_over_tries-1
                       )

        try:
            self.ex.Cancel_Order(order_ID)
        except OrderNotFound:
            pass

        return False # We'll just return and let the entire thing be recalculated from scratch


  ############################################################################
  def __Generate_Order( self
                      , trade_asset_pair
                      , flow_val_in_base
                      ):
        if trade_asset_pair[self.fn] == trade_asset_pair[self.bn] and trade_asset_pair[self.tn] == trade_asset_pair[self.qn]:
            # from base to quote -> sell base
            buy_base_not_sell = False
        elif trade_asset_pair[self.fn] == trade_asset_pair[self.qn] and trade_asset_pair[self.tn] == trade_asset_pair[self.bn]:
            # from quote to base -> buy base
            buy_base_not_sell = True

        return ( trade_asset_pair[self.bn] + '/' + trade_asset_pair[self.qn]
               , flow_val_in_base
               , buy_base_not_sell
               )


  ############################################################################
  # Purely for unittest purposes
  def Get_Orders( self
                , portfolio_frac_target
                , immutable_assets
                , reference_asset
                , portfolio_frac_threshold
                , portfolio_frac_tolerance
                , portfolio_rebalance_error_tol
                , save_debug_data = False
                , debug_path      = '.'
                , portfolio = None
                , tickers   = None
                , markets   = None
                ):
        portfolio, markets, asset_pairs = \
            self._Get_Asset_Data( immutable_assets
                                , reference_asset
                                , portfolio_frac_target.keys()
                                , portfolio
                                , tickers
                                , markets
                                )

        res = \
            self._Calculate_Order_Precursors( portfolio_frac_target
                                            , reference_asset
                                            , portfolio_frac_threshold
                                            , portfolio_frac_tolerance
                                            , portfolio_rebalance_error_tol
                                            , portfolio
                                            , markets
                                            , asset_pairs
                                            )

        orders = []
        for idx in res.ordered_flow_idxs:
            trade_asset_pair = res.flow_asset_pairs[idx]
            res.adjusted_flow_array_base = Sanitize_Flow_At_IDX( res
                                                               , idx
                                                               )

            order = self.__Generate_Order( trade_asset_pair
                                         , res.adjusted_flow_array_base[ idx ]
                                         )
            if order[1] == 0.:
                continue

            orders.append( order )

        return orders
