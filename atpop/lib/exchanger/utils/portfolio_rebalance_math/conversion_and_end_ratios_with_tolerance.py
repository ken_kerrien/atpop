import numpy as np
from scipy.optimize import linprog
from .utils import Generate_Asset_Vector      ,\
                   Generate_Conversion_Vector ,\
                   Portfolio_Rebalance_Exception
from ..names import EXCHANGE_RATE_NAME,\
                    TRADE_FEE_NAME


###############################################################################
def Generate_Connectivity_Matrix_And_Flow_Origin_Names_And_Flow_Asset_Pairs( asset_pairs
                                                                           , ordered_asset_names
                                                                           , qn, bn
                                                                           , fn, tn
                                                                           ):
    # Connectivity matrix
    # rows    -> ordered_asset_names
    # columns -> asset_pairs
    conn_matrix = []
    for asset_name in ordered_asset_names:
        flow_f = []
        flow_b = []
        for asset_pair in asset_pairs:
            if asset_pair[qn] == asset_name:
                flow_f.append(1)
                flow_b.append( -(1. - asset_pair[TRADE_FEE_NAME]) * asset_pair[EXCHANGE_RATE_NAME] )
            elif asset_pair[bn] == asset_name:
                flow_f.append( -(1. - asset_pair[TRADE_FEE_NAME]) / asset_pair[EXCHANGE_RATE_NAME] )
                flow_b.append(1)
            else:
                flow_f.append(0.)
                flow_b.append(0.)
        conn_matrix.append( flow_f + flow_b )

    assert conn_matrix

    # Flow start names
    on_f = []
    on_b = []
    for asset_pair in asset_pairs:
        on_f.append( asset_pair[qn] )
        on_b.append( asset_pair[bn] )
    on = on_f + on_b

    # Flow asset pairs
    fap_f = []
    fap_b = []
    for asset_pair in asset_pairs:
        fap_f.append( dict(asset_pair) )
        fap_f[-1][fn] = fap_f[-1][qn]
        fap_f[-1][tn] = fap_f[-1][bn]

        fap_b.append( dict(asset_pair) )
        fap_b[-1][fn] = fap_b[-1][bn]
        fap_b[-1][tn] = fap_b[-1][qn]

    flow_asset_pairs = fap_f + fap_b

    return conn_matrix, on, flow_asset_pairs


###############################################################################
# Our conversion graph
#  +----------------+
#  |                |
# BTC --- USDT --- ETH
#          |        |
#          +------ XRP

# Define a currency order
# BTC
# USDT
# ETH
# XRP

# Define states
# Before transactions
#  BTC : b0
# USDT : b1
#  ETH : b2
#  XRP : b3
# After transactions
#  BTC : a0
# USDT : a1
#  ETH : a2
#  XRP : a3
# Portfolio fraction after transactions
#  BTC : r0
# USDT : r1
#  ETH : r2
#  XRP : r3

################################################################################
# NOT NEEDED - Define adjacency matrix
#    from
# to
# (Only lower triangle, so matrix is directed)
#
#       BTC, USDT, ETH, XRP
# BTC     0     0    0    0
# USDT    1     0    0    0
# ETH     1     1    0    0
# XRP     0     1    0    0

################################################################################
# Directed conversion graph
#           f1
#  +------------------+
#  |   f0        f2   v
# BTC ---> USDT ---> ETH
#           |  f3     v f4
#           +------> XRP

# Flow vector and anti flow vector
# The value of the flow vector is measured in the currency it starts from!
# The flow is considered positive if it flows "outwards" carrying currency
# away from the starting node
# f0 :  BTC -> USDT   f5 : USDT ->  BTC
# f1 :  BTC ->  ETH   f6 :  ETH ->  BTC
# f2 : USDT ->  ETH   f7 :  ETH -> USDT
# f3 : USDT ->  XRP   f8 :  XRP -> USDT
# f4 :  ETH ->  XRP   f9 :  XRP ->  ETH

# KCL for all the nodes
#  BTC :             1*f0 +            1*f1 +             0*f2 +             0*f3 +            0*f4   +  -g/v_BTC_USDT*f5 + -g/v_BTC_ETH*f6 +             0*f7 +             0*f8 +            0*f9   =  b0 - a0
# USDT : -g*v_BTC_USDT*f0 +            0*f1 +             1*f2 +             1*f3 +            0*f4   +              1*f5 +            0*f6 + -g*v_ETH_USDT*f7 + -g*v_XRP_USDT*f8 +            0*f9   =  b1 - a1
#  ETH :             0*f0 + -g*v_BTC_ETH*f1 + -g/v_ETH_USDT*f2 +             0*f3 +            1*f4   +              0*f5 +            1*f6 +             1*f7 +             0*f8 + -g*v_XRP_ETH*f9   =  b2 - a2
#  XRP :             0*f0 +            0*f1 +             0*f2 + -g/v_XRP_USDT*f3 + -g/v_XRP_ETH*f4   +              0*f5 +            0*f6 +             0*f7 +             1*f8 +            1*f9   =  b3 - a3

# Connectivity matrix (C)
# Flow - Anti flow
#             1            1             0             0            0   -g/v_BTC_USDT -g/v_BTC_ETH             0             0            0
# -g*v_BTC_USDT            0             1             1            0               1            0 -g*v_ETH_USDT -g*v_XRP_USDT            0
#             0 -g*v_BTC_ETH -g/v_ETH_USDT             0            1               0            1             1             0 -g*v_XRP_ETH
#             0            0             0 -g/v_XRP_USDT -g/v_XRP_ETH               0            0             0             1            1

# The equations
#            Cf = b - a
#        Cf + a = b
#       Cf + Ia = b
# [C I][fT aT]T = b



################################################################################
# State after transactions
#  BTC : max(0, r0 - e) <= a0_CQ / total_CQ <= min(1, r0 + e)
# USDT : max(0, r1 - e) <= a1_CQ / total_CQ <= min(1, r1 + e)
#  ETH : max(0, r2 - e) <= a2_CQ / total_CQ <= min(1, r2 + e)
#  XRP : max(0, r3 - e) <= a3_CQ / total_CQ <= min(1, r3 + e)

#  BTC : r0i <= a0_CQ / total_CQ <= r0s
# USDT : r1i <= a1_CQ / total_CQ <= r1s
#  ETH : r2i <= a2_CQ / total_CQ <= r2s
#  XRP : r3i <= a3_CQ / total_CQ <= r3s

#  BTC : r0i * (a0_CQ + a1_CQ + a2_CQ + a3_CQ) <= a0_CQ <= r0s * (a0_CQ + a1_CQ + a2_CQ + a3_CQ)
# USDT : r1i * (a0_CQ + a1_CQ + a2_CQ + a3_CQ) <= a1_CQ <= r1s * (a0_CQ + a1_CQ + a2_CQ + a3_CQ)
#  ETH : r2i * (a0_CQ + a1_CQ + a2_CQ + a3_CQ) <= a2_CQ <= r2s * (a0_CQ + a1_CQ + a2_CQ + a3_CQ)
#  XRP : r3i * (a0_CQ + a1_CQ + a2_CQ + a3_CQ) <= a3_CQ <= r3s * (a0_CQ + a1_CQ + a2_CQ + a3_CQ)

#  BTC : r0i * (a0*v_BTC_CQ + a1*v_USDT_CQ + a2*v_ETH_CQ + a3*v_XRP_CQ) <= a0* v_BTC_CQ <= r0s * (a0*v_BTC_CQ + a1*v_USDT_CQ + a2*v_ETH_CQ + a3*v_XRP_CQ)
# USDT : r1i * (a0*v_BTC_CQ + a1*v_USDT_CQ + a2*v_ETH_CQ + a3*v_XRP_CQ) <= a1*v_USDT_CQ <= r1s * (a0*v_BTC_CQ + a1*v_USDT_CQ + a2*v_ETH_CQ + a3*v_XRP_CQ)
#  ETH : r2i * (a0*v_BTC_CQ + a1*v_USDT_CQ + a2*v_ETH_CQ + a3*v_XRP_CQ) <= a2* v_ETH_CQ <= r2s * (a0*v_BTC_CQ + a1*v_USDT_CQ + a2*v_ETH_CQ + a3*v_XRP_CQ)
#  XRP : r3i * (a0*v_BTC_CQ + a1*v_USDT_CQ + a2*v_ETH_CQ + a3*v_XRP_CQ) <= a3* v_XRP_CQ <= r3s * (a0*v_BTC_CQ + a1*v_USDT_CQ + a2*v_ETH_CQ + a3*v_XRP_CQ)

# The equations
# [v_BTC_CQ         0        0        0
#         0 v_USDT_CQ        0        0
#         0         0 v_ETH_CQ        0
#         0         0        0 v_XRP_CQ] = V

#       ri*1T*V*a <= V*a <= rs*1T*V*a
# (ri*1T - I) V a <= 0   <= (rs*1T - I) V a

# Let's define M as
# [r0-1   r0   r0   r0
#    r1 r1-1   r1   r1
#    r2   r2 r2-1   r2
#    r3   r3   r3 r3-1] = r*1T - I = M

#          Mi V a <= 0 <= Ms V a

# [MiVT -MsVT]T a <= 0
#           Mis a <= 0
# [0 Mis][fT aT]T <= 0


###############################################################################
def Rebalance_Portfolio( portfolio
                       , portfolio_frac_target
                       , asset_pairs
                       , reference_asset
                       , portfolio_frac_tolerance
                       , qn, bn
                       , fn, tn
                       , error_tolerance = 1e-6
                       , save_debug_data = False
                       , debug_path      = '.'
                       ):
    ####################
    # Define helper variables

    ordered_asset_names = list( set( [asset_pair[qn] for asset_pair in asset_pairs]
                                   + [asset_pair[bn] for asset_pair in asset_pairs]
                                   )
                              )
    #ordered_asset_names = ['BTC', 'USDT', 'ETH', 'XRP']

    C, flow_origin_names, flow_asset_pairs =\
        Generate_Connectivity_Matrix_And_Flow_Origin_Names_And_Flow_Asset_Pairs( asset_pairs
                                                                               , ordered_asset_names
                                                                               , qn, bn
                                                                               , fn, tn
                                                                               )
    # incoming is negative, outgoing is positive
    #C = np.array( [ [             1,            1,             0,             0,            0, -g/v_BTC_USDT, -g/v_BTC_ETH,             0,             0,            0 ]
    #                [ -g*v_BTC_USDT,            0,             1,             1,            0,             1,            0, -g*v_ETH_USDT, -g*v_XRP_USDT,            0 ]
    #                [             0, -g*v_BTC_ETH, -g/v_ETH_USDT,             0,            1,             0,            1,             1,             0, -g*v_XRP_ETH ]
    #                [             0,            0,             0, -g/v_XRP_USDT, -g/v_XRP_ETH,             0,            0,             0,             1,            1 ]
    #              ]
    #            )

    num_of_currencies, num_of_tradecons = np.shape(C)
    num_of_tradecons //= 2

    b = Generate_Asset_Vector(portfolio,             ordered_asset_names)
    r = Generate_Asset_Vector(portfolio_frac_target, ordered_asset_names)
    #r = [0., 0., 0.5, 0.5]
    #r = [0.1, 0.2, 0.4, 0.3] # This failed the optimizer without  options = {"bland":True}
    #r = [0.3, 0.5, 0.2, 0] # This failed the optimizer

    v = Generate_Conversion_Vector( ordered_asset_names
                                  , reference_asset
                                  , asset_pairs
                                  , qn, bn
                                  )


    vonc = Generate_Conversion_Vector( flow_origin_names
                                     , reference_asset
                                     , asset_pairs
                                     , qn, bn
                                     )


    ####################
    # Define matrices
    ##########
    # [C I][fT aT]T = b
    I = np.identity(num_of_currencies)
    A1 = np.concatenate( (C, I), axis=1 ) # put next to each other

    ##########
    # (ri*1T - I) V a <= 0 <= (rs*1T - I) V a
    #          Mi V a <= 0 <= Ms V a
    ri = np.maximum( np.asarray(r) - portfolio_frac_tolerance, 0 )
    rs = np.minimum( np.asarray(r) + portfolio_frac_tolerance, 1 )
    # Let's keep zero desired portfolio fractions zeros
    # In other word, tolerances should not apply to zeros
    for ii in range(len(r)):
        if r[ii] == 0.:
            rs[ii] = 0.
    Mi = np.outer( ri, np.ones(num_of_currencies) ) - I
    Ms = np.outer( rs, np.ones(num_of_currencies) ) - I

    V = np.diag( v )

    MiV = np.inner( Mi, V )
    MsV = np.inner( Ms, V )

    # [MiVT -MsVT]T a <= 0
    #           Mis a <= 0
    Mis = np.concatenate( (MiV, -MsV) )

    # [0 Mis][fT aT]T <= 0
    Z = np.zeros((2*num_of_currencies, 2*num_of_tradecons))
    A2 = np.concatenate( (Z, Mis), axis=1 ) # put next to each other


    ####################
    # Define linprog matrices
    A_eq = A1

    b_eq = b

    A_ub = A2

    b_ub = np.zeros(2*num_of_currencies)

    c = np.concatenate( (np.zeros(2*num_of_tradecons), -np.ones(num_of_currencies)*v) )

    abs_max_CQ = 1. * np.sum( np.asarray(b)*np.asarray(v) )


    ####################
    # Linear Programming

    # currency bounds

    a_bounds = [ (0, abs_max_CQ/vt) for vt in v ]
    f_bounds = [ (0, abs_max_CQ/vt) for vt in vonc ]

    bounds = f_bounds + a_bounds

    def Debug_Log():
        if not save_debug_data:
            return

        from os       import path
        from time     import time
        from datetime import datetime

        with open( path.join( debug_path, 'rebalance_portfolio_conversion_and_end_ratios_with_tolerance.log' )
                 , 'a'
                 ) as f:
            def log_local_var(var_name, vars_dict=locals()):
                f.write("#" + var_name + " = \\\n" )
                f.write( str( vars_dict[var_name] ) + "\n" )

            def log_expr(expr, expr_val):
                f.write("#" + expr + " = \\\n" )
                f.write( str( expr_val ) + "\n" )

            f.write("#"*20 + "\n")
            f.write( str(                       time() ) + "\n" )
            f.write( str(datetime.fromtimestamp(time())) + "\n" )
#            log_local_var(               "portfolio")
#            log_local_var(   "portfolio_frac_target")
#            log_local_var(             "asset_pairs")
#            log_local_var(         "reference_asset")
#            log_local_var("portfolio_frac_tolerance")
#            log_local_var(         "error_tolerance")
            log_expr( "portfolio"
                    , portfolio
                    )
            log_expr( "portfolio_frac_target"
                    , portfolio_frac_target
                    )
            log_expr( "asset_pairs"
                    , asset_pairs
                    )
            log_expr( "reference_asset"
                    , reference_asset
                    )
            log_expr( "portfolio_frac_tolerance"
                    , portfolio_frac_tolerance
                    )
            log_expr( "error_tolerance"
                    , error_tolerance
                    )
            f.write("#"*10 + "\n")
#            log_local_var(     "ordered_asset_names")
            log_expr( "ordered_asset_names"
                    , ordered_asset_names
                    )
#            log_local_var(        "flow_asset_pairs")
            log_expr( "flow_asset_pairs"
                    , flow_asset_pairs
                    )
#            log_local_var(                     "ret")
            try:
                log_expr( "ret.message"
                        , ret.message
                        )
            except (NameError, AttributeError):
                pass

    try:
#    minimize:
#
#        c @ x
#
#    such that:
#
#        A_ub @ x <= b_ub
#        A_eq @ x == b_eq
#        lb <= x <= ub
#
#    By default "lb = 0" and "ub = None" unless specified with "bounds".

        ret = linprog( c
                     , A_ub    = A_ub
                     , b_ub    = b_ub
                     , A_eq    = A_eq
                     , b_eq    = b_eq
#                     , bounds  = bounds
#                     , method  = 'interior-point' # This results in cyclic transfers
                     , method  = 'revised simplex'
#                     , method  = 'simplex'
                     , options = { 'tol'      : error_tolerance
                                 , 'maxiter'  : 100000
#                                 , 'bland'    : True
#                                 , 'presolve' : True
#                                 , 'pivot'    : 'bland'
                                 }
                     )
    except ValueError:
        Debug_Log()
        raise Portfolio_Rebalance_Exception

    # If cyclic, the flow problem can still potentially be solved, so that's not necessarily an issue
    # However there are still cases, when we can't solve it
    if not ret.success:
        Debug_Log()
        raise Portfolio_Rebalance_Exception

    flow_vals          = ret.x[:2*num_of_tradecons]
    ordered_asset_vals = ret.x[-num_of_currencies:]

    return flow_vals, flow_asset_pairs, C, (ordered_asset_vals, ordered_asset_names, ret.message)
