# Setup logging
from ..log_func import logger

########################################
from .data_numpy import Data
import numpy as np
from scipy import interpolate

import time


################################################################################
class Price_Record_Class_Base:
  ######################################
  def __init__( self, time_step_s ):
    self.max_stop_timestamp = None
    self.time_step_s        = time_step_s
    self.steps_to_stop      = {}
    self.prices             = {}

  ######################################
  def Get_Assets(self):
    return self.prices.keys()

  ######################################
  def Get_Stats_Dict(self):
    stats = { "max_stop_timestamp" : self.max_stop_timestamp
            , "time_step_s"        : self.time_step_s
            }
    for asset, steps_to_stop in self.steps_to_stop.items():
      stats[ "steps_to_stop " + asset ] = steps_to_stop
    for asset, prices in self.prices.items():
      stats[ "len(prices) " + asset ] = len(prices)

    return stats

  ######################################
  def Get_Steps_To_Start(self, asset):
    return len( self.prices[asset] )-1 + self.steps_to_stop[asset]

  ######################################
  def Get_Max_Steps_To_Start(self):
    return \
      max( [ self.Get_Steps_To_Start( asset )
             for asset in self.prices.keys()
           ]
         )

  ######################################
  def Get_First_Idx(self, asset):
    return self.Get_Max_Steps_To_Start() - self.Get_Steps_To_Start(asset)

  ######################################
  def Get_Last_Idx(self, asset):
    return self.Get_Max_Steps_To_Start() - self.steps_to_stop[asset]

  ######################################
  def Get_Prices_At_Idx( self
                       , idx
                       , extend_by_samples = 0
                       , normalize         = False
                       ):
    p = {}
    for asset in self.prices.keys():
      first_idx = self.Get_First_Idx( asset )
      last_idx  = self.Get_Last_Idx( asset )
      if idx < first_idx:
        continue
      if idx > last_idx + extend_by_samples:
        continue

      p[asset] = self.prices[asset][ min( last_idx, idx ) - first_idx ]
      if normalize:
        p[asset] /= self.prices[asset][0]

    return p

  ######################################
  def Get_Price_Vector( self
                      , asset
                      , up_to_but_not_including_time = None
                      ):
    end_idx   = self.Get_Length( up_to_but_not_including_time )

    p_start_idx = self.Get_First_Idx( asset )
    p_end_idx   = self.Get_Last_Idx(  asset ) + 1

    c_end_idx   = min(   end_idx,   p_end_idx )
    
    return self.prices[asset][ :c_end_idx-p_start_idx ]

  ######################################
  def Get_Price_Matrix( self
                      , reference_asset                       = None
                      , max_history                           = None
                      , up_to_but_not_including_time          = None
                      , extend_price_to_end_by_this_many_secs = 0
                      , extend_by_samples                     = 0
                      ):
    extend_to_end_by_samples = int( np.floor( extend_price_to_end_by_this_many_secs / self.time_step_s ) )

    abs_m_end_idx   = self.Get_Length( up_to_but_not_including_time )
    abs_m_start_idx = 0
    if not max_history is None:
      abs_m_start_idx = int( max( 0
                                , abs_m_end_idx - max(0, max_history)
                                )
                           )

    assets = []
    for asset in self.prices.keys():
      if self.Get_First_Idx( asset ) >= abs_m_end_idx:
        continue
      if self.Get_Last_Idx( asset ) < abs_m_start_idx:
        continue
      assets.append( asset )

    col_num = abs_m_end_idx - abs_m_start_idx
    row_num = len( assets )
    if not reference_asset is None:
      row_num += 1
    dim = ( row_num, col_num )

    c = np.zeros( dim )
    for ii, asset in enumerate(assets):
      # Example:
      #       |--|    <- one asset
      #    |--|--|--| <- all timestamps
      #   /  /  /  /
      #  0  1  2  3
      abs_p_start_idx = self.Get_First_Idx( asset )
      abs_p_end_idx   = self.Get_Last_Idx(  asset ) + 1
      abs_start_idx = max( abs_m_start_idx, abs_p_start_idx )
      abs_end_idx   = min(   abs_m_end_idx,   abs_p_end_idx )
      p_start_idx = abs_start_idx - abs_p_start_idx
      p_end_idx   = abs_end_idx   - abs_p_start_idx
      m_start_idx = abs_start_idx - abs_m_start_idx
      m_end_idx   = abs_end_idx   - abs_m_start_idx
      c[ ii, m_start_idx:m_end_idx ] = self.prices[asset][ p_start_idx:p_end_idx ]
      if abs_m_end_idx - extend_to_end_by_samples <= abs_end_idx < abs_m_end_idx:
        c[ ii, m_end_idx:col_num ] = self.prices[asset][-1]
      else:
        c[ ii, m_end_idx:m_end_idx + extend_by_samples ] = self.prices[asset][-1]

    if not reference_asset is None:
      assets.append( reference_asset )
      c[-1,:] = np.ones( col_num )

    price_timestamp_series = \
      self.Get_Timestamp_Series_Range( abs_m_start_idx, abs_m_end_idx )

    return assets, c, price_timestamp_series


################################################################################
class Price_Record_With_Timestamp_Series_Class(Price_Record_Class_Base):
  ######################################
  def __init__( self, time_step_s ):
    super().__init__(time_step_s)

  ######################################
  def Get_Length( self
                , up_to_but_not_including_time = None
                ):
    if up_to_but_not_including_time is None:
      return len( self.timestamp_series )

    return np.count_nonzero( self.timestamp_series < up_to_but_not_including_time )

  ######################################
  def Get_Timestamp_Series_Range( self, start_idx, stop_idx ):
    return self.timestamp_series[ start_idx : stop_idx ]

  ######################################
  def Get_Timestamp_Series( self, asset = None ):
    if asset is None:
      return self.timestamp_series

    stop_idx  = len( self.timestamp_series ) - self.steps_to_stop[asset]
    start_idx = stop_idx - len( self.prices[asset] )

    # Example:
    #       |--|    <- one asset
    #    |--|--|--| <- all timestamps
    #   /  /  /  /
    #  0  1  2  3
    return self.Get_Timestamp_Series_Range( start_idx, stop_idx )

  ######################################
  def Get_Min_Start_Timestamp(self):
    return self.timestamp_series[0]

  min_start_timestamp = property(Get_Min_Start_Timestamp)


################################################################################
class Price_Record_Class(Price_Record_Class_Base):
  ######################################
  def __init__( self, time_step_s ):
    super().__init__(time_step_s)

  ######################################
  def Get_Length( self
                , up_to_but_not_including_time = None
                ):
    max_len = self.Get_Max_Steps_To_Start()+1

    if up_to_but_not_including_time is None:
      return max_len

    delta_t = max( 0
                 , up_to_but_not_including_time - self.min_start_timestamp
                 )
    nom_len = int( np.ceil( delta_t / self.time_step_s ) )

    return min( nom_len, max_len )

  ######################################
  def Get_Timestamp_Series_Range( self, start_idx, stop_idx ):
    return self.Get_Timestamp_At_Idx( start_idx ) + np.arange( stop_idx-start_idx ) * self.time_step_s

  ######################################
  def Get_Timestamp_Series( self, asset = None ):
    if asset is None:
      return self.max_stop_timestamp - self.time_step_s * np.arange( self.Get_Max_Steps_To_Start(), -1, -1 )

    # Example:
    #       |--|    <- one asset
    #    |--|--|--| <- all timestamps
    #   /  /  /  /
    #  0  1  2  3
    return self.max_stop_timestamp - self.time_step_s * np.arange( self.Get_Steps_To_Start(asset), self.steps_to_stop[asset]-1, -1 )

  ######################################
  def Get_Timestamp_At_Idx(self, idx):
    return self.Get_Min_Start_Timestamp() + self.time_step_s * idx

  ######################################
  def Get_Min_Start_Timestamp(self):
    return self.max_stop_timestamp - self.time_step_s * self.Get_Max_Steps_To_Start()

  ######################################
#  def Get_Start_Timestamp(self, asset):
#    return self.max_stop_timestamp - self.time_step_s * self.Get_Steps_To_Start( asset )

  min_start_timestamp = property(Get_Min_Start_Timestamp)
  timestamp_series    = property(Get_Timestamp_Series)
 

################################################################################
# Price
class Price_Setup():
  ID        = "price"
  time_name = "timestamp"
  # Assets not having enough trade volume have to be marked with "NA"
  # Assets for which we don't have data will be nan (empty, when written to csv)
  NA_val    = 0.


################################################################################
class Price_Data(Data):
  ######################################
  def __init__( self
              , Setup = Price_Setup
              , path  = '.'
              ):
    self.sd = np.array( []
                      , dtype = [ ( Setup.time_name, np.float64 ) ]
                      )

    self.setup = Setup
    self.path  = path


  ######################################
  def Read_CSV( self
              , file_name = None
              ):
    Data.Read_CSV( self
                 , file_name
                 )
    self.sd.sort( order = self.setup.time_name )


  ######################################
  def Get_Latest_Valid_Timestamp( self
                                , asset_name = None
                                ):
    if self.Is_Empty():
      return

    time_series = self.sd[self.setup.time_name]

    if not asset_name is None:
      # Let's see if we already have that asset
      if not asset_name in self.sd.dtype.names:
        # Nope
        return
            
      # We do!
      # Let's take the not NaN values
      time_series = time_series[ np.invert( np.isnan( self.sd[asset_name] )
                                          | (self.sd[asset_name] == self.setup.NA_val)
                                          )
                               ]

    # Do we have actual values left?
    if time_series.size == 0:
      # Nope
      return

    max_time = time_series.max()

    if np.isnan(max_time):
      return

    return max_time


  ######################################
  def Add_New_Asset( self
                   , asset
                   , t_s
                   , price
                   , prefer_old_over_new = False
                   ):
    if len(t_s) == 0 or len(price) == 0:
      return

    current_assets = self.Get_Assets()
    current_time_series = self.sd[ self.setup.time_name ]
    # numpy's unique returns a sorted array!!!
    new_time_series = np.unique( np.concatenate( ( current_time_series
                                                 , t_s
                                                 )
                                               )
                               )

    if not asset in current_assets                      \
       or                                               \
       new_time_series.size != current_time_series.size \
       or                                               \
       np.any(new_time_series != current_time_series):
      # Allocate memory
      dtype = self.sd.dtype.descr
      if not asset in current_assets:
        dtype += [ (asset, '<f8') ]

      new_sd = np.empty( ( len(new_time_series), )
                       , dtype = dtype
                       )
      # Fill it up
      new_sd.fill(np.nan)
      new_sd[ self.setup.time_name ] = new_time_series

      if current_assets:
        mask = np.in1d( new_sd[ self.setup.time_name ]
                      , self.sd[ self.setup.time_name ]
                      , assume_unique = True
                      )
        new_sd[ current_assets ][ mask ] = self.sd[ current_assets ]
      self.sd = new_sd

    # We fill up the <asset> column with the new data
    # This is the last timestamp with proper old data
    old_max = self.Get_Latest_Valid_Timestamp( asset )
    # MIGHT BE NONE!
    if old_max is None:
      old_max = self.sd[ self.setup.time_name ].min() - 1
    # This is the first timestamp with new data
    new_min = min(t_s)

    mask = np.in1d( self.sd[ self.setup.time_name ]
                  , t_s
                  , assume_unique = True
                  )
    start_idx = 0

    if prefer_old_over_new:
      while start_idx < len(t_s) and t_s[start_idx] <= old_max:
        start_idx += 1
      mask &= self.sd[ self.setup.time_name ] > old_max

    self.sd[ asset ][ mask ] = price[ start_idx: ]


  ######################################
  def Get_Assets( self ):
    assets = list(self.sd.dtype.names)

    try:
      assets.remove( self.setup.time_name )
    except ValueError:
      pass

    return assets


  ######################################
  def Get_Relevant_Assets( self
                         , max_price_age = 4*60*60
                         ):
    # Get asset list from price dataframe
    # Weed out the low volume entries and the old ones
    asset_list = []
    for asset in self.Get_Assets():
      if self.sd[asset][-1] == self.setup.NA_val:
        continue

      last_valid_timestamp = self.Get_Latest_Valid_Timestamp( asset )

      # See if last valid timestamps is more than 4 hours old
      if last_valid_timestamp is None or last_valid_timestamp < time.time() - max_price_age:
        continue

      asset_list.append( asset )

    return asset_list


  ######################################
  def Get_Prices( self
                , asset
                , time_step_s
                , history                         = None
                , timestamp_multiple_of_time_step = True
                ):
    if self.Is_Empty():
      return None, []

    ################
    # Fill missing prices
    timestamps = self.sd[self.setup.time_name]
    prices     = self.sd[asset]

    if len(timestamps) == 0 or history == 0:
      return None, []

    logger.debug( "Setting up price filling" )

    last_timestamp = timestamps[ -1 ]
    if timestamp_multiple_of_time_step:
      last_timestamp -= timestamps[ -1 ] % time_step_s

    out_len = int( (last_timestamp - timestamps[0]) / time_step_s ) + 1
    if not history is None:
      out_len = min( out_len, history )

    first_timestamp = last_timestamp - (out_len-1) * time_step_s

    new_timestamps = np.linspace( first_timestamp
                                , last_timestamp
                                , out_len
                                )

    mask = np.invert( np.isnan(prices)
                    | (prices == self.setup.NA_val)
                    )

    f = interpolate.interp1d( timestamps[mask]
                            , prices[mask]
                            , kind          = 'previous'
                            , bounds_error  = False
                            , fill_value    = 'extrapolate'
                            , assume_sorted = True
                            )

    logger.debug( "Filling prices" )

    prices_out = f(new_timestamps)

    logger.debug( "Prices filled for asset %s"
                , asset
                )

    return last_timestamp, prices_out


  ######################################
  def Get_Prices_For_Assets( self
                           , timeframe_sec
                           , max_history           = None
                           , try_to_include_assets = None
                           ):
    ################
    # Prepare empty result class/object
    price_record = Price_Record_Class( timeframe_sec )

    ################
    # Narrow down assets
    assets = self.Get_Assets()
    if not try_to_include_assets is None:
      assets = set(assets) & set(try_to_include_assets)

    if not assets:
      return price_record

    ################
    # Get prices
    logger.debug( "Getting prices started %s"
                , str(assets)
                )
    prices     = {}
    stop_times = {}
    for asset in assets:
      logger.debug( "Getting prices for asset %s"
                  , asset
                  )

      t_end, p =\
        self.Get_Prices( asset
                       , timeframe_sec
                       , max_history
                       )
      if t_end is None:
        continue

      prices[asset]     = p
      stop_times[asset] = t_end

    logger.debug( "Getting prices done, postprocessing" )

    price_record.max_stop_timestamp = max( stop_times.values() )

    for asset in stop_times.keys():
      logger.debug( "Starting postprocessing asset %s"
                  , asset
                  )

      steps_to_stop  = int( round( ( price_record.max_stop_timestamp - stop_times[asset] ) / timeframe_sec ) )

      if steps_to_stop > max_history-1:
        continue

      price_record.steps_to_stop[asset] = steps_to_stop

      samples_to_start = steps_to_stop + len(prices[asset])
      trim_step_count = max( 0, samples_to_start - max_history )

      price_record.prices[asset]        = prices[asset][trim_step_count:]

    logger.debug( "Postprocessing done" )

    return price_record


  ######################################
  def Mark_NA( self
             , start_time
             , exclude_assets
             ):
    self.sd[ [ asset for asset in list(self.sd.dtype.names)
               if not asset in exclude_assets + [ self.setup.time_name ]
             ]
           ][ self.sd[self.setup.time_name] >= start_time ] = self.setup.NA_val


#  ######################################
#  def Get_Max_Time( self ):
#    max_time = self.sd[ self.setup.time_name ].max()
#
#    return 0 if np.isnan(max_time) else max_time
