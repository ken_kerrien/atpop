from ..EMA.backtest                     import Calculate_Portfolio_Frac_Precursor_Series

from .setup                             import Setup
from .compute                           import Consolidate_Single_Asset_Portfolio_Frac_Precursor_Strategies

from atpop.backtest.compute             import Calculate_Portfolio_Frac_Precursor_Strategies_Series_For_Pars_Strategies_Series,\
                                               Calculate_Backtest_From_Portfolio_Frac_Precursor_Strategies_Series
from atpop.backtest.plot                import Plot_Portfolio_Value_Backtest
from atpop.lib.data.portfolio_frac_data import Portfolio_Frac_Data           ,\
                                               Portfolio_Frac_Precursor_Data


################################################################################
def Calculate_And_Plot_Backtest_For_Pars_Strategies_Series( assets_price_time
                                                          , assets_price_record
                                                          , pars_strategies_time
                                                          , pars_strategies_series
                                                          , reference_asset
                                                          , trade_fee
                                                          , portfolio_frac_tolerance = 0.03  # %3 percent
                                                          , initial_value            = 1.
                                                          , disp_hist                = 0
                                                          , save_portfolio_frac_data = False
                                                          , setup                    = Setup
                                                          ):
    ####################################
    # Wrapper function
    f = lambda st, pars, prices, utbnii : \
        Calculate_Portfolio_Frac_Precursor_Series( st, pars, prices, utbnii, reference_asset )


    ####################################
    # Actually calculate the portfolio value
    trade_idx_series,\
    portfolio_frac_prec_strategies_series,\
    (_initial_price_idx,) =\
        Calculate_Portfolio_Frac_Precursor_Strategies_Series_For_Pars_Strategies_Series( assets_price_time
                                                                                       , assets_price_record
                                                                                       , pars_strategies_time
                                                                                       , pars_strategies_series
                                                                                       , f
                                                                                       )


    ####################################
    # fixing portfolio_frac_prec_strategies_series
    for idx in range(len(portfolio_frac_prec_strategies_series)):
        portfolio_frac_prec_strategies = portfolio_frac_prec_strategies_series[idx]

        portfolio_frac_prec_strategies_series[idx] =\
            Consolidate_Single_Asset_Portfolio_Frac_Precursor_Strategies( portfolio_frac_prec_strategies
                                                                        , setup.strategy
                                                                        , reference_asset
                                                                        )


    ####################################
    # Actually calculate the portfolio value
    portfolio_val,\
    time_and_portfolio_fracs = \
        Calculate_Backtest_From_Portfolio_Frac_Precursor_Strategies_Series( assets_price_time
                                                                          , assets_price_record
                                                                          , trade_idx_series
                                                                          , portfolio_frac_prec_strategies_series
                                                                          , reference_asset
                                                                          , trade_fee
                                                                          , portfolio_frac_tolerance
                                                                          , initial_value
                                                                          )


    ####################################
    # Plot
    Plot_Portfolio_Value_Backtest( assets_price_time
                                 , portfolio_val
                                 , initial_value
                                 , disp_hist = disp_hist
                                 )


    ####################################
    if save_portfolio_frac_data:
        time_and_portfolio_frac_prec_strategies = \
            zip( [ assets_price_time[idx] for idx in trade_idx_series ]
               , portfolio_frac_prec_strategies_series
               )

        pfpd = Portfolio_Frac_Precursor_Data()
        for te, portfolio_frac_strategies in time_and_portfolio_frac_prec_strategies:
            for strategy, portfolio_frac_precursor in portfolio_frac_strategies.items():
                pfpd.Add_Entry( portfolio_frac_precursor, te, strategy )
        pfpd.To_CSV()

        pfd = Portfolio_Frac_Data()
        pfd.setup.ID = "portfolio_frac_target"
        for te, portfolio_frac in time_and_portfolio_fracs:
            pfd.Add_Entry( portfolio_frac, te )
        pfd.To_CSV()
