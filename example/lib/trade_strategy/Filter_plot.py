import numpy as np
import matplotlib.pyplot as plt


################################################################################
def Draw_Buy_Sell_Signals(ax, trade_signal_idx, buy_sell_signal_series, t):
    for x, val in zip(trade_signal_idx, buy_sell_signal_series):
        if val == "b":
            ax.axvline(x=t[x], color='g')
        elif val == "s":
            ax.axvline(x=t[x], color='r')


################################################################################
def Plot_Backtest_Filter( t
                        , series
                        , filter_fast
                        , filter_slow
                        , filter_diff_rel
                        , trade_signal_idx
                        , buy_sell_signal_series
                        , asset_val
                        , reference_util_scaled = None
                        , util_scaled           = None
                        , title                 = None
                        , xlim                  = None
                        , xlabel                = None
                        ):
#    fig, ax_arr = plt.subplots(3, sharex=True)
    if not reference_util_scaled is None and not util_scaled is None:
        fig, ax_arr = plt.subplots(3, sharex=True)
    else:
        fig, ax_arr = plt.subplots(2, sharex=True)

    ax_idx = 0
    ##############################################
    Draw_Buy_Sell_Signals( ax_arr[ax_idx]
                         , trade_signal_idx
                         , buy_sell_signal_series
                         , t
                         )

    ax_arr[ax_idx].plot( t,      series, 'b'
                       , t, filter_fast, 'g'
                       , t, filter_slow, 'r'
                       )
    #ax_arr[ax_idx].semilogy(t, c, t, ema_s_fast, t, ema_s_slow)

    ax_arr[ax_idx].grid()
    ax_arr[ax_idx].set_xlabel('time')
    ax_arr[ax_idx].set_ylabel('asset price')
        
    ax1v = ax_arr[ax_idx].twinx()
    ax1v.plot(t, asset_val, 'k')
    #ax1v.semilogy(t, asset_val, 'k')

    ax1v.set_ylabel('asset val')

    ax_arr[ax_idx].set_ylim(ax1v.get_ylim())


    ax_idx += 1
    ##############################################
    Draw_Buy_Sell_Signals( ax_arr[ax_idx]
                         , trade_signal_idx
                         , buy_sell_signal_series
                         , t
                         )

    ax_arr[ax_idx].plot(t, filter_diff_rel, 'k')

    ax_arr[ax_idx].axhline(y=0, color='r')


    ax_idx += 1
    ##############################################
    if not reference_util_scaled is None and not util_scaled is None:
        Draw_Buy_Sell_Signals( ax_arr[ax_idx]
                             , trade_signal_idx
                             , buy_sell_signal_series
                             , t
                             )

        ax_arr[ax_idx].plot( t[-len(util_scaled):]
                           , util_scaled
                           , t[-len(reference_util_scaled):]
                           , reference_util_scaled
                           )

        ax_arr[ax_idx].grid()

    #fig.tight_layout()
    if not title is None:
        fig.suptitle( title )

    if not xlim is None:
        for ax in ax_arr:
            ax.set_xlim(xlim)

    if not xlabel is None:
        for ax in ax_arr:
            ax.set_xlabel(xlabel)

    plt.show()
