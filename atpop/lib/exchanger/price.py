# Setup logging
from ..log_func import log_exception_info
from ..log_func import log_exception_error

from ..data.price_data          import Price_Data

from .utils.asset_pair_handler  import Asset_Pair_Handler
from .utils.portfolio_handler   import Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs
from .exchange.utils            import timeframes

from .base                      import Exchanger_Base


import time


################################################################################
class Exchanger_Price( Exchanger_Base ):
  ##############################################################################
  def __init__( self
              , **kwargs
              ):
    super().__init__( **kwargs )

    self.Asset_Price_Setup = kwargs["Asset_Price_Setup"]
    STATE_ARTIFACTS_PATH   = kwargs["STATE_ARTIFACTS_PATH"]

    ###################################
    # Output
    self.price_data = Price_Data( path = STATE_ARTIFACTS_PATH )
    try:
      self.price_data.Read_CSV()
    except IOError as e:
      log_exception_info(e)


  ##############################################################################
  def _Update_Price_Data( self ):
    ############################################################################
    # STEP 1: Figure out which assets are still relevant, that is traded at
    # reasonable volumes
    tickers = self.ex.Fetch_Tickers()

    asset_pair_handler = Asset_Pair_Handler( self.bn, self.qn )
    asset_pair_handler.Generate_Asset_Pairs( tickers.keys() )
    asset_pair_handler.Filter_To_Contain_Asset( self.Asset_Price_Setup.reference_asset )
    asset_pair_handler.Extract_Volumes( tickers
                                      , self.qvn, self.bvn
                                      , filter_flag = True
                                      )

    if self._Get_Param_True('debug_update_price_data'):
      debug_asset_pair_volumes        = [ dict(ap) for ap in asset_pair_handler.asset_pairs ]

    asset_pair_handler.Query_Exchange_Rate( self.ex
                                          , tickers
                                          , filter_flag = True
                                          )

    if self._Get_Param_True('debug_update_price_data'):
      debug_asset_pair_exchange_rates = [ dict(ap) for ap in asset_pair_handler.asset_pairs ]

    vol_portfolio    = \
      Calculate_Volume_Portfolio( asset_pair_handler.asset_pairs
                                , self.Asset_Price_Setup.reference_asset
                                , self.qn, self.bn
                                , self.qvn, self.bvn
                                )

    if self._Get_Param_True('debug_update_price_data'):
      import os
      from datetime import datetime

      os.makedirs( os.path.abspath(self.DEBUG_ARTIFACTS_PATH)
                 , exist_ok = True
                 )

      with open( os.path.join( self.DEBUG_ARTIFACTS_PATH, 'update_price_data.log' )
               , 'a'
               ) as f:
        def log_local_var(var_name, vars_dict=locals()):
          f.write("#" + var_name + " = \\\n" )
          f.write( str( vars_dict[var_name] ) + "\n" )

        f.write("#"*20 + "\n")
        f.write( str(             time.time() ) + "\n" )
        f.write( str(datetime.fromtimestamp(time.time())) + "\n" )
        log_local_var(     "debug_asset_pair_volumes")
        log_local_var("debug_asset_pair_exchange_rates")
        log_local_var(          "vol_portfolio")
        f.write("#"*10 + "\n")
        f.write("#"*10 + "\n")

    vol_portfolio_conv = \
      Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( vol_portfolio
                                                            , self.Asset_Price_Setup.reference_asset
                                                            , asset_pair_handler.asset_pairs
                                                            , self.qn, self.bn
                                                            )

    assets = [ asset
               for asset, val in vol_portfolio_conv.items()
               if val >= self.Exchange_Setup.th_val_in_ref
             ]


    #######################################################
    # STEP 2: Query historical price data for relevant assets
    a_priori_max_time = self.price_data.Get_Latest_Valid_Timestamp()

    for asset_name in assets:
      # We are about to store historical asset data
      last_valid_timestamp       = self.price_data.Get_Latest_Valid_Timestamp( asset_name )
      start_of_history_timestamp = time.time() - self.Asset_Price_Setup.history * timeframes[self.Asset_Price_Setup.timeframe]

      if last_valid_timestamp is None:
        since_s = start_of_history_timestamp
      else:
        since_s = max( last_valid_timestamp + timeframes[self.Asset_Price_Setup.timeframe]/2.
                     , start_of_history_timestamp
                     )

      t_s, price = self.ex.Fetch_Historical_Price( asset_name
                                                 , self.Asset_Price_Setup.reference_asset 
                                                 , tickers.keys()
                                                 , self.Asset_Price_Setup.timeframe
                                                 , since_s
                                                 )

      self.price_data.Add_New_Asset( asset_name
                                   , t_s
                                   , price
                                   )

    # Assets not having enough trade volume have to be marked with "NA"
    self.price_data.Mark_NA( a_priori_max_time
                           , assets
                           )


############################################################################
def Calculate_Volume_Portfolio( volume_asset_pairs
                              , reference_asset
                              , qn, bn
                              , qvn, bvn
                              ):
  portfolio = {}

  for asset_pair in volume_asset_pairs:
    base = asset_pair[bn]
    quote = asset_pair[qn]

    if base not in portfolio:
      portfolio[base] = 0
    if quote not in portfolio:
      portfolio[quote] = 0

    if not asset_pair[bvn] is None:
      portfolio[base]  += asset_pair[bvn]
    if not asset_pair[qvn] is None:
      portfolio[quote] += asset_pair[qvn]

  if reference_asset in portfolio:
    del portfolio[reference_asset]

  return portfolio
