# Setup logging
from ...log_func import logger


########################################
from .setup           import Setup
from ..par_defs       import Extract_Par,\
                             Convert_Pars_Strategies_To_Par_Dict_Strategies
from ..name_mangling  import Add_Suffix_To_Name

from ..Filter_compute import Calculate_Score2
from .backtest        import Calculate_Backtest_With_Util_Signals
from platypus         import NSGAII, SPEA2, Problem, Real, nondominated

import numpy as np


################################################################################
class Param_Opter():
    ####################################
    def __init__( self
                , price_data
                , history
                , opt_setup
                , timeframe_sec
                , opt_par_data
                , max_price_age = 4*60*60 # 4 hours
                , setup         = Setup
                ):
        self.assets_price_record = None

        ############
        self.price_data      = price_data
        self.opt_par_data    = opt_par_data

        self.history         = int( history )

        self.timeframe_sec   = timeframe_sec

        self.half_life       = opt_setup.half_life
        self.trade_fee       = opt_setup.trade_fee
        self.opt_evals       = opt_setup.evaluations

        ############
        self.setup           = setup

        self.max_price_age   = max_price_age


    ####################################
    def _Update_Prices( self
                      , assets_price_record
                      ):
        if assets_price_record is None:
            return

        self.assets_price_record = assets_price_record


    ####################################
    def _Update( self ):
        # Get asset list from prices
        # Weed out the low volume entries and the old ones
        relevant_assets = self.price_data.Get_Relevant_Assets( self.max_price_age )
        if not relevant_assets:
            all_assets_at_start = len( self.price_data.Get_Assets() )
            if all_assets_at_start > 0:
                logger.info( "We have %d assets in price data, but none relevant"
                           , all_assets_at_start
                           )
            return True

        self._Update_Prices( self.price_data.Get_Prices_For_Assets_On_Same_Time_Scale( self.timeframe_sec
                                                                                     , history                 = self.history
                                                                                     , try_to_include_assets   = relevant_assets
                                                                                     #, extend_to_max_stop_time = False
                                                                                     )[1]
                           )
        return self.assets_price_record is None


    ################################################################################
    def Multiobjective_Func( self
                           , pars
                           ):
        _total_portfolio_val,\
        trade_idx_series,\
        _portfolio_frac_precursor_series,\
        _fast, _slow, _diff_rel,\
        util_scaled =\
            Calculate_Backtest_With_Util_Signals( None
                                                , self.assets_price_record
                                                , pars
                                                , self.setup.asset
                                                , "reference"
                                                , self.trade_fee
                                                , self.half_life
                                                #, portfolio_frac_tolerance
                                                #, initial_value
                                                )

        N_fast = Extract_Par( self.setup.par_defs, pars, "N_fast" )
        N_slow = Extract_Par( self.setup.par_defs, pars, "N_slow" )

        score = -Calculate_Score2( util_scaled )
        # [N_fast, N_slow]
        return ( [ np.sign(score)*score**2        # How much wealth do I have at the very end?
                 , np.sqrt(N_fast**2 + N_slow**2) # Fast and slow coefficients should be small
                 , -N_slow/N_fast                 # N_fast and N_slow should be dissimilar
                 , len(trade_idx_series)          # Number of trade events should be low
                 ]
               , [ 1 - N_slow/N_fast              # N_fast < N_slow
    #             , score - reference_score
                 ]
               )


    ####################################
    def Get_Next_Iteration_Of_Optimal_Pars_Strategies_And_Price_Time_Strategies( self ):
        if self._Update():
            return None, None

        price_time = self.price_data.Get_Latest_Valid_Timestamp( self.setup.asset )

        # Get latest optimal parameters
        strategy = Add_Suffix_To_Name( self.setup.strategy
                                     , self.setup.asset
                                     )
        _latest_par_dict_strategies, latest_price_time_strategies =\
            self.opt_par_data.Get_Latest_Par_Dict_Strategies_And_Price_Time_Strategies( strategy )
        assert len(latest_price_time_strategies) == 1
        if latest_price_time_strategies[ strategy ] == price_time:
            return None, None

        # two decision variables, four objectives, one constraint
        problem = Problem( 2, 4, 1 )
        problem.types[:] = [ Real( self.setup.par_defs["N_fast"][0]
                                 , self.setup.par_defs["N_fast"][1]
                                 )
                           , Real( self.setup.par_defs["N_slow"][0]
                                 , self.setup.par_defs["N_slow"][1]
                                 )
                           ]
        problem.constraints[:] = "<0"
        #problem.constraints[:] = ["<=0", "<0"]
        problem.function = lambda vars : self.Multiobjective_Func( vars )

        algorithm = NSGAII( problem )
        #algorithm = SPEA2(problem)
        algorithm.run( self.opt_evals )

        solutions = algorithm.result
        solutions = nondominated( solutions )
        solutions = [s for s in solutions if s.feasible]
        solutions = sorted( solutions
                          , key=lambda item: item.objectives[0]
                          )

        ############
        strategy = Add_Suffix_To_Name( self.setup.strategy, self.setup.asset )
        pars_strategies       = { strategy : solutions[0].variables }
        price_time_strategies = { strategy : price_time }

        return pars_strategies, price_time_strategies


    ####################################
    def Get_Next_Iteration_Of_Optimal_Par_Dict_Strategies_And_Price_Time_Strategies( self ):
        pars_strategies, price_time_strategies = \
            self.Get_Next_Iteration_Of_Optimal_Pars_Strategies_And_Price_Time_Strategies()

        if pars_strategies is None:
            return None, None

        par_dict_strategies = \
            Convert_Pars_Strategies_To_Par_Dict_Strategies( self.setup.par_defs
                                                          , pars_strategies
                                                          )
        return par_dict_strategies, price_time_strategies
