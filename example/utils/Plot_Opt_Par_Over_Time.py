import sys, os
sys.path.append( os.path.join( os.path.dirname(__file__)
                             , r'..'
                             )
               )

from utils.common                               import *

from atpop.lib.exchanger.exchange.utils         import timeframes

from atpop.lib.data.price_data                  import Price_Data
from atpop.lib.data.opt_par_data                import Opt_Par_Data ,\
                                                       Opt_Par_Setup

from lib.trade_strategy.EMA.setup               import Setup
#from lib.trade_strategy.multi_asset_EMA.setup   import Setup
from lib.trade_strategy.name_mangling           import Add_Suffix_To_Name

import numpy as np
import matplotlib.pyplot as plt


################################################################################
if __name__ == '__main__':
    asset = "LTC"

    opt_par_data = Opt_Par_Data( path = STATE_ARTIFACTS_PATH )
    opt_par_data.Read_CSV()

    assert asset in opt_par_data.Get_Assets()

    par_dict_series, time_series =\
        opt_par_data.Get_Par_Dict_Series_And_Time_Series( Add_Suffix_To_Name( Setup.strategy
                                                                            , asset
                                                                            )
                                                        )
    data = {}
    for par_dict in par_dict_series:
        for par_name, val in par_dict.items():
            if not par_name in data:
                data[par_name] = []
            data[par_name].append( val )

    time_series = np.asarray( time_series )
    time_series -= time_series[0]
    time_series /= 60.*60.*24.*28.

    fig, ax_arr = plt.subplots( len( data.keys() )
                              , sharex=True
                              )
    fig.suptitle(asset, fontsize='large')

    for ax_idx, data_name in enumerate( data.keys() ):
        ax_arr[ax_idx].grid()
        ax_arr[ax_idx].grid( which     = 'minor'
                           , axis      = 'x'
                           , linestyle = '-'
                           , linewidth = 0.2
                           )
        major_ticks = np.arange(0, time_series[-1]+1, 1)
        minor_ticks = np.arange(0, time_series[-1]+1, 0.25)
        ax_arr[ax_idx].set_xticks(major_ticks)
        ax_arr[ax_idx].set_xticks(minor_ticks, minor=True)

        ax_arr[ax_idx].set_xlabel('Time [month]')
        ax_arr[ax_idx].set_ylabel(data_name)

        ax_arr[ax_idx].plot( time_series
                           , data[data_name]
                           , '-o'
                           )

    plt.show()
