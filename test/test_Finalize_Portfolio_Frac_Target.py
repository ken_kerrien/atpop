import unittest

import sys
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )

from atpop.lib.exchanger.utils.finalize_portfolio_frac_target import Get_Portfolio_Frac_Target,\
                                                                     Get_Compound_Portfolio_Frac_Target


################################################################################
lg = "<>"
l = "<"

reference_asset = 'USDT'


################################################################################
class Case_1(unittest.TestCase):
    def setUp(self):
        pass

    def test(self):
        portfolio_frac_precursor = \
            { 'ADA'  : l
            , 'BCH'  : l
            , 'BSV'  : l
            , 'BTC'  : l
            , 'DASH' : l
            , 'DOGE' : l
            , 'EOS'  : l
            , 'ETC'  : l
            , 'ETH'  : l
            , 'IOTA' : l
            , 'LTC'  : l
            , 'NEO'  : l
            , 'ONT'  : l
            , 'QTUM' : l
            , 'TRX'  : l
            , 'XLM'  : l
            , 'XMR'  : l
            , 'XRP'  : l
            , 'ZEC'  : l
            , 'USDT' : lg
            }

        portfolio_frac = \
            { 'USDT' : 0.5
            , 'BLAH' : 0.5
            }

        portfolio_frac_target =\
            Get_Portfolio_Frac_Target( portfolio_frac_precursor
                                     , portfolio_frac
                                     , reference_asset
                                     )

        self.assertTrue( portfolio_frac_target == {'USDT': 1.0} )


################################################################################
class Case_2(unittest.TestCase):
    def setUp(self):
        pass

    def test(self):
        portfolio_frac_precursor = \
            { 'ADA'  : 1
            , 'BCH'  : 3
            , 'BSV'  : l
            }

        portfolio_frac = \
            { 'USDT' : 0.5
            , 'BLAH' : 0.5
            }

        portfolio_frac_target =\
            Get_Portfolio_Frac_Target( portfolio_frac_precursor
                                     , portfolio_frac
                                     , reference_asset
                                     )

        self.assertTrue( portfolio_frac_target == { 'ADA' : 0.25
                                                  , 'BCH' : 0.75
                                                  }
                       )


################################################################################
class Case_3(unittest.TestCase):
    def setUp(self):
        pass

    def test(self):
        portfolio_frac_precursor = \
            { 'ADA'  : 0.25
            , 'BCH'  : 0.75
            , 'BSV'  : l
            }

        portfolio_frac = \
            { 'USDT' : 0.5
            , 'BLAH' : 0.5
            }

        portfolio_frac_target =\
            Get_Portfolio_Frac_Target( portfolio_frac_precursor
                                     , portfolio_frac
                                     , reference_asset
                                     )

        self.assertTrue( portfolio_frac_target == { 'ADA' : 0.25
                                                  , 'BCH' : 0.75
                                                  }
                       )


################################################################################
class Case_4(unittest.TestCase):
    def setUp(self):
        pass

    def test(self):
        portfolio_frac_precursor = \
            { 'ADA'  : 0.2
            , 'BCH'  : 0.7
            , 'BSV'  : lg
            }

        portfolio_frac = \
            { 'USDT' : 0.5
            , 'BLAH' : 0.5
            }

        portfolio_frac_target =\
            Get_Portfolio_Frac_Target( portfolio_frac_precursor
                                     , portfolio_frac
                                     , reference_asset
                                     )

        self.assertTrue( portfolio_frac_target == { 'ADA' : 0.2
                                                  , 'BCH' : 0.7
                                                  , 'BSV' : 0.1
                                                  }
                       )


################################################################################
class Case_5(unittest.TestCase):
    def setUp(self):
        pass

    def test(self):
        portfolio_frac_precursor1 = \
            { 'ADA'  : 0.2
            , 'BCH'  : 0.7
            , 'BSV'  : lg
            }

        portfolio_frac_precursor2 = \
            { 'ADA'  : 0.25
            , 'BCH'  : 0.75
            , 'BSV'  : l
            }

        portfolio_frac_precursor_strategies = \
            { 1: portfolio_frac_precursor1
            , 2: portfolio_frac_precursor2
            }

        portfolio_frac = \
            { 'USDT' : 0.5
            , 'BLAH' : 0.5
            }

        portfolio_frac_target =\
            Get_Compound_Portfolio_Frac_Target( portfolio_frac_precursor_strategies
                                              , portfolio_frac
                                              , reference_asset
                                              )

        self.assertTrue( portfolio_frac_target == { 'ADA' : 0.225
                                                  , 'BCH' : 0.725
                                                  , 'BSV' : 0.05
                                                  }
                       )


################################################################################
class Case_6(unittest.TestCase):
    def setUp(self):
        pass

    def test(self):
        portfolio_frac_precursor = \
            { 'ZEC': 1.0
            , 'XMR': 1.0
            , 'ZRX': 1.0
            , 'QTUM': 1.0
            , 'XTZ': 1.0
            , 'ETH': 1.0
            , 'DASH': 1.0
            , 'BCH': 1.0
            , 'ONT': 1.0
            , 'BSV': 1.0
            , 'ETC': 1.0
            , 'TRX': 1.0
            , 'OMG': 1.0
            , 'EOS': 1.0
            , 'NEO': 1.0
            , 'ADA': 1.0
            , 'BTC': 1.0
            , 'LTC': 1.0
            , 'XEM': 1.0
            , 'XLM': 1.0
            , 'ICX': 1.0
            , 'XRP': 1.0
            , 'USD': 1.0
            }

        reference_asset = 'reference'

        portfolio_frac = \
            { reference_asset: 1.0 }

        portfolio_frac_target =\
            Get_Portfolio_Frac_Target( portfolio_frac_precursor
                                     , portfolio_frac
                                     , reference_asset
                                     )

        self.assertTrue( portfolio_frac_target == { 'NEO': 0.043478
                                                  , 'XEM': 0.043478
                                                  , 'TRX': 0.043478
                                                  , 'ADA': 0.043478
                                                  , 'ICX': 0.043478
                                                  , 'USD': 0.043478
                                                  , 'XLM': 0.043478
                                                  , 'ZRX': 0.043478
                                                  , 'ETH': 0.043478
                                                  , 'BSV': 0.043478
                                                  , 'DASH': 0.043478
                                                  , 'BCH': 0.043478
                                                  , 'ETC': 0.043478
                                                  , 'QTUM': 0.043478
                                                  , 'XMR': 0.043478
                                                  , 'LTC': 0.043478
                                                  , 'XTZ': 0.043478
                                                  , 'ZEC': 0.043478
                                                  , 'XRP': 0.043478
                                                  , 'ONT': 0.043478
                                                  , 'EOS': 0.043478
                                                  , 'OMG': 0.043478
                                                  , 'BTC': 0.043478
                                                  }
                       )


################################################################################
class Case_7(unittest.TestCase):
    def setUp(self):
        pass

    def test(self):
        portfolio_frac_precursor1 = \
            { 'ETC': 1.0
            , 'OMG': 1.0
            , 'XTZ': 1.0
            , 'ADA': 1.0
            , 'BCH': 1.0
            , 'ZEC': 1.0
            , 'XEM': 1.0
            , 'ICX': 1.0
            , 'XLM': 1.0
            , 'NEO': 1.0
            , 'XRP': 1.0
            , 'QTUM': 1.0
            , 'TRX': 1.0
            , 'LTC': 1.0
            , 'ZRX': 1.0
            , 'ONT': 1.0
            , 'BSV': 1.0
            , 'XMR': 1.0
            , 'EOS': 1.0
            , 'DASH': 1.0
            , 'BTC': 1.0
            , 'ETH': 1.0
            , 'reference': 1.0
            }

        portfolio_frac_precursor_strategies = \
            { 1: portfolio_frac_precursor1 }

        reference_asset = 'reference'

        portfolio_frac = \
            { reference_asset: 1.0 }

        portfolio_frac_target =\
            Get_Compound_Portfolio_Frac_Target( portfolio_frac_precursor_strategies
                                              , portfolio_frac
                                              , reference_asset
                                              )

        self.assertTrue( portfolio_frac_target == { 'EOS': 0.043478
                                                  , 'ONT': 0.043478
                                                  , 'reference': 0.043478
                                                  , 'TRX': 0.043478
                                                  , 'ADA': 0.043478
                                                  , 'BCH': 0.043478
                                                  , 'BTC': 0.043478
                                                  , 'XTZ': 0.043478
                                                  , 'BSV': 0.043478
                                                  , 'ICX': 0.043478
                                                  , 'XLM': 0.043478
                                                  , 'ZEC': 0.043478
                                                  , 'OMG': 0.043478
                                                  , 'XEM': 0.043478
                                                  , 'ETC': 0.043478
                                                  , 'XRP': 0.043478
                                                  , 'NEO': 0.043478
                                                  , 'ETH': 0.043478
                                                  , 'LTC': 0.043478
                                                  , 'QTUM': 0.043478
                                                  , 'DASH': 0.043478
                                                  , 'XMR': 0.043478
                                                  , 'ZRX': 0.043478
                                                  }
                       )


################################################################################
if __name__ == '__main__':
    unittest.main()
