import numpy as np
from scipy.optimize import linprog
from .utils import Generate_Asset_Vector         ,\
                   Generate_Conversion_Vector    ,\
                   Portfolio_Rebalance_Exception ,\
                   Generate_Connectivity_Matrix_And_Flow_Origin_Names_And_Flow_Asset_Pairs
from ..portfolio_handler import Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs


###############################################################################
# Our conversion graph
#  +----------------+
#  |                |
# BTC --- USDT --- ETH
#          |
#          +------ XRP

# Define a currency order
# BTC
# USDT
# ETH
# XRP

# Define states
# Before transactions
#  BTC : b0
# USDT : b1
#  ETH : b2
#  XRP : b3
# After transactions
#  BTC : a0
# USDT : a1
#  ETH : a2
#  XRP : a3
# Portfolio ratio after transactions
#  BTC : r0
# USDT : r1
#  ETH : r2
#  XRP : r3

################################################################################
# NOT NEEDED - Define adjacency matrix
#    from
# to
# (Only lower triangle, so matrix is directed)
#
#       BTC, USDT, ETH, XRP
# BTC     0     0    0    0
# USDT    1     0    0    0
# ETH     1     1    0    0
# XRP     0     1    0    0

################################################################################
# Directed conversion graph
#           f1
#  +------------------+
#  |   f0        f2   v
# BTC ---> USDT ---> ETH
#           |  f3     v f4
#           +------> XRP

# Flow vector and anti flow vector
#
# The value of the flow vector is measured in the reference asset!
# Why not in the asset the vector starts from (the origin asset)?
# Because that means that we could have flows, which are several
# 1000 in value, while others are fractions of one. Having values
# that difference in scale within the same matrix does not bode well
# for numerical stability. BUT, when we return the flow values, we
# back calculate the flows to be measured in their flow origin.
#
# The flow is considered positive if it flows "outwards" carrying currency
# away from the starting node
# f0 :  BTC -> USDT   f5 : USDT ->  BTC
# f1 :  BTC ->  ETH   f6 :  ETH ->  BTC
# f2 : USDT ->  ETH   f7 :  ETH -> USDT
# f3 : USDT ->  XRP   f8 :  XRP -> USDT
# f4 :  ETH ->  XRP   f9 :  XRP ->  ETH

# KCL for all the nodes
#  BTC :  1*f0 +  1*f1 +  0*f2 +  0*f3 +  0*f4   +  -g*f5 + -g*f6 +  0*f7 +  0*f8 +  0*f9   =  b0 - a0
# USDT : -g*f0 +  0*f1 +  1*f2 +  1*f3 +  0*f4   +   1*f5 +  0*f6 + -g*f7 + -g*f8 +  0*f9   =  b1 - a1
#  ETH :  0*f0 + -g*f1 + -g*f2 +  0*f3 +  1*f4   +   0*f5 +  1*f6 +  1*f7 +  0*f8 + -g*f9   =  b2 - a2
#  XRP :  0*f0 +  0*f1 +  0*f2 + -g*f3 + -g*f4   +   0*f5 +  0*f6 +  0*f7 +  1*f8 +  1*f9   =  b3 - a3

# Connectivity matrix (C)
# Flow - Anti flow
#  1  1  0  0  0  -g -g  0  0  0
# -g  0  1  1  0   1  0 -g -g  0
#  0 -g -g  0  1   0  1  1  0 -g
#  0  0  0 -g -g   0  0  0  1  1

# The equations
#            Cf = b - a
#        Cf + a = b
#       Cf + Ia = b
# [C I][fT aT]T = b



################################################################################
# State after transactions
#  BTC : max(0, r0 - e) <= a0 / total <= min(1, r0 + e)
# USDT : max(0, r1 - e) <= a1 / total <= min(1, r1 + e)
#  ETH : max(0, r2 - e) <= a2 / total <= min(1, r2 + e)
#  XRP : max(0, r3 - e) <= a3 / total <= min(1, r3 + e)

#  BTC : r0i <= a0 / total <= r0s
# USDT : r1i <= a1 / total <= r1s
#  ETH : r2i <= a2 / total <= r2s
#  XRP : r3i <= a3 / total <= r3s

#  BTC : r0i * (a0 + a1 + a2 + a3) <= a0 <= r0s * (a0 + a1 + a2 + a3)
# USDT : r1i * (a0 + a1 + a2 + a3) <= a1 <= r1s * (a0 + a1 + a2 + a3)
#  ETH : r2i * (a0 + a1 + a2 + a3) <= a2 <= r2s * (a0 + a1 + a2 + a3)
#  XRP : r3i * (a0 + a1 + a2 + a3) <= a3 <= r3s * (a0 + a1 + a2 + a3)

# The equations
# ri*1T*a <= a <= rs*1T*a

# Let's define M as
# [r0-1   r0   r0   r0
#    r1 r1-1   r1   r1
#    r2   r2 r2-1   r2
#    r3   r3   r3 r3-1] = r*1T - I = M

#    (ri*1T - I)a <= 0 <= (rs*1T - I)a
#            Mi a <= 0 <= Ms a

#   [MiT -MsT]T a <= 0
#           Mis a <= 0
# [0 Mis][fT aT]T <= 0


###############################################################################
def Rebalance_Portfolio( portfolio
                       , portfolio_frac_target
                       , asset_pairs
                       , reference_asset
                       , portfolio_frac_tolerance
                       , qn, bn
                       , fn, tn
                       , error_tolerance = 1e-6
                       , save_debug_data = False
                       , debug_path      = '.'
                       ):
  portfolio_in_ref = \
    Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( portfolio
                                                          , reference_asset
                                                          , asset_pairs
                                                          , qn, bn
                                                          )
  portfolio_sum_in_ref = sum( portfolio_in_ref.values() )
  portfolio_frac = { k:v/portfolio_sum_in_ref
                     for k,v in portfolio_in_ref.items() if v > 0
                   }

  ####################
  # Define helper variables

  ordered_asset_names = list( set( [asset_pair[qn] for asset_pair in asset_pairs]
                                 + [asset_pair[bn] for asset_pair in asset_pairs]
                                 )
                            )
  #ordered_asset_names = ['BTC', 'USDT', 'ETH', 'XRP']

  C, flow_origin_names, flow_asset_pairs =\
    Generate_Connectivity_Matrix_And_Flow_Origin_Names_And_Flow_Asset_Pairs( asset_pairs
                                                                           , ordered_asset_names
                                                                           , qn, bn
                                                                           , fn, tn
                                                                           )
  # incoming is negative, outgoing is positive
  #C = np.array( [ [ 1,  1,  0,  0,  0,  -g, -g,  0,  0,  0]
  #              , [-g,  0,  1,  1,  0,   1,  0, -g, -g,  0]
  #              , [ 0, -g, -g,  0,  1,   0,  1,  1,  0, -g]
  #              , [ 0,  0,  0, -g, -g,   0,  0,  0,  1,  1]
  #              ]
  #            )

  num_of_currencies, num_of_tradecons = np.shape(C)
  num_of_tradecons //= 2

  b = Generate_Asset_Vector(portfolio_frac,        ordered_asset_names)
  r = Generate_Asset_Vector(portfolio_frac_target, ordered_asset_names)
  #r = [0., 0., 0.5, 0.5]
  #r = [0.1, 0.2, 0.4, 0.3] # This failed the optimizer without  options = {"bland":True}
  #r = [0.3, 0.5, 0.2, 0] # This failed the optimizer


  ####################
  # Define matrices
  ##########
  # [C I][fT aT]T = b
  I = np.identity(num_of_currencies)
  A1 = np.concatenate( (C, I), axis=1 ) # put next to each other

  ##########
  #    (ri*1T - I)a <= 0 <= (rs*1T - I)a
  #            Mi a <= 0 <= Ms a
  ri = np.maximum( np.asarray(r) - portfolio_frac_tolerance, 0 )
  rs = np.minimum( np.asarray(r) + portfolio_frac_tolerance, 1 )
  # Let's keep zero desired portfolio fractions zeros
  # In other word, tolerances should not apply to zeros
  for ii in range(len(r)):
    if r[ii] == 0.:
      rs[ii] = 0.
  Mi = np.outer( ri, np.ones(num_of_currencies) ) - I
  Ms = np.outer( rs, np.ones(num_of_currencies) ) - I

  #   [MiT -MsT]T a <= 0
  #           Mis a <= 0
  Mis = np.concatenate( (Mi, -Ms) )

  # [0 Mis][fT aT]T <= 0
  Z = np.zeros((2*num_of_currencies, 2*num_of_tradecons))
  A2 = np.concatenate( (Z, Mis), axis=1 ) # put next to each other


  ####################
  # Define linprog matrices
  A_eq = A1

  b_eq = b

  A_ub = A2

  b_ub = np.zeros(2*num_of_currencies)

  c = np.concatenate( (np.zeros(2*num_of_tradecons), -np.ones(num_of_currencies)) )

  abs_max = np.sum( np.abs(b) )


  ####################
  # Linear Programming
  bounds = [ (0 , abs_max) ] * (2*num_of_tradecons) \
         + [ (0 , None   ) ] * num_of_currencies

  try:
#  minimize:
#
#    c @ x
#
#  such that:
#
#    A_ub @ x <= b_ub
#    A_eq @ x == b_eq
#    lb <= x <= ub
#
#  By default "lb = 0" and "ub = None" unless specified with "bounds".

    ret = linprog( c
                 , A_ub    = A_ub
                 , b_ub    = b_ub
                 , A_eq    = A_eq
                 , b_eq    = b_eq
                 , bounds  = bounds
#                 , method  = 'interior-point' # This results in cyclic transfers
                 , method  = 'revised simplex'
#                 , method  = 'simplex'
                 , options = { 'tol'      : error_tolerance
                             , 'maxiter'  : 100000
#                             , 'bland'    : True
#                             , 'presolve' : True
#                             , 'pivot'    : 'bland'
                             }
                 )
  except ValueError:
    raise Portfolio_Rebalance_Exception

  # If cyclic, the flow problem can still potentially be solved, so that's not necessarily an issue
  # However there are still cases, when we can't solve it
  if not ret.success:
    raise Portfolio_Rebalance_Exception

  flow_vals          = ret.x[:2*num_of_tradecons]
  ordered_asset_vals = ret.x[-num_of_currencies:]

  v_flow_vals = \
    Generate_Conversion_Vector( flow_origin_names
                              , reference_asset
                              , asset_pairs
                              , qn, bn
                              )

  v_ordered_asset_vals = \
    Generate_Conversion_Vector( ordered_asset_names
                              , reference_asset
                              , asset_pairs
                              , qn, bn
                              )

  flow_vals          = np.asarray(flow_vals)          * portfolio_sum_in_ref / v_flow_vals
  ordered_asset_vals = np.asarray(ordered_asset_vals) * portfolio_sum_in_ref / v_ordered_asset_vals

  return flow_vals, flow_asset_pairs, C, (ordered_asset_vals, ordered_asset_names, ret.message)
