# Setup logging
from .log_func import logger
from .log_func import log_exception_info
from .log_func import log_exception_error


########################################
import os
import time

INF = float('inf')

################################################################################
class Wait_For_Files():
    ########################################
    def __init__( self
                , file_paths
                , max_wait_time          = None
                , report_change_at_start = True
                ):
        self.file_paths             = file_paths
        self.file_mod_times         = [0] * len( file_paths )
        self.max_wait_time          = max_wait_time
        self.last_run_time          = 0
        self.report_change_at_start = report_change_at_start

        self.poll_period_s = 0.1 # Check at every 0.1
        self.stable_time_s = 2   # If file has been modified, but seems stable for 2 secs, let's report it


    ########################################
    def Wait(self, func):
        first_run = True
        while True:
            logger.debug( "Sleep, Data." )
            time.sleep( self.poll_period_s )

            # Let's see if any files changed since last time we looked at them
            mod_file_paths = []
            for ii, file_path in enumerate( self.file_paths ):
                try:
                    new_file_mod_time = os.path.getmtime( file_path )
                except OSError:
                    logger.debug( "Couldn't find file %s"
                                , file_path
                                )
                    continue

                assert new_file_mod_time >= self.file_mod_times[ii]

                if first_run and not self.report_change_at_start:
                    self.file_mod_times[ii] = new_file_mod_time
                    logger.debug( "Not reporting on file %s already existing at start, last modified %s"
                                , file_path
                                , time.ctime( new_file_mod_time )
                                )
                    continue

                if new_file_mod_time == self.file_mod_times[ii]:
                    logger.debug( "File %s has not changed since %s"
                                , file_path
                                , time.ctime( new_file_mod_time )
                                )
                    continue

                if new_file_mod_time + self.stable_time_s > time.time():
                    logger.debug( "File %s has changed, but not stable yet %s"
                                , file_path
                                , time.ctime( new_file_mod_time )
                                )
                    continue

                self.file_mod_times[ii] = new_file_mod_time

                logger.debug( "File %s has changed at %s"
                            , file_path
                            , time.ctime( new_file_mod_time )
                            )

                mod_file_paths.append( file_path )

            # Figure out how much longer to wait before calling back
            now = time.time()
            if self.max_wait_time is None:
                wait_time = INF
            else:
                wait_time = self.max_wait_time - (now - self.last_run_time)
            time_out = (wait_time <= 0) # Did we already run out of wait time?

            # If no files changed and we havn't timed out, then we're done for now
            if not mod_file_paths and not time_out:
                if wait_time < INF:
                    logger.debug( "Next wait time out in %d secs"
                                , wait_time
                                )
                continue

            # Something did happen
            # We'll do a callback
            if time_out:
                self.last_run_time = now # Book keeping for regular interval timeouts

            logger.debug( "Calling callback function" )

            if self.max_wait_time is None:
                func(mod_file_paths)
            else:
                func(mod_file_paths, time_out)

            ##########
            first_run = False
