"""
This service watches the historical price data. If it changes, it recalculates
the optimal parameters for the trade strategy, and writes that to the trade
algorithm parameters.
"""

# Setup logging
from ..lib.log_func import logger
from ..lib.log_func import log_exception_info
from ..lib.log_func import log_exception_error


########################################
from ..lib.wait_for_objects            import Wait_For_Files
from ..lib.exchanger.exchange.utils    import timeframes

from ..lib.data.price_data             import Price_Data
from ..lib.data.opt_par_data           import Opt_Par_Data


################################################################################
class Trade_Strategy_Updater( ):
    ############################################################################
    def __init__( self
                , Asset_Price_Setup
                , Opt_Setup
                , Portfolio_Setup
                , STATE_ARTIFACTS_PATH
                , DEBUG_ARTIFACTS_PATH
                , Trade_Strategy_Optimization
                ):
        ###################################
        # Input
        self.price_data = \
            Price_Data( path = STATE_ARTIFACTS_PATH )

        ###################################
        # Output
        self.opt_par_data = \
            Opt_Par_Data(  path = STATE_ARTIFACTS_PATH )
        try:
            self.opt_par_data.Read_CSV()
        except IOError as e:
            log_exception_info(e)

        ###################################
        self.optimizer = Trade_Strategy_Optimization( self.price_data
                                                    , Asset_Price_Setup.history
                                                    , Opt_Setup
                                                    , Portfolio_Setup
                                                    , timeframes[Asset_Price_Setup.timeframe]
                                                    , self.opt_par_data
                                                    , max_price_age = Asset_Price_Setup.max_recent_price_age
                                                    #, setup         = Setup
                                                    #, debug_file_name = 'opt_par_opt.csv'
                                                    , debug_path    = DEBUG_ARTIFACTS_PATH
                                                    )


    ############################################################################
    def Wait_For_Event(self):
        wo = Wait_For_Files( [self.price_data.Get_CSV_File_Path()] )
        wo.Wait( self._Opt_Par )


    ############################################################################
    def _Opt_Par(self, mod_file_paths):
        logger.info( "Change detected with %s"
                   , self.price_data.Get_CSV_File_Path()
                   )

        while True:
            ##############################
            # Read in historical price data
            try:
                self.price_data.Read_CSV()
            except IOError as e:
                log_exception_info(e)
                return

            logger.info( "Historical price data updated" )

            ##############################
            # Get new optimal parameters
            par_dict_strategies, price_time_strategies = \
                self.optimizer.Get_Next_Iteration_Of_Optimal_Par_Dict_Strategies_And_Price_Time_Strategies()

            if par_dict_strategies is None:
                logger.info( "No new optimum parameters calculated" )
                return

            logger.info( "New optimum parameters calculated" )

            ##############################
            # Write new optimal parameters
            if not self.opt_par_data.Add_Entries_If_Different( price_time_strategies
                                                             , par_dict_strategies
                                                             ):
                logger.info( "New optimum parameters same as previous" )
                continue

            try:
                self.opt_par_data.To_CSV()
                logger.info( "New optimal parameters written to %s"
                           , self.opt_par_data.Get_CSV_File_Path()
                           )
            except IOError as e:
                log_exception_error(e)
