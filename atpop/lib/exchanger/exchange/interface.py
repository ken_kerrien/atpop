# Setup logging
from ...log_func import logger
from ...log_func import log_exception_info
from ...log_func import log_exception_error


########################################
from .utils      import timeframes

import numpy as np
import datetime
import time

import ccxt
from ccxt import AuthenticationError,\
                 DDoSProtection,\
                 RequestTimeout,\
                 ExchangeNotAvailable,\
                 InvalidNonce,\
                 InvalidAddress

# CCXT errors

# Unrecoverable
# ExchangeError
# +---+ NotSupported
# +---+ AuthenticationError
# |   +---+ PermissionDenied
# +---+ InsufficientFunds
# +---+ InvalidAddress
# +---+ InvalidOrder
#     +---+ OrderNotFound

# Recoverable
# NetworkError
# +---+ DDoSProtection
# +---+ RequestTimeout
# +---+ ExchangeNotAvailable
# +---+ InvalidNonce


################################################################################
class Not_Connected_To_Exchange_Exception(Exception):
    pass

class Exchange_Does_Not_Support_Functionality(Exception):
    pass

class Portfolio_Fetch_Exception(Exception):
    pass


################################################################################
# Execute Function With Exchange Not Connected Handling
def EFWENCH( max_tries = 3 ):
    def EFWENCH_Decorator(func):
        def func_wrapper(self, *args, **kwargs):
            for try_cnt in range(max_tries, 0, -1):
                try:
                    return func(self, *args, **kwargs)
                except Not_Connected_To_Exchange_Exception:
                    if try_cnt <= 1:
                        raise

                    # Potentially recoverable
                    self.Connect( exchange_id = self.exchange_id
                                , keys        = self.keys
                                , force       = True
                                )
        return func_wrapper
    return EFWENCH_Decorator


################################################################################
# # Execute Function Trying To Recover
def EFTTR( max_tries = 3 ):
    def EFTTR_Decorator(func):
        def func_wrapper(self, *args, **kwargs):
            for try_cnt in range(max_tries, 0, -1):
                try:
                    return func(self, *args, **kwargs)
                # Potentially recoverable
                except ( DDoSProtection
                       , RequestTimeout
                       , ExchangeNotAvailable
                       , InvalidNonce
                       ) as e:
                    logger.info( "%s exception caught, %d tries left"
                               , str(e.__class__)
                               , try_cnt-1
                               )

                    if try_cnt <= 1:
                        raise
        return func_wrapper
    return EFTTR_Decorator


################################################################################
def Sanity_Check( rate_limit             = True
                , exchange_functionality = None
                ):
    def Sanity_Check_Decorator(func):
        def func_wrapper(self, *args, **kwargs):
            if self.exchange is None:
#                logger.error( "Not connected to any exchange." )
                raise Not_Connected_To_Exchange_Exception

            if exchange_functionality is not None and exchange_functionality not in self.exchange.has:
#                logger.error( "Exchange does not support %s."%exchange_functionality )
                raise Exchange_Does_Not_Support_Functionality( exchange_functionality )

            if rate_limit:
                self.Wait_Rate_Limit()

            return func(self, *args, **kwargs)
        return func_wrapper
    return Sanity_Check_Decorator


################################################################################
class Exchange_Interface():
    ############################################################################
    def __init__( self
                , exchange_id
                , keys                    = None
                , save_debug_data         = False
                , debug_path              = '.'
                , ccxt_verbose            = False
                , OHLCV_page_sample_limit = 1000
                ):
        self.exchange        = None
        self.last_req_time   = 0

        self.exchange_id             = exchange_id
        self.keys                    = keys
        self.save_debug_data         = save_debug_data
        self.debug_path              = debug_path
        self.ccxt_verbose            = ccxt_verbose
        self.OHLCV_page_sample_limit = OHLCV_page_sample_limit


    ############################################################################
    def Wait_Rate_Limit(self):
        while True:
            now = time.time()
            wait_sec = min( self.min_wait_sec - (now - self.last_req_time)
                          , 0.1
                          )
            if wait_sec <= 0:
                break

            time.sleep(wait_sec)

        self.last_req_time = now


    ############################################################################
    def Connect( self
               , exchange_id = None
               , keys        = None
               , force       = False
               ):
        if exchange_id is None:
            exchange_id = self.exchange_id

        if keys is None:
            keys = self.keys

        if self.exchange is not None and not force:
            logger.info( "Already connected to exchange %s."
                       , self.exchange.__class__.__name__
                       )
            return

        conn_comm = getattr(ccxt, exchange_id)

        conf_dict = {}

        if self.ccxt_verbose:
            conf_dict.update( {'verbose': True} )
        if keys is not None:
            conf_dict.update( keys )
        self.exchange = conn_comm( conf_dict )
        logger.info( "Connected to exchange %s."
                   , exchange_id
                   )

        self.min_wait_sec = self.exchange.rateLimit / 1000.
        logger.info( "Minimum wait time for exchange is %g sec."
                   , self.min_wait_sec
                   )

        self.Load_Markets()


    ############################################################################
    def Disconnect(self):
        self.exchange = None


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check()
    def Load_Markets(self, reload = False):
        self.exchange.load_markets(reload)
        logger.info( "Fetched and cached markets." )


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check()
    def Fetch_Balance(self):
        balance = self.exchange.fetch_balance()
        logger.info( "Fetched balance." )

        def Sanitize(bal):
            bal_sanitized = {}
            for key, val in bal.items():
                if val > 0.:
                    bal_sanitized[key] = val
            return bal_sanitized

        return Sanitize( balance["total"] ),\
               Sanitize( balance[ "used"] ),\
               Sanitize( balance[ "free"] )


    ################################################################################
    def Get_Asset_Portfolio( self ):
        # Get current asset portfolio
        max_tries = 10

        for _ in range(max_tries):
            bal_total, _, bal_free = self.Fetch_Balance()

            if bal_total == bal_free:
                return bal_free

        # We failed fetching the portfolio, because certain assets are still used
        raise Portfolio_Fetch_Exception( 'Portfolio has assets in the used state even after %d tries.'%max_tries )


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check()
    def Fetch_Tickers(self):
        try:
            tickers = self.exchange.fetch_tickers()
            logger.info( "Fetched tickers." )
        except KeyError:
            logger.error( "Error fetching tickers." )
            tickers = {}
            if self.save_debug_data:
                from os import path

                with open( path.join( self.debug_path, 'exchange_interface.log' )
                         , 'a'
                         ) as f:
                    def log_expr(expr, expr_val):
                        f.write("#" + expr + " = \\\n" )
                        f.write( str( expr_val ) + "\n" )

                    f.write("#"*20 + "\n")
                    f.write( str(                                time.time() ) + "\n" )
                    f.write( str(datetime.datetime.fromtimestamp(time.time())) + "\n" )
                    log_expr( "self.exchange.last_http_response"
                            , self.exchange.last_http_response
                            )

        return tickers


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check()
    def Fetch_Order(self, order_ID):
        order = self.exchange.fetch_order(order_ID)
        logger.info( "Fetched order by ID: %s."
                   , order_ID
                   )

        return order


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check()
    def Cancel_Order(self, order_ID):
        self.exchange.cancel_order(order_ID)
        logger.info( "Cancelled order by ID: %s."
                   , order_ID
                   )


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check(rate_limit = False)
    def Get_Markets( self ):
        return self.exchange.markets


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check(rate_limit = False)
    def Get_Quotes(self):
        return list( set( [ market["quote"] for market in self.exchange.markets.values() ] ) )


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check(rate_limit = False)
    def Get_Symbols(self, quote = None):
        symbols = []
        for market in self.exchange.markets.values():
            if quote is None or market["quote"] == quote:
                symbols.append( market["symbol"] )

        return sorted(symbols)


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check( rate_limit             = True
                 , exchange_functionality = 'fetchOHLCV'
                 )
    def Fetch_OHCLV( self
                   , symbol
                   , timeframe
                   , since_s = None
                   ):
        if since_s is None:
            since_ms    = None
            limit_smpls = None
        else:
            since_ms    = int( since_s * 1e3 )
            limit_smpls = self.OHLCV_page_sample_limit

        data = list(zip(*self.exchange.fetchOHLCV( symbol
                                                 , timeframe
                                                 , since = since_ms # UTC timestamp in milliseconds
                                                 , limit = limit_smpls
                                                 )
                       )
                   )

        if len(data) == 0:
            data = [ [], [], [], [], [], [] ]

#        t = [datetime.datetime.fromtimestamp(ms / 1000.) for ms in data[0]]
        t_s = np.asarray(data[0])/1000. # timestamp in seconds
        o = data[1]
        h = data[2]
        l = data[3]
        c = data[4]
        v = data[5]

        from_to_str = ""
        if len(t_s) > 0:
            from_to_str = " from %s to %s"%( datetime.datetime.fromtimestamp( t_s[ 0] )
                                           , datetime.datetime.fromtimestamp( t_s[-1] )
                                           )
        logger.info( "Fetched %d %s OHLCV samples for symbol %s%s."
                   , len(t_s)
                   , timeframe
                   , symbol
                   , from_to_str
                   )

        return t_s, o, h, c, l, v


    ############################################################################
    @EFTTR()
    @EFWENCH()
    @Sanity_Check()
    def Create_Market_Order(self, symbol, amount, buy_not_sell):
        func = self.exchange.create_market_buy_order if buy_not_sell else self.exchange.create_market_sell_order

        order_ID = func(symbol, amount)['id']
        logger.info( "Created market order for %sing %f amount of %s."
                   , "buy" if buy_not_sell else "sell"
                   , amount
                   , symbol
                   )

        return order_ID


    ############################################################################
    def Fetch_Historical_Price( self
                              , asset
                              , reference_asset 
                              , symbols
                              , timeframe
                              , since_s = None
                              ):
        timeframe_sec = timeframes[timeframe]

        symbol     =           asset + '/' + reference_asset
        rev_symbol = reference_asset + '/' +           asset

        if symbol not in symbols and rev_symbol not in symbols:
            return None, None

        c_mod = lambda x : x
        if rev_symbol in symbols:
            symbol = rev_symbol
            c_mod = lambda x : 1./np.asarray(x)

        Calc_Most_Recent_Timestamp = \
            lambda x : x - x%timeframe_sec

        full_t_s = []
        full_c   = []
        while True:
            # Check that we don't ask for prices which are not yet available
            if not since_s is None and since_s > Calc_Most_Recent_Timestamp(time.time()):
                break

            t_s, _, _, c, _, _ = self.Fetch_OHCLV( symbol
                                                 , timeframe
                                                 , since_s
                                                 )
            if len(full_t_s):
                while len(t_s) and full_t_s[-1] >= t_s[0]:
                    t_s = t_s[1:]
                    c   = c[1:]

            if len(t_s) == 0 or len(c) == 0:
                break

            full_t_s = np.concatenate((full_t_s, t_s      ))
            full_c   = np.concatenate((full_c  , c_mod(c) ))

            since_s = t_s[-1] + timeframe_sec/2.

        return full_t_s, full_c


################################################################################
def Filter_Markets( markets ):
    return \
        { key : 
          { 'precision' :
            { 'amount': val['precision']['amount']
            }
          , 'limits' :
            { 'cost' :
              { 'min' : val['limits']['cost']['min']
              , 'max' : val['limits']['cost']['max']
              }
            }
          , 'quote'  : val['quote']
          , 'symbol' : val['symbol']
          }
          for key, val in markets.items()
        }
