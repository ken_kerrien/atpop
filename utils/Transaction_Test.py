from atpop.lib.exchanger.exchange.interface import Exchange_Interface


################################################################################
if __name__ == '__main__':
    exchange_id  = 'hitbtc'

    symbol       = 'USDT/TUSD'
    amount       = 400
    buy_not_sell = True

    ex = Exchange_Interface( exchange_id
                           , keys = { 'apiKey': "REPLACE_THIS"
                                    , 'secret': "REPLACE_THIS"
                                    }
                           )
    ex.Connect( force = True )

    order_ID = ex.Create_Market_Order( symbol, amount, buy_not_sell )

    print( ex.Fetch_Order( order_ID ) )
    print( "#"*10 )
