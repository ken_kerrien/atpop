"""
This service periodically updates historical price data and watches the
portfolio target file. If the target changes, it will communicate with
the exchange online, and issue a sequence of trade commands that should
achieve the targeted portfolio.
"""

# Setup logging
from ..lib.log_func import logger
from ..lib.log_func import log_exception_info
from ..lib.log_func import log_exception_error
from ..lib.log_func import log_exception_error_with_backtrace


########################################
from ..lib.wait_for_objects                         import Wait_For_Files
from ..lib.exchanger.exchange.utils                 import timeframes
from ..lib.exchanger.utils.portfolio_rebalance_math import Portfolio_Rebalance_Exception
from ..lib.exchanger.portfolio                      import Exchanger_Portfolio
from ..lib.exchanger.order                          import Exchanger_Order
from ..lib.exchanger.price                          import Exchanger_Price

import time


########################################
# Execute Function Trying To Recover
def EFTTR2(error_tolerance, func):
  max_tries = 4

  for try_cnt in range(max_tries, 0, -1):
    last_try_flag = (try_cnt <= 1)
    try:
      save_debug_data_at_benign_exceptions = last_try_flag
      return func( error_tolerance
                 , save_debug_data_at_benign_exceptions
                 )

    except Portfolio_Rebalance_Exception:
      logger.info( "Porfolio rebalancing failed miserably, tolerance was %g, let's increase it, %d tries left"
                 , error_tolerance
                 , try_cnt-1
                 )

      if last_try_flag:
        raise

    except ValueError:
      logger.info( "Couldn't find porfolio rebalancing solution within tolerance %g, let's increase it, %d tries left"
                 , error_tolerance
                 , try_cnt-1
                 )

      if last_try_flag:
        raise

    error_tolerance *= 10


################################################################################
class Exchanger( Exchanger_Portfolio, Exchanger_Order, Exchanger_Price ):
  ##############################################################################
  def __init__( self
              , Exchange_Setup
              , Portfolio_Setup
              , Asset_Price_Setup
              , STATE_ARTIFACTS_PATH
              , DEBUG_ARTIFACTS_PATH
              ):
    super().__init__( Exchange_Setup     = Exchange_Setup
                    , Portfolio_Setup    = Portfolio_Setup
                    , Asset_Price_Setup  = Asset_Price_Setup
                    , STATE_ARTIFACTS_PATH = STATE_ARTIFACTS_PATH
                    , DEBUG_ARTIFACTS_PATH = DEBUG_ARTIFACTS_PATH
                    )


  ##############################################################################
  def Wait_For_Event(self):
    wo = Wait_For_Files( [self.portfolio_frac_precursor_data.Get_CSV_File_Path()]
                       , timeframes[self.Asset_Price_Setup.timeframe]
                       , report_change_at_start = False
                       )
    wo.Wait( self._Exchange )


  ##############################################################################
  def _Exchange(self, mod_file_paths, time_out):
    ##############################
    # Read OHLCV for price
    if time_out:
      try:
        self._Exchange_Handle_Timeout()
      except Exception as e:
        log_exception_error_with_backtrace(e)
        self.ex.Disconnect()
        logger.info( "Unrecoverable error fetching price data from exchange, so we'll disconnect and hope for the best" )

    ##############################
    # Perform trading activity
    if self.portfolio_frac_precursor_data.Get_CSV_File_Path() in mod_file_paths:
      try:
        self._Exchange_Handle_File()
      except Exception as e:
        log_exception_error_with_backtrace(e)
        self.ex.Disconnect()
        logger.info( "Unrecoverable error getting orders from exchange, so we'll disconnect and hope for the best" )


  ##############################################################################
  def _Exchange_Handle_File(self):
    ##############################
    logger.info( "Change detected with %s"
               , self.portfolio_frac_precursor_data.Get_CSV_File_Path()
               )
    try:
      self.portfolio_frac_precursor_data.Read_CSV()
    except IOError as e:
      log_exception_info(e)
      return


    ##############################
    # Calculate new portfolio frac target
    portfolio_frac_target = self._Get_Portfolio_Frac_Target()

    self.portfolio_frac_target_data.Add_Entry( portfolio_frac_target, time.time() )
    try:
      self.portfolio_frac_target_data.To_CSV()

      logger.info( "New portfolio fraction target %s written to %s"
                 , str(portfolio_frac_target)
                 , self.portfolio_frac_target_data.Get_CSV_File_Path()
                 )
    except IOError as e:
      log_exception_error(e)


    ##############################
    # Make it rain!
    while True:
      # Query all orders needed to go from current portfolio to portfolio target.
      # Then we only use the first order, and recalculate all orders again, because
      # things may have changed by the time it went through.

      # Also, in this following step it is possible that an asset is not found in the
      # ticker data anymore, meaning that asset is not traded, anymore. If that happens,
      # we remove it from the local simulated portfolio book keeping altogether. It may
      # still somehow exist on the exchange, but if we can't trade it, we'll treat it as
      # if it didn't exist for the purposes of making orders.
      self.tradeable_portfolio, markets, asset_pairs = \
        self._Get_Asset_Data( self.immutable_starting_portfolio
                            , self.Asset_Price_Setup.reference_asset
                            , portfolio_frac_target.keys()
                            , portfolio = self.simulated_portfolio if self._Get_Param_True('sim_trade') else None
                            )

      cop_res =\
        EFTTR2( self.Portfolio_Setup.rebalance_error_tol # initial error tolerance
              , lambda error_tolerance, save_debug_data_at_benign_exceptions : \
                self._Calculate_Order_Precursors( portfolio_frac_target
                                                , self.Asset_Price_Setup.reference_asset
                                                , self.Portfolio_Setup.frac_threshold
                                                , self.Portfolio_Setup.frac_tolerance
                                                , error_tolerance
                                                , self.tradeable_portfolio
                                                , markets
                                                , asset_pairs
                                                , save_debug_data                      = self._Get_Param_True('debug_calculate_order_precursors')
                                                , save_debug_data_at_benign_exceptions = save_debug_data_at_benign_exceptions
                                                , debug_path                           = self.DEBUG_ARTIFACTS_PATH
                                                )
              )

      if len(cop_res.ordered_flow_idxs) == 0: # We are done
        break

      # Are we allowed to trade?
      if self._Get_Param_True('dry_run'):
        logger.info( "We are dry running, so no actual trade activity" )
        break

      for flow_idx in cop_res.ordered_flow_idxs:
        pmo_res = self._Place_Market_Order( cop_res
                                          , asset_pairs
                                          , flow_idx
                                          )
        if not pmo_res is None:
          break
      else:
        # We have a bunch of flows, but all of them are so low that they are 0 after
        # sanitization, so we don't know what to do with any of the orders, so we just
        # call it a day
        break

      if pmo_res == False:
        # We tried a few times decreasing the trade amount, but always got an exception
        # of insufficient funds
        continue

      order_ID, order_str = pmo_res

      _order_closed = self._Verify_Order( order_ID
                                        , order_str
                                        )

    logger.info( "There are no more orders to place" )


  ##############################################################################
  def _Exchange_Handle_Timeout(self):
    logger.info( "Timeout occured, let's fetch price data" )

    self._Update_Price_Data()

    logger.info( "Fetched newest price values" )

    try:
      self.price_data.To_CSV()

      logger.info( "New price values written to %s"
                 , self.price_data.Get_CSV_File_Path()
                 )
    except IOError as e:
      log_exception_error(e)
