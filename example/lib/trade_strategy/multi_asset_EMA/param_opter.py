# Setup logging
from ...log_func import logger


########################################
from .setup            import Setup
from ..par_defs        import Convert_Par_Dict_To_Pars,\
                              Convert_Pars_Strategies_To_Par_Dict_Strategies
from ..name_mangling   import Get_Name_Suffix

from ..EMA.param_opter import Param_Opter as Filter_Sub_Param_Opter
from ..EMA.setup       import Setup as Filter_Sub_Setup


################################################################################
class Param_Opter():
    ####################################
    def __init__( self
                , price_data
                , history
                , sub_opt_setup
                , timeframe_sec
                , opt_par_data
                , max_price_age   = 4*60*60 # 4 hours
                , sub_param_opter = Filter_Sub_Param_Opter
                , sub_setup       = Filter_Sub_Setup
                , debug_path      = '.'
                , setup           = Setup
                ):
        ############
        self.price_data      = price_data
        self.opt_par_data    = opt_par_data

        self.history         = int( history )

        self.timeframe_sec   = timeframe_sec

        self.sub_opt_setup   = self.sub_opt_setup

        ############
        self.setup           = setup

        self.max_price_age   = max_price_age

        self.sub_param_opter = sub_param_opter
        self.sub_setup       = sub_setup

        self.debug_path      = debug_path


    ####################################
    def Get_Next_Iteration_Of_Optimal_Pars_Strategies_And_Price_Time_Strategies( self ):
        # Get asset list from prices
        # Weed out the low volume entries and the old ones
        relevant_assets = self.price_data.Get_Relevant_Assets( self.max_price_age )

        if not relevant_assets:
            all_assets_at_start = len( self.price_data.Get_Assets() )
            if all_assets_at_start > 0:
                logger.info( "We have %d assets in price data, but none relevant"
                           , all_assets_at_start
                           )
            return None, None

        # Get latest optimal parameters
        latest_par_dict_strategies, latest_price_time_strategies =\
            self.opt_par_data.Get_Latest_Par_Dict_Strategies_And_Price_Time_Strategies( self.setup.strategy )

        price_time_dict       = {}
        pars_strategies       = {}
        price_time_strategies = {}
        for strategy in latest_par_dict_strategies.keys():
            par_asset = Get_Name_Suffix( strategy, self.setup.strategy )
            if not par_asset in relevant_assets:
                continue

            price_time_dict[ par_asset ]      = latest_price_time_strategies[ strategy ]
            # for the output
            price_time_strategies[ strategy ] = latest_price_time_strategies[ strategy ]
            pars_strategies[ strategy ]       = Convert_Par_Dict_To_Pars( self.setup.par_defs, latest_par_dict_strategies[ strategy ] )

        # Which asset should we optimize for?
        # Look for asset in prices that does not have optimal params
        # Those have priority
        for asset in relevant_assets:
            if not asset in price_time_dict.keys():
                break
        else:
            # All assets in prices have optimal params
            for asset, price_time in price_time_dict.items():
                if price_time != self.price_data.Get_Latest_Valid_Timestamp( asset ):
                    break #BINGO!
            else:
                return None, None # Nothing left to do...

        #####################################################
        logger.info( "Optimizing parameters for %s"
                   , asset
                   )

        fss = self.sub_setup()
        fss.strategy = self.setup.strategy
        fss.asset    = asset

        sub_param_opter = self.sub_param_opter( self.price_data
                                              , self.history
                                              , self.sub_opt_setup
                                              , self.timeframe_sec
                                              , self.opt_par_data
                                              , setup = fss
                                              )

        sub_pars_strategies, sub_price_time_strategies =\
            sub_param_opter.Get_Optimal_Pars_Strategiest()

        for strategy in sub_pars_strategies.keys():
            pars_strategies[ strategy ]       = sub_pars_strategies[ strategy ]
            price_time_strategies[ strategy ] = sub_price_time_strategies[ strategy ]

        return pars_strategies, price_time_strategies


    ####################################
    def Get_Next_Iteration_Of_Optimal_Par_Dict_Strategies_And_Price_Time_Strategies( self ):
        pars_strategies, price_time_strategies = \
            self.Get_Next_Iteration_Of_Optimal_Pars_Strategies_And_Price_Time_Strategies()

        if pars_strategies is None:
            return None, None

        par_dict_strategies = \
            Convert_Pars_Strategies_To_Par_Dict_Strategies( self.setup.par_defs
                                                          , pars_strategies
                                                          )
        return par_dict_strategies, price_time_strategies
