from ..lib.exchanger.utils.finalize_portfolio_frac_target   import Get_Compound_Portfolio_Frac_Target
from ..lib.exchanger.utils.portfolio_handler                import Check_Portfolio_Frac_Target_Within_Tolerance
from ..lib.exchanger.utils.portfolio_rebalance_math         import Rebalance_Portfolio
from ..lib.exchanger.utils.names                            import QUOTE_NAME        ,\
                                                                   BASE_NAME         ,\
                                                                   FROM_NAME         ,\
                                                                   TO_NAME           ,\
                                                                   EXCHANGE_RATE_NAME,\
                                                                   TRADE_FEE_NAME
import numpy as np


################################################################################
def Get_Portfolio_Frac_Target_Series( portfolio_frac_target_data
                                    , start_time
                                    , stop_time
                                    ):
    trade_time_series, portfolio_frac_target_series = \
        portfolio_frac_target_data.Get_Portfolio_Frac_Series()

    while len(trade_time_series) > 0 and start_time > trade_time_series[0]:
        trade_time_series = trade_time_series[1:]
        portfolio_frac_target_series = portfolio_frac_target_series[1:]

    while len(trade_time_series) > 0 and stop_time < trade_time_series[-1]:
        trade_time_series = trade_time_series[:-1]
        portfolio_frac_target_series = portfolio_frac_target_series[:-1]

    return trade_time_series, portfolio_frac_target_series


################################################################################
def Get_Portfolio_Frac_Precursor_Strategies_Series( portfolio_frac_precursor_data
                                                  , start_time
                                                  , stop_time
                                                  ):
    trade_time_series, portfolio_frac_prec_strategies_series = \
        portfolio_frac_precursor_data.Get_Strategies_Series()

    while len(trade_time_series) > 0 and start_time > trade_time_series[0]:
        trade_time_series = trade_time_series[1:]
        portfolio_frac_prec_strategies_series = portfolio_frac_prec_strategies_series[1:]

    while len(trade_time_series) > 0 and stop_time < trade_time_series[-1]:
        trade_time_series = trade_time_series[:-1]
        portfolio_frac_prec_strategies_series = portfolio_frac_prec_strategies_series[:-1]

    return trade_time_series, portfolio_frac_prec_strategies_series


################################################################################
def Calculate_Portfolio_Frac_Precursor_Strategies_Series_For_Pars_Strategies( pars_strategies
                                                                            , assets_price_record
                                                                            , Calculate_Portfolio_Frac_Precursor_Series
                                                                            , up_to_but_not_including_time = None
                                                                            ):
    ####################################
    # Get portfolio fraction precursor series using every strategy
    blah = {}
    for strategy, pars in pars_strategies.items():
        res = \
            Calculate_Portfolio_Frac_Precursor_Series( strategy
                                                     , pars
                                                     , assets_price_record
                                                     , up_to_but_not_including_time
                                                     )
        blah[strategy] = (res[0], res[1]) #(trade_time_series, portfolio_frac_precursor_series)


    ####################################
    # Combine the portfolio fraction precursor series from various strategies
    combined_trade_time_series                     = []
    combined_portfolio_frac_prec_strategies_series = []

    while True:
        # Find next combined trade time
        combined_trade_time                      = float("inf")
        combined_portfolio_frac_prec_strategies  = {}
        for strategy, (trade_time_series, portfolio_frac_precursor_series) in blah.items():
            for trade_time, portfolio_frac_precursor in zip(trade_time_series, portfolio_frac_precursor_series):
                if combined_trade_time_series and trade_time <= combined_trade_time_series[-1]:
                    continue

                if trade_time > combined_trade_time:
                    break

                if trade_time < combined_trade_time:
                    combined_portfolio_frac_prec_strategies  = {}

                combined_trade_time                               = trade_time
                combined_portfolio_frac_prec_strategies[strategy] = portfolio_frac_precursor

        if combined_trade_time == float("inf"):
            break

        # There were some strategies that changed their portfolios!
        # What should happen to the rest? We'll copy the previous values
        # IF we have those
        if combined_portfolio_frac_prec_strategies_series:
            # Copy portfolios for strategies that have been in there in the past
            # but did not change this time
            for strategy, portfolio_frac_precursor in combined_portfolio_frac_prec_strategies_series[-1].items():
                # If it's in the new, we don't copy
                if strategy in combined_portfolio_frac_prec_strategies:
                    continue

                combined_portfolio_frac_prec_strategies[strategy] = portfolio_frac_precursor

            # If the strategies ended up the same as last entry, don't care
            if combined_portfolio_frac_prec_strategies_series[-1] == combined_portfolio_frac_prec_strategies:
                continue

        combined_trade_time_series.append( combined_trade_time )
        combined_portfolio_frac_prec_strategies_series.append( combined_portfolio_frac_prec_strategies )

    return combined_trade_time_series, combined_portfolio_frac_prec_strategies_series


################################################################################
def Calculate_Portfolio_Frac_Precursor_Strategies_Series_For_Pars_Strategies_Series( assets_price_record
                                                                                   , pars_strategies_time
                                                                                   , pars_strategies_series
                                                                                   , Calculate_Portfolio_Frac_Precursor_Series
                                                                                   ):
    ####################################
    # Sanity check
    assert len(pars_strategies_series) == len(pars_strategies_time)

    price_stop_time  = assets_price_record.max_stop_timestamp

    if len(pars_strategies_time) <= 0:
        return [], [], (price_stop_time,)

    # If the last asset price is before or at the first strategy,
    # there's nothing to do
    if price_stop_time <= pars_strategies_time[0]:
        return [], [], (price_stop_time,)

    ####################################
    # Compile
    # the order of things at every time step:
    # 1. price sample arrives exactly at time step
    # 2. some minimal time later the corresponding trade action may be taken
    # 3. even later new trade parameters may be established
    # 4. if so, corresponding trade action may be taken
    # ALL OF THESE CAN BE LABELED TO HAVE HAPPENED AT THE SAME TIME STEP!

    rev_trade_time_series                     = []
    rev_portfolio_frac_prec_strategies_series = []
    for par_idx in reversed(range(len(pars_strategies_series))):
        up_to_but_not_including_time = None
        if par_idx+1 < len(pars_strategies_time):
            up_to_but_not_including_time = pars_strategies_time[par_idx+1]

        res = \
            Calculate_Portfolio_Frac_Precursor_Strategies_Series_For_Pars_Strategies( pars_strategies_series[par_idx]
                                                                                    , assets_price_record
                                                                                    , Calculate_Portfolio_Frac_Precursor_Series
                                                                                    , up_to_but_not_including_time = up_to_but_not_including_time
                                                                                    )
        for trade_time, portfolio_frac_prec_strategies in zip( reversed(res[0]), reversed(res[1]) ):
            rev_trade_time_series.append( max( trade_time, pars_strategies_time[par_idx] ) )
            rev_portfolio_frac_prec_strategies_series.append( portfolio_frac_prec_strategies )
            if trade_time <= pars_strategies_time[par_idx]:
                break

    return list(reversed(rev_trade_time_series))                     \
         , list(reversed(rev_portfolio_frac_prec_strategies_series)) \
         , ( rev_trade_time_series[-1], )


################################################################################
def Calculate_Backtest_From_Portfolio_Frac_Precursor_Strategies_Series( assets_price_record
                                                                      , trade_time_series
                                                                      , portfolio_frac_prec_strategies_series
                                                                      , reference_asset
                                                                      , trade_fee
                                                                      , portfolio_frac_threshold  = 0.03 # 3% 
                                                                      , portfolio_frac_tolerance  = 0.01 # 1%
                                                                      , initial_value             = 1.
                                                                      , save_rebalance_debug_data = False
                                                                      , extend_by_samples         = 1
                                                                      ):
    return _Calculate_Backtest_From_Some_Portfolio_Frac_Series( assets_price_record
                                                              , trade_time_series
                                                              , portfolio_frac_prec_strategies_series
                                                              , Get_Compound_Portfolio_Frac_Target
                                                              , reference_asset
                                                              , trade_fee
                                                              , portfolio_frac_threshold
                                                              , portfolio_frac_tolerance
                                                              , initial_value
                                                              , save_rebalance_debug_data
                                                              , extend_by_samples
                                                              )


################################################################################
def Calculate_Backtest_From_Portfolio_Frac_Target_Series( assets_price_record
                                                        , trade_time_series
                                                        , portfolio_frac_target_series
                                                        , reference_asset
                                                        , trade_fee
                                                        , portfolio_frac_threshold  = 0.03 # 3%
                                                        , portfolio_frac_tolerance  = 0.01 # 1%
                                                        , initial_value             = 1.
                                                        , save_rebalance_debug_data = False
                                                        , extend_by_samples         = 1
                                                        ):
    Get_Portfolio_Frac_Target = \
        lambda portfolio_frac_target, _portfolio_frac, _reference_asset: portfolio_frac_target

    return _Calculate_Backtest_From_Some_Portfolio_Frac_Series( assets_price_record
                                                              , trade_time_series
                                                              , portfolio_frac_target_series
                                                              , Get_Portfolio_Frac_Target
                                                              , reference_asset
                                                              , trade_fee
                                                              , portfolio_frac_threshold
                                                              , portfolio_frac_tolerance
                                                              , initial_value
                                                              , save_rebalance_debug_data
                                                              , extend_by_samples
                                                              )


################################################################################
def _Calculate_Backtest_From_Some_Portfolio_Frac_Series( assets_price_record
                                                       , trade_time_series
                                                       , some_portfolio_frac_series
                                                       , Get_Portfolio_Frac_Target
                                                       , reference_asset
                                                       , trade_fee
                                                       , portfolio_frac_threshold
                                                       , portfolio_frac_tolerance
                                                       , initial_value
                                                       , save_rebalance_debug_data
                                                       , extend_by_samples
                                                       ):
    ####################################
    # Helper function
    def Get_Asset_Pairs(prices):
        asset_pairs = []
        for asset in prices.keys():
            asset_pairs.append( { QUOTE_NAME         : reference_asset
                                , BASE_NAME          : asset
                                , EXCHANGE_RATE_NAME : prices[asset]
                                , TRADE_FEE_NAME     : trade_fee
                                }
                              )
        return asset_pairs


    ####################################
    # Calculate portfolio fraction and value
    portfolio_frac          = {reference_asset :                1. } # All my wealth is in the reference asset when I start

    portfolio               = {reference_asset :     initial_value }

    portfolio_backtest_val  = {reference_asset : [ initial_value ] }

    time_and_portfolio_fracs = [ ( assets_price_record.min_start_timestamp
                                 , portfolio_frac
                                 )
                               ]

    price_timestamp_series = assets_price_record.timestamp_series
    next_prices            = assets_price_record.Get_Prices_At_Idx( 0
                                                                  , normalize = True
                                                                  )

    # Calculate how much wealth is in any one asset at any given time

    # Assumption: Prices don't change between time steps, only at timesteps
    # Assumption: Trade timestamps indicate when a trade action finished
    # Assumption: Trades only affect portfolio value at the next price timestep

    # Here's the situation, prices may not be available for all idxs:
    #      idx:N-2 N-1  N  N+1 N+2
    # Asset #1: x---x---x---x---x--
    # Asset #2: x---x---x---x---x--
    # Asset #3: x---x---o
    # If the trades are done right, there will be trade action after N,
    # and the algorithm will exclude Asset #3 from the portfolio, BUT
    # at idx N there is already no price value for Asset #3!
    # So, we'll extend all asset values by one, to cover for this case.
    # If there is no trade action to get rid of all of Asset #3, we'll
    # fail!

    for ii in range( 1, len(price_timestamp_series) ):
        # creating_asset pairs
        prices = next_prices
        next_prices = assets_price_record.Get_Prices_At_Idx( ii
                                                           , extend_by_samples = extend_by_samples
                                                           , normalize         = True
                                                           )
        asset_pairs = Get_Asset_Pairs(prices)

        # Calculate new portfolio fraction
        while len(trade_time_series):
            trade_time = trade_time_series[0]

            if trade_time >= price_timestamp_series[ii]:
                break

            some_portfolio_frac = some_portfolio_frac_series[0]

            portfolio_frac_target = \
                Get_Portfolio_Frac_Target( some_portfolio_frac
                                         , portfolio_frac
                                         , reference_asset
                                         )

            trade_time_series          = trade_time_series[1:]
            some_portfolio_frac_series = some_portfolio_frac_series[1:]

            if Check_Portfolio_Frac_Target_Within_Tolerance( portfolio_frac_target
                                                           , portfolio_frac
                                                           , portfolio_frac_threshold
                                                           ):
                continue

#            with open( 'debug_compute.log'
#                     , 'a'
#                     ) as f:
#              f.write("#######\n")
#              f.write(str(trade_time)+"\n")
#              f.write(str(portfolio_frac)+"\n")
#              f.write(str(portfolio_frac_target)+"\n")
#              f.write("###\n")

            time_and_portfolio_fracs.append( ( trade_time
                                             , portfolio_frac_target
                                             )
                                           )

            # The portfolio rebalancing is solved as a network flow problem
            debug_portfolio = dict(portfolio)

            _flow_vals, _flow_asset_pairs, _conn_matrix,\
            (ordered_asset_vals, ordered_asset_names, _ret_message) =\
                Rebalance_Portfolio( portfolio
                                   , portfolio_frac_target
                                   , asset_pairs
                                   , reference_asset
                                   , portfolio_frac_tolerance
                                   , QUOTE_NAME, BASE_NAME
                                   , FROM_NAME, TO_NAME
                                   , save_debug_data = save_rebalance_debug_data
                                   )

            portfolio           = {}
            portfolio_val_total = 0.
            for asset, val in zip(ordered_asset_names, ordered_asset_vals):
                if val <= 0:
                    continue
                portfolio[asset] = val
                portfolio_val_total += val * ( 1. if asset == reference_asset else prices[asset] )

            portfolio_frac      = {}
            for asset in portfolio.keys():
                portfolio_frac[asset] = portfolio[asset] * ( 1. if asset == reference_asset else prices[asset] ) / portfolio_val_total

            assert portfolio_frac \
                , "portfolio:\n%s\n"               %str( debug_portfolio          ) \
                + "portfolio_frac_target:\n%s\n"   %str( portfolio_frac_target    ) \
                + "asset_pairs:\n%s\n"             %str( asset_pairs              ) \
                + "reference_asset:\n%s\n"         %str( reference_asset          ) \
                + "portfolio_frac_threshold:\n%s\n"%str( portfolio_frac_threshold ) \
                + "portfolio_frac_tolerance:\n%s\n"%str( portfolio_frac_tolerance ) \
                + "QUOTE_NAME:\n%s\n"              %str( QUOTE_NAME               ) \
                + "BASE_NAME:\n%s\n"               %str( BASE_NAME                ) \
                + "FROM_NAME:\n%s\n"               %str( FROM_NAME                ) \
                + "TO_NAME:\n%s\n"                 %str( TO_NAME                  ) \
                + "_flow_vals:\n%s\n"              %str( _flow_vals               ) \
                + "_flow_asset_pairs:\n%s\n"       %str( _flow_asset_pairs        ) \
                + "_conn_matrix:\n%s\n"            %str( _conn_matrix             ) \
                + "ordered_asset_vals:\n%s\n"      %str( ordered_asset_vals       ) \
                + "ordered_asset_names:\n%s\n"     %str( ordered_asset_names      ) \
                + "_ret_message:\n%s\n"            %str( _ret_message             )

        # If the following fails, we want an asset in our portfolio AFTER it has already
        # been phased out!
        assert set( portfolio.keys() ) == set( portfolio_frac.keys() )
        valid_assets = set( next_prices.keys() ) | { reference_asset }
        delisted_assets = set(      portfolio.keys() ) - valid_assets
        assert not delisted_assets, \
          "No price data at %d, %f, but still in portfolio: %s; Next trade is at %f, which is %f steps: %s"%( ii
                                                                                                            , price_timestamp_series[ii]
                                                                                                            , str(delisted_assets)
                                                                                                            , trade_time
                                                                                                            , (trade_time-price_timestamp_series[ii]) / assets_price_record.time_step_s
                                                                                                            , str(some_portfolio_frac)
                                                                                                            )

        # If we ended up holding the asset at the end of this past period,
        # let's see how much its value changed due to price fluctuation
        for asset in set(portfolio_backtest_val.keys()) | set(portfolio.keys()):
            if not asset in portfolio_backtest_val:
                portfolio_backtest_val[asset] = [0.]*ii

            val = 0
            if asset in portfolio:
                val = portfolio[asset]

                if asset != reference_asset:
                    val *= next_prices[asset]

            portfolio_backtest_val[asset].append( val )

    return portfolio_backtest_val   \
         , time_and_portfolio_fracs


################################################################################
def Calculate_Backtest( assets_price_record
                      , pars_strategies_time
                      , pars_strategies_series
                      , Calculate_Portfolio_Frac_Precursor_Series
                      , reference_asset
                      , trade_fee
                      , portfolio_frac_threshold = 0.03 # 3%
                      , portfolio_frac_tolerance = 0.01 # 1%
                      , initial_value            = 1.
                      ):
  ####################################
  # Actually calculate the portfolio value
  trade_time_series,\
  portfolio_frac_prec_strategies_series,\
  (initial_price_time,) =\
    Calculate_Portfolio_Frac_Precursor_Strategies_Series_For_Pars_Strategies_Series( assets_price_record
                                                                                   , pars_strategies_time
                                                                                   , pars_strategies_series
                                                                                   , Calculate_Portfolio_Frac_Precursor_Series
                                                                                   )
  portfolio_val,\
  _time_and_portfolio_fracs = \
    Calculate_Backtest_From_Portfolio_Frac_Precursor_Strategies_Series( assets_price_record
                                                                      , trade_time_series
                                                                      , portfolio_frac_prec_strategies_series
                                                                      , reference_asset
                                                                      , trade_fee
                                                                      , portfolio_frac_threshold
                                                                      , portfolio_frac_tolerance
                                                                      , initial_value
                                                                      )

  return portfolio_val, trade_time_series, initial_price_time
