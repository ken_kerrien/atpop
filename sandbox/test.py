f=\
{ 'EOS': 0.006292059750181562
, 'ETH': 0.19333729356349985
, 'LTC': 0.07332055834909433
, 'BCH': 0.15360813285128705
, 'BTC': 0.22806400000000007
, 'CHZ': 0.34537795548593725
}
f_t=\
{ 'EOS': 0.016028999999999998
, 'ETH': 0.16481800000000002
, 'LTC': 0.103224
, 'BCH': 0.18304
, 'BTC': 0.197363
, 'CHZ': 0.333333
, 'ICX': 0.002193
}

for asset in f.keys():
    print( f[asset] - f_t[asset] )

exit()

class Portfolio_Rebalance_Exception(Exception):
    pass

try:
    raise Portfolio_Rebalance_Exception
except Exception as err:
    exception_type = type(err).__name__


def outer():
    s = ""
    s2 = ""
    def inner():
        nonlocal s, s2
        s = "Hello World!"
    inner()
    print(s2)
    print(s)

outer()

exit(0)


################################################################################
class A:
    def __init__(self, **kwargs):
        print("A.__init__")

class B(A):
    def __init__(self, **kwargs):
        b1 = kwargs["b1"]
        print("B.__init__ "+str(b1))
        super().__init__(**kwargs)

class C(A):
    def __init__(self, c1, c2, **kwargs):
        print("C.__init__ "+str(c1)+" "+str(c2))
        super().__init__(**kwargs)

class D(B,C):
    def __init__(self, b1, c1, c2):
        print("D.__init__ "+str(b1)+" "+str(c1)+" "+str(c2))
        super().__init__(b1=b1, c1=c1, c2=c2)


x = D("b1", "c1", "c2")

def test(a):
    pass

def test(a,b):
    pass
