################################################################################
def Add_Suffix_To_Name( base
                      , suffix
                      ):
    return base + " " + suffix


################################################################################
def Get_Name_Suffix( name
                    , base
                    ):
    if name.startswith( base + " " ):
        return name[len(base)+1:]

    return name


################################################################################
def Split_Name( name ):
    space_idx = name.find(" ")

    return name[:space_idx], name[space_idx+1:]
