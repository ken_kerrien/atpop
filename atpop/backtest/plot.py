import numpy as np
import matplotlib.pyplot as plt


################################################################################
def Prepare_Time( t_in
                , subtract_t
                ):
  t = np.asarray(t_in)
  t -= subtract_t
  t /= 24.*60.*60.
  return t


################################################################################
def Prepare_Portfolio_Val_And_Time( t
                                  , portfolio_val
                                  , initial_value
                                  , initial_idx
                                  , disp_hist
                                  ):
  total_val = sum( [ val[initial_idx] for val in portfolio_val.values() ] )

  if disp_hist == 0:
    disp_hist = len(t)
  disp_hist = min( disp_hist, len(t) )

  portfolio_val = { asset : np.asarray(prices[-disp_hist:]) / total_val * initial_value
                    for asset,prices in portfolio_val.items()
                  }

  t_zero = t[-1]
  t = Prepare_Time( t[-disp_hist:], t_zero )

  return t, portfolio_val, t_zero


################################################################################
def Plot_Portfolio_Value_Backtest( t
                                 , portfolio_val
                                 , initial_value               = 1.
                                 , initial_idx                 = 0
                                 , disp_hist                   = 0
                                 , trade_time_series           = np.asarray([])
                                 , assets_price_record_to_plot = None
                                 ):
  t, portfolio_val, t_zero =\
    Prepare_Portfolio_Val_And_Time( t
                                  , portfolio_val
                                  , initial_value
                                  , initial_idx
                                  , disp_hist
                                  )
  trade_time_series =\
    Prepare_Time( trade_time_series, t_zero )

  xlabel = "time [day]"


  ####################################
  fig, ax = plt.subplots(1, sharex=True)

  ax.stackplot( t
              , np.vstack( portfolio_val.values() )
              , labels = portfolio_val.keys()
              )

  for trade_time in trade_time_series:
    ax.axvline( trade_time
#              , color = 'gray'
              , color = 'black'
              , linewidth=0.3
              )

  if not assets_price_record_to_plot is None:
    for asset in assets_price_record_to_plot.Get_Assets():
      asset_time_series  = Prepare_Time( assets_price_record_to_plot.Get_Timestamp_Series(asset)
                                       , t_zero
                                       )
      if asset_time_series[0] > t[0] or asset_time_series[-1] <= t[0]:
        continue
      idx_arr = np.where( asset_time_series == t[0] )
      if len(idx_arr) == 0:
        continue
      first_idx = idx_arr[0]
      asset_price_vector = assets_price_record_to_plot.Get_Price_Vector(asset)
      asset_price_vector /= asset_price_vector[ first_idx ]

      ax.plot( asset_time_series, asset_price_vector, label=asset )

  ax.grid()
  ax.set_xlabel('time' if xlabel is None else xlabel)
  ax.set_xlim((t[0], t[-1]))
  ax.set_ylabel('asset value')
  ax.legend(loc='lower left')

  fig.tight_layout()
  plt.show()


################################################################################
def Plot_Portfolio_Value_Backtest_And_Par_Dict_Strategies_Series( t
                                                                , portfolio_val
                                                                , par_dict_strategies_time
                                                                , par_dict_strategies_series
                                                                , initial_value = 1.
                                                                , initial_idx   = 0
                                                                , disp_hist     = 0
                                                                ):
  t, portfolio_val, t_zero =\
    Prepare_Portfolio_Val_And_Time( t
                                  , portfolio_val
                                  , initial_value
                                  , initial_idx
                                  , disp_hist
                                  )
  par_dict_strategies_time =\
    Prepare_Time( par_dict_strategies_time
                , t_zero
                )

  xlabel = "time [day]"


  ####################################
  strategies_pars_time_and_val = {}
  current_strategies_pars_time = {}
  current_strategies_pars_val  = {}

  def add_current_strategies_pars(strategies_par, t):
    if not strategies_par in strategies_pars_time_and_val:
      strategies_pars_time_and_val[strategies_par] = []
    strategies_pars_time_and_val[strategies_par].append( ( current_strategies_pars_time[strategies_par] + [t]
                                                         , current_strategies_pars_val[strategies_par]
                                                         )
                                                       )
    current_strategies_pars_time.pop(strategies_par)
    current_strategies_pars_val.pop(strategies_par)

  # Step through each time entry
  for pdst, par_dict_strategies in zip( par_dict_strategies_time, par_dict_strategies_series ):
    # Get all the parameters that have new values
    new_strategies_pars_val  = {}
    for strategy, par_dict in par_dict_strategies.items():
      for par_name, par_val in par_dict.items():
        strategies_par = strategy + ": " + par_name
        new_strategies_pars_val[strategies_par] = [par_val]

    # If a parameter does not have a new value, retire it
    for strategies_par in list( current_strategies_pars_val.keys() ):
      if strategies_par in new_strategies_pars_val:
        continue
      # This parameter does not have a new value
      add_current_strategies_pars(strategies_par, pdst)

    # These parameters have a new value
    for strategies_par, par_val in new_strategies_pars_val.items():
      if not strategies_par in current_strategies_pars_val:
        current_strategies_pars_time[strategies_par] = []
        current_strategies_pars_val[strategies_par]  = []
      current_strategies_pars_time[strategies_par].append(pdst)
      current_strategies_pars_val[strategies_par].append(par_val)

  t_final = max( t[-1], par_dict_strategies_time[-1] )
  for strategies_par in list( current_strategies_pars_val.keys() ):
    add_current_strategies_pars( strategies_par, t_final )


  ####################################
  fig, axarr = plt.subplots( 1+len(strategies_pars_time_and_val), sharex=True )

  axarr[0].stackplot( t
                    , np.vstack( portfolio_val.values() )
                    , labels = portfolio_val.keys()
                    )

  axarr[0].grid()
  axarr[0].set_xlabel('time' if xlabel is None else xlabel)
  axarr[0].set_xlim((t[0], t[-1]))
  axarr[0].set_ylabel('asset value')
  axarr[0].legend(loc='lower left')

  for ii in range(len(strategies_pars_time_and_val)):
    for spt, spv in strategies_pars_time_and_val:
      axarr[1+ii].plot(spt[:-1], spv, ".")
      axarr[1+ii].step(spt, spv+[spv[-1]], where="post")

  fig.tight_layout()
  plt.show()
