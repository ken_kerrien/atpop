import sys, os
sys.path.append( os.path.join( os.path.dirname(__file__)
                             , r'../..'
                             )
               )

from utils.common                       import *

from atpop.lib.exchanger.exchange.utils import timeframes

from atpop.lib.data.price_data          import Price_Data
from atpop.lib.data.opt_par_data        import Opt_Par_Data ,\
                                               Opt_Par_Setup

from lib.trade_strategy.EMA.setup       import Setup
from lib.trade_strategy.EMA.backtest    import Calculate_And_Plot_Backtest_For_Pars_Series
from lib.trade_strategy.par_defs        import Convert_Par_Dict_To_Pars
from lib.trade_strategy.name_mangling   import Add_Suffix_To_Name

from services.config                    import Asset_Price_Setup,\
                                               Opt_Setup


################################################################################
if __name__ == '__main__':
    asset = 'BTC'

    opt_par_data = Opt_Par_Data( path = STATE_ARTIFACTS_PATH )
    opt_par_data.Read_CSV()

    price_data = Price_Data( path = STATE_ARTIFACTS_PATH )
    price_data.Read_CSV()

    par_dict_series, time_series =\
        opt_par_data.Get_Par_Dict_Series_And_Time_Series( Add_Suffix_To_Name( Setup.strategy
                                                                            , asset
                                                                            )
                                                        )
    pars_series = [ Convert_Par_Dict_To_Pars( Setup.par_defs, par_dict )
                    for par_dict in par_dict_series
                  ]

    assets_price_time, assets_price_record =\
        price_data.Get_Prices_For_Assets_On_Same_Time_Scale( timeframes[Asset_Price_Setup.timeframe]
                                                           , history = Asset_Price_Setup.history
                                                           )

    Calculate_And_Plot_Backtest_For_Pars_Series( assets_price_time
                                               , assets_price_record
                                               , time_series
                                               , pars_series
                                               , asset
                                               , "reference"
                                               , Opt_Setup.trade_fee
                                               #, disp_hist = int(2*Opt_Setup.half_life)
                                               , title     = asset
                                               )
