# Setup logging
from ..log_func import logger
from ..log_func import log_exception_info
from ..log_func import log_exception_error
from ..log_func import log_exception_error_with_backtrace

from ..data.portfolio_frac_data             import Portfolio_Frac_Precursor_Data,\
                                                   Portfolio_Frac_Data

from .utils.portfolio_handler               import Exlude_Assets_From_Portfolio,\
                                                   Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs
from .utils.asset_pair_handler              import Asset_Pair_Handler
from .utils.finalize_portfolio_frac_target  import Get_Compound_Portfolio_Frac_Target
from .base                                  import Exchanger_Base


import numpy as np


################################################################################
class Exchanger_Portfolio( Exchanger_Base ):
    #x##########################################################################
    def __init__( self
                , **kwargs
                ):
        super().__init__( **kwargs )

        self.Portfolio_Setup   = kwargs["Portfolio_Setup"]
        self.Asset_Price_Setup = kwargs["Asset_Price_Setup"]
        STATE_ARTIFACTS_PATH   = kwargs["STATE_ARTIFACTS_PATH"]

        ###################################
        # Input
        self.portfolio_frac_precursor_data = \
            Portfolio_Frac_Precursor_Data( path = STATE_ARTIFACTS_PATH )

        ###################################
        # Debug Output
        self.portfolio_frac_target_data = \
            Portfolio_Frac_Data( path = self.DEBUG_ARTIFACTS_PATH )
        self.portfolio_frac_target_data.setup.ID = "portfolio_frac_target"
        try:
            self.portfolio_frac_target_data.Read_CSV()
        except IOError as e:
            log_exception_info(e)


        ###################################
        def log_portfolio( prefix, portf ):
            for key, val in portf.items():
                logger.info( "%s: %s %g"
                           , prefix
                           , key
                           , val
                           )

        log_portfolio("Immutable starting assets request", self.Portfolio_Setup.immutable_starting_portfolio_request)

        try:
            portfolio = self.ex.Get_Asset_Portfolio()

            log_portfolio("Current portfolio", portfolio)

            # Only used if we are simulating a portfolio, and not actually trading
            self.simulated_portfolio = portfolio

            portfolio, self.immutable_starting_portfolio = \
                Exlude_Assets_From_Portfolio( portfolio
                                            , self.Portfolio_Setup.immutable_starting_portfolio_request
                                            )
            log_portfolio("Available portfolio", portfolio)
        except Exception as e:
            log_exception_error_with_backtrace(e)
            self.ex.Disconnect()
            logger.info( "Unrecoverable error fetching portfolio from exchange, so we'll disconnect and hope for the best" )


    ############################################################################
    def _Get_Portfolio_Frac_Target( self ):
        portfolio_frac_strategies = self.portfolio_frac_precursor_data.Get_Latest_Strategies()

        # Get current portfolio fraction
        portfolio_val = self.__Get_Portfolio_In_Reference_Asset()

        portfolio_total = sum( portfolio_val.values() )
        portfolio_frac = dict( [ (asset, val/portfolio_total) for asset, val in portfolio_val.items() if val > 0] )

        compound_portfolio_frac_target = \
            Get_Compound_Portfolio_Frac_Target( portfolio_frac_strategies
                                              , portfolio_frac
                                              , self.Asset_Price_Setup.reference_asset
                                              )
        assert not "timestamp" in compound_portfolio_frac_target
#        if "timestamp" in compound_portfolio_frac_target:
#            reference = self.Asset_Price_Setup.reference_asset
#
#            import os
#            from datetime import datetime
#            import time
#
#            os.makedirs( os.path.abspath(self.DEBUG_ARTIFACTS_PATH)
#                       , exist_ok = True
#                       )
#
#            with open( os.path.join( self.DEBUG_ARTIFACTS_PATH, 'get_portfolio_frac_target.log' )
#                     , 'a'
#                     ) as f:
#                def log_local_var(var_name, vars_dict=locals()):
#                    f.write("#" + var_name + " = \\\n" )
#                    f.write( str( vars_dict[var_name] ) + "\n" )
#
#                f.write("#"*20 + "\n")
#                f.write( str(                       time.time() ) + "\n" )
#                f.write( str(datetime.fromtimestamp(time.time())) + "\n" )
#                log_local_var("portfolio_frac_strategies")
#                log_local_var("portfolio_frac")
#                log_local_var("reference")

        return compound_portfolio_frac_target


    ############################################################################
    def __Get_Portfolio_In_Reference_Asset( self ):
        ########################################################################
        # Query most up-to-date data from exchange

        # Get current asset portfolio
        # Can raise Portfolio_Fetch_Exception
        portfolio = self.ex.Get_Asset_Portfolio()

        # Fetch tickers for conversion prices
        tickers = self.ex.Fetch_Tickers()


        ########################################################################
        # Preprocess
        asset_pair_handler = Asset_Pair_Handler( self.bn, self.qn )
        asset_pair_handler.Generate_Asset_Pairs( tickers.keys() )
        asset_pair_handler.Filter_To_Contain_Asset( self.Asset_Price_Setup.reference_asset
                                                  , asset_list = portfolio.keys()
                                                  )
        asset_pair_handler.Query_Exchange_Rate( self.ex
                                              , tickers
                                              , filter_flag = True
                                              )

        # Exclude assets we want to save from current portfolio
        portfolio, _   = \
            Exlude_Assets_From_Portfolio( portfolio
                                        , self.immutable_starting_portfolio
                                        )

        # Convert all assets to reference asset
        portfolio_conv = \
            Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( portfolio
                                                                  , self.Asset_Price_Setup.reference_asset
                                                                  , asset_pair_handler.asset_pairs
                                                                  , self.qn, self.bn
                                                                  )
        return portfolio_conv
