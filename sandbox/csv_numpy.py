import numpy as np

#res = np.loadtxt( './sandbox/test.csv'
#                , delimiter = ","
#                , skiprows  = 1
#                )
sd = np.genfromtxt( './sandbox/test.csv'
#                  , dtype          = None
                  , delimiter      = ','
                  , missing_values = np.nan
                  , names          = True
                  , encoding       = None
                  )

sd['asset'][ np.asarray([True, True, True, False, True]) ] = np.asarray([ 1,2,3,4 ])

np.savetxt( './sandbox/test_out.csv'
          , sd
          , fmt='%0.15g'
          , header         = ','.join( sd.dtype.names )
          , delimiter      = ','
          , comments       = ''
          , encoding       = None
          )

with open( './sandbox/test_out.csv', 'w' ) as fh:
  fh.write( ','.join( sd.dtype.names ) )
  fh.write( '\n' )
  for row in sd:
    fh.write( ','.join( [ '' if np.isnan(num) else ('%.15f'%num).rstrip('0').rstrip('.') for num in row ] ) )
    fh.write( '\n' )

print( sd )
print( sd['index'] )
print( sd['index'][-1] )
print( sd['asset'] )
print( len(sd) )
print( sd.size )
