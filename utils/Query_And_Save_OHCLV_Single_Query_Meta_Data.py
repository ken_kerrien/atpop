import sys
from atpop.lib.exchanger.exchange.interface import Exchange_Interface

import pandas as pd

import ccxt


################################################################################
if __name__ == '__main__':
    reference_asset = 'USD'

    assets = [ 'NEO'
             , 'BCH'
             , 'LTC'
             , 'ZEC'
             , 'DASH'
             , 'DOGE'
             , 'EOS'
             , 'ETC'
             , 'BTC'
             , 'ETH'
             , 'XRP'
             , 'XMR'
             ]

    df = pd.DataFrame({ 'Exchange'  : []
                      , 'Asset'     : []
                      , 'Timestamp' : []
                      , 'Entries'   : []
                      }
                     )


    for exchange_id in ccxt.exchanges:
        if exchange_id in [ '_1broker'
                          , '_1btcxe'
                          ]:
            continue

        print( "Exchange: %s"%exchange_id )

        try:
            exchange = Exchange_Interface( exchange_id )
            exchange.Connect()

            tickers = exchange.Fetch_Tickers()

            for asset in assets:
                print( "Asset: %s"%asset )

                ts, c = exchange.Fetch_Historical_Price( asset
                                                       , reference_asset
                                                       , tickers.keys()
                                                       , '5m'
                                                       )

                if ts is not None and c is not None:
                    df = df.append( { 'Exchange'  : exchange_id
                                    , 'Asset'     : asset
                                    , 'Timestamp' : ts[0] * 1e-3
                                    , 'Entries'   : len(ts)
                                    }
                                  , ignore_index = True
                                  )
                else:
                    df = df.append( { 'Exchange'  : exchange_id
                                    , 'Asset'     : asset
                                    , 'Timestamp' : ''
                                    , 'Entries'   : ''
                                    }
                                  , ignore_index = True
                                  )

                df.to_csv( 'OHCLV_meta_data.csv'
                         , index = False
            #            , sep = '\t'
                         )
        except:
            print( "Error: %s"%sys.exc_info()[0] )
