import datetime
import numpy  as np
import time
import ccxt


################################################################################
if __name__ == '__main__':
    exchange_id = 'hitbtc'
    #exchange_id = 'poloniex'
    #exchange = getattr (ccxt, exchange_id) ( keys )
    exchange = getattr (ccxt, exchange_id) ( )

    symbol = 'ETH/BTC'

    query_timestamp_ms = exchange.milliseconds () - 2*365*24*60*60*1000 # Starting from two years age
#    query_timestamp_ms = None
    sample_time_ms = 5*60*1000


    ####################################
    print( "Symbol                         : %s"%symbol             )
    print("#"*10)

    for ii in [1,2,3]:
        time.sleep(1.)
        data = exchange.fetch_ohlcv( symbol
                                   , timeframe = '5m'
                                   , since     = query_timestamp_ms # UTC timestamp in milliseconds
                                   , limit     = None
                                   )

        data = list(zip(*data))

        if len(data) == 0:
            break

        t_ms = np.asarray(data[0])
        dt = [datetime.datetime.fromtimestamp(ms / 1000.) for ms in data[0]]
        o = data[1]
        h = data[2]
        c = data[3]
        l = data[4]
        v = data[5]

        if not query_timestamp_ms is None:
            diff_sample = (t_ms[0] - query_timestamp_ms)/float(sample_time_ms)
            diff_day    = diff_sample/12./24
        duration_sample = (t_ms[-1] - t_ms[0])/float(sample_time_ms)
        duration_day    = duration_sample/12./24

        if not query_timestamp_ms is None:
            print( "Requested start timestamp [ms] : %d"%query_timestamp_ms )
        print(     "Actual start timestamp [ms]    : %d"%t_ms[0]            )
        print(     "Actual start [date time]       : %s"%dt[0]              )
        if not query_timestamp_ms is None:
            print( "Difference [samples]           : %.2f"%diff_sample      )
            print( "Difference [days]              : %.2f"%diff_day         )
        print(     "Actual stop timestamp [ms]     : %d"%t_ms[-1]           )
        print(     "Actual stop [date time]        : %s"%dt[-1]             )
        print(     "Actual samples                 : %d"%len(t_ms)          )
        print(     "Duration [samples]             : %g"%duration_sample    )
        print(     "Duration [days]                : %g"%duration_day       )
        print("#"*10)

        query_timestamp_ms = t_ms[-1] + sample_time_ms - 1
