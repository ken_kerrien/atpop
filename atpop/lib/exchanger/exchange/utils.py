################################################################################
timeframes = {  '1M' : 60*60*24*30.
             ,  '1w' : 60*60*24* 7.
             ,  '1d' : 60*60*24.
             ,  '4h' : 60*60* 4.
             ,  '1h' : 60*60.
             , '30m' : 60*30.
             , '15m' : 60*15.
             ,  '5m' : 60* 5.
             ,  '3m' : 60* 3.
             ,  '1m' : 60.
             }


################################################################################
def Extract_Precisions_Mins_Maxs( markets
                                , flow_asset_pairs
                                , qn, bn
                                ):
    precisions = []
    mins       = []
    maxs       = []
    for flow_asset_pair in flow_asset_pairs:
        symbol      = flow_asset_pair[bn] + '/' + flow_asset_pair[qn]
        precisions.append( markets[symbol]['precision']['amount']  )
        mins.append(      markets[symbol]['limits']['cost']['min'] )
        maxs.append(      markets[symbol]['limits']['cost']['max'] )
    
    return precisions, mins, maxs
