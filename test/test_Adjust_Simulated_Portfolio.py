import unittest

import sys
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )

from atpop.lib.exchanger.utils.portfolio_rebalance_math.utils  import Generate_Asset_Vector
from atpop.lib.exchanger.utils.portfolio_handler  import Adjust_Simulated_Portfolio
from atpop.lib.exchanger.utils.names              import QUOTE_NAME,\
                                                         BASE_NAME ,\
                                                         FROM_NAME ,\
                                                         TO_NAME

import numpy as np

class Res:
  pass


################################################################################
class Adjust_Simulated_Portfolio_Base():
  def test(self):
    portfolio_res = \
      Adjust_Simulated_Portfolio( self.portfolio
                                , self.symbol
                                , self.amount_in_base
                                , self.buy_base_not_sell
                                , self.res
                                , self.reference_asset
                                , self.asset_pairs
                                , qn = QUOTE_NAME
                                , bn = BASE_NAME
                                , fn = FROM_NAME
                                , tn = TO_NAME
                                )
    if ( set(portfolio_res.keys()) != set(self.portfolio_res.keys()) ):
      self.assertTrue( set(portfolio_res.keys()) == set(self.portfolio_res.keys()) )

    portfolio_res_vec = \
      Generate_Asset_Vector( portfolio_res
                           , portfolio_res.keys()
                           )
    portfolio_res_target_vec = \
      Generate_Asset_Vector( self.portfolio_res
                           , portfolio_res.keys()
                           )

    abs_diff = np.abs( np.asarray(portfolio_res_target_vec) - np.asarray(portfolio_res_vec) )
    rel_diff = np.asarray( abs_diff / np.asarray(portfolio_res_target_vec) )
    self.assertTrue( all( rel_diff < 0.001 ) )


################################################################################
class Case_01( Adjust_Simulated_Portfolio_Base
             , unittest.TestCase
             ):
  ######################################
  def setUp(self):
    self.portfolio = \
      { 'DAI': 0.1
      , 'BTC': 0.1
      , 'ETH': 9.
      , 'TUSD': 0.1
      , 'EURS': 0.1
      , 'LTC': 25.
      , 'EOS': 400.
      , 'HBAR': 1000.
      , 'USDT': 0.1
      }
    self.symbol = \
      "ETH/USDT"
    self.amount_in_base = \
      8.
    self.buy_base_not_sell = \
      False
    self.res = Res()
    self.res.ordered_flow_idxs = \
      [4, 5, 9, 12, 15, 16, 17, 1, 2]
    self.res.ordered_trade_asset_pairs = \
      [ {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 0.9991, 'trade_fee': 0.0025, 'from': 'DAI', 'to': 'USDT'}, {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.002906, 'trade_fee': 0.0025, 'from': 'TUSD', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 62859.38, 'trade_fee': 0.0025, 'from': 'BTC', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.13222, 'trade_fee': 0.0025, 'from': 'EURS', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 4484.802, 'trade_fee': 0.0025, 'from': 'ETH', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 4.73553, 'trade_fee': 0.0025, 'from': 'EOS', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'HBAR', 'exchange_rate': 0.414261, 'trade_fee': 0.0025, 'from': 'HBAR', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'WIN', 'exchange_rate': 0.00092259, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'WIN'}, {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 256.7353, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'LTC'}]
    self.res.flows = \
      [ 0.00000000e+00, 3.96354289e+04, 1.25949390e+04, 0.00000000e+00
      , 8.06861169e-01, 3.00789598e-01, 0.00000000e+00, 0.00000000e+00
      , 0.00000000e+00, 1.69833480e-01, 0.00000000e+00, 0.00000000e+00
      , 2.68692348e-01, 0.00000000e+00, 0.00000000e+00, 8.79852866e+00
      , 3.84490000e+02, 9.74000000e+02]
    self.res.flow_asset_pairs = \
      [ {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 62859.38, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'BTC'}, {'quote': 'USDT', 'base': 'WIN', 'exchange_rate': 0.00092259, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'WIN'}, {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 256.7353, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'LTC'}, {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.13222, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'EURS'}, {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 0.9991, 'trade_fee': 0.0025, 'from': 'DAI', 'to': 'USDT'}, {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.002906, 'trade_fee': 0.0025, 'from': 'TUSD', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 4484.802, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'ETH'}, {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 4.73553, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'EOS'}, {'quote': 'USDT', 'base': 'HBAR', 'exchange_rate': 0.414261, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'HBAR'}, {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 62859.38, 'trade_fee': 0.0025, 'from': 'BTC', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'WIN', 'exchange_rate': 0.00092259, 'trade_fee': 0.0025, 'from': 'WIN', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 256.7353, 'trade_fee': 0.0025, 'from': 'LTC', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.13222, 'trade_fee': 0.0025, 'from': 'EURS', 'to': 'USDT'}, {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 0.9991, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'DAI'}, {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.002906, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'TUSD'}, {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 4484.802, 'trade_fee': 0.0025, 'from': 'ETH', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 4.73553, 'trade_fee': 0.0025, 'from': 'EOS', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'HBAR', 'exchange_rate': 0.414261, 'trade_fee': 0.0025, 'from': 'HBAR', 'to': 'USDT'}]
    self.res.conn_matrix = \
      [ [0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1.0, 1.0, 1.0, 1.0, -0.9975, -0.9975, 1.0, 1.0, 1.0, -0.9975, -0.9975, -0.9975, -0.9975, 1.0, 1.0, -0.9975, -0.9975, -0.9975], [-0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0]]
    self.res.ordered_asset_names = \
      ['WIN', 'ETH', 'TUSD', 'EURS', 'USDT', 'BTC', 'LTC', 'HBAR', 'EOS', 'DAI']
    self.res.precision_array = \
      [ 1.e-05, 1.e+02, 1.e-03, 1.e-02, 1.e-02, 1.e-02, 1.e-04, 1.e-02, 1.e+00, 1.e-05, 1.e+02, 1.e-03, 1.e-02, 1.e-02, 1.e-02, 1.e-04, 1.e-02, 1.e+00]
    self.res.min_array = \
      [ 1.e-07, 1.e-07, 1.e-07, 1.e-07, 1.e-07, 1.e-08, 1.e-07, 1.e-07, 1.e-06, 1.e-07, 1.e-07, 1.e-07, 1.e-07, 1.e-07, 1.e-08, 1.e-07, 1.e-07, 1.e-06]
    self.res.max_array = \
      [ None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]
    self.res.conversion_to_base_array = \
      [ 1.59085247e-05, 1.08390509e+03, 3.89506235e-03, 8.83220576e-01
      , 1.00090081e+00, 9.97102420e-01, 2.22975284e-04, 2.11169605e-01
      , 2.41393711e+00, 1.00000000e+00, 1.00000000e+00, 1.00000000e+00
      , 1.00000000e+00, 1.00000000e+00, 1.00000000e+00, 1.00000000e+00
      , 1.00000000e+00, 1.00000000e+00]
    self.res.gain_to_base_array = \
      [ 0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 1., 1., 1., 1., 1., 1., 1., 1., 1.]
    self.res.adjusted_flow_array_base = \
      [ 0.00000000e+00, 4.28517202e+07, 4.89332344e+01, 0.00000000e+00
      , 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00
      , 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00
      , 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 7.99815814e+00
      , 3.84490000e+02, 9.74000000e+02]
    self.reference_asset = \
      "USDT"
    self.asset_pairs = \
      [ {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 62859.38, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'WIN', 'exchange_rate': 0.00092259, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 256.7353, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.13222, 'trade_fee': 0.0025}, {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 0.9991, 'trade_fee': 0.0025}, {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.002906, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 4484.802, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 4.73553, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'HBAR', 'exchange_rate': 0.414261, 'trade_fee': 0.0025}]
    self.portfolio_res = \
      { 'DAI': 0.1
      , 'BTC': 0.1
      , 'ETH': 9. - self.amount_in_base
      , 'TUSD': 0.1
      , 'EURS': 0.1
      , 'LTC': 25.
      , 'EOS': 400.
      , 'HBAR': 1000.
      , 'USDT': 0.1 + 4484.802*self.amount_in_base*0.9975
      }


################################################################################
class Case_02( Adjust_Simulated_Portfolio_Base
             , unittest.TestCase
             ):
  ######################################
  def setUp(self):
    self.portfolio = \
      { 'EOS': 0.4
      , 'BTC': 0.1
      , 'ETH': 0.8
      , 'LTC': 24.
      , 'DAI': 0.8
      , 'NANO': 6600.
      , 'EURS': 0.3
      , 'TUSD': 0.3
      }
    self.symbol = \
      "LTC/USDT"
    self.amount_in_base = \
      55.
    self.buy_base_not_sell = \
      True
    self.res = Res()
    self.res.ordered_flow_idxs = \
      [4, 5, 9, 11, 14, 15, 2]
    self.res.ordered_trade_asset_pairs = \
      [{'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 1.00052, 'trade_fee': 0.0025, 'from': 'DAI', 'to': 'USDT'}, {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.00351, 'trade_fee': 0.0025, 'from': 'TUSD', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 60881.11, 'trade_fee': 0.0025, 'from': 'BTC', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.13274, 'trade_fee': 0.0025, 'from': 'EURS', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 4339.508, 'trade_fee': 0.0025, 'from': 'ETH', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 4.51926, 'trade_fee': 0.0025, 'from': 'EOS', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 246.5829, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'LTC'}]
    self.res.flows = \
      [0.00000000e+00, 0.00000000e+00, 1.37817043e+04, 0.00000000e+00
      ,8.06861169e-01, 3.00789598e-01, 0.00000000e+00, 0.00000000e+00
      ,0.00000000e+00, 1.69833480e-01, 0.00000000e+00, 2.68692348e-01
      ,0.00000000e+00, 0.00000000e+00, 8.00370516e-01, 4.35772164e-01]
    self.res.flow_asset_pairs = \
      [{'quote': 'USDT', 'base': 'NANO', 'exchange_rate': 5.773708, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'NANO'}, {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 60881.11, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'BTC'}, {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 246.5829, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'LTC'}, {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.13274, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'EURS'}, {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 1.00052, 'trade_fee': 0.0025, 'from': 'DAI', 'to': 'USDT'}, {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.00351, 'trade_fee': 0.0025, 'from': 'TUSD', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 4339.508, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'ETH'}, {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 4.51926, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'EOS'}, {'quote': 'USDT', 'base': 'NANO', 'exchange_rate': 5.773708, 'trade_fee': 0.0025, 'from': 'NANO', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 60881.11, 'trade_fee': 0.0025, 'from': 'BTC', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 246.5829, 'trade_fee': 0.0025, 'from': 'LTC', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.13274, 'trade_fee': 0.0025, 'from': 'EURS', 'to': 'USDT'}, {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 1.00052, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'DAI'}, {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.00351, 'trade_fee': 0.0025, 'from': 'USDT', 'to': 'TUSD'}, {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 4339.508, 'trade_fee': 0.0025, 'from': 'ETH', 'to': 'USDT'}, {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 4.51926, 'trade_fee': 0.0025, 'from': 'EOS', 'to': 'USDT'}]
    self.res.conn_matrix = \
      [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0], [0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [1.0, 1.0, 1.0, 1.0, -0.9975, -0.9975, 1.0, 1.0, -0.9975, -0.9975, -0.9975, -0.9975, 1.0, 1.0, -0.9975, -0.9975], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0], [0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0], [-0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, -0.9975, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.9975, 0.0, 0.0]]
    self.res.ordered_asset_names = \
      ['EOS', 'BTC', 'USDT', 'ETH', 'LTC', 'DAI', 'NANO', 'EURS', 'TUSD']
    self.res.precision_array = \
      [1.e-02, 1.e-05, 1.e-03, 1.e-02, 1.e-02, 1.e-02, 1.e-04, 1.e-02, 1.e-02, 1.e-05
      ,1.e-03, 1.e-02, 1.e-02, 1.e-02, 1.e-04, 1.e-02]
    self.res.min_array = \
      [1.e-08, 1.e-07, 1.e-07, 1.e-07, 1.e-07, 1.e-08, 1.e-07, 1.e-07, 1.e-08, 1.e-07
      ,1.e-07, 1.e-07, 1.e-07, 1.e-08, 1.e-07, 1.e-07]
    self.res.max_array = \
      [None, None, None, None, None, None, None, None, None, None, None, None, None, None
      ,None, None]
    self.res.conversion_to_base_array = \
      [1.73198922e-01, 1.64254561e-05, 4.05543126e-03, 8.82815121e-01
      ,9.99480270e-01, 9.96502277e-01, 2.30440870e-04, 2.21275165e-01
      ,1.00000000e+00, 1.00000000e+00, 1.00000000e+00, 1.00000000e+00
      ,1.00000000e+00, 1.00000000e+00, 1.00000000e+00, 1.00000000e+00]
    self.res.gain_to_base_array = \
      [0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 0.9975, 1.,     1.
      ,1.,     1.,     1.,     1.,     1.,     1.,    ]
    self.res.adjusted_flow_array_base = \
      [ 0.,          0.,         54.87350351,  0.,          0.,          0.
      , 0.,          0.,          0.,          0.,          0.,          0.
      , 0.,          0.,          0.,          0.        ]
    self.reference_asset = \
      "USDT"
    self.asset_pairs = \
      [{'quote': 'USDT', 'base': 'NANO', 'exchange_rate': 5.773708, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 60881.11, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 246.5829, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.13274, 'trade_fee': 0.0025}, {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 1.00052, 'trade_fee': 0.0025}, {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.00351, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 4339.508, 'trade_fee': 0.0025}, {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 4.51926, 'trade_fee': 0.0025}]
    self.portfolio_res = \
      {'EOS': 0.4357721643664056, 'BTC': 0.16983348000000004, 'ETH': 0.8003705160147485, 'LTC': 79.52250351024043, 'DAI': 0.806861168599994, 'NANO': 6618.827697533706, 'EURS': 0.2686923475999947, 'TUSD': 0.30078959835998376}
    self.portfolio_res_in_ref = \
      {'EOS': 1.969367711534522, 'BTC': 10339.650777562802, 'ETH': 3473.2142572101293, 'LTC': 19608.889530815264, 'DAI': 0.8064418188541898, 'NANO': 38215.178427871935, 'EURS': 0.30435856982041803, 'TUSD': 0.2997375196659563}
    self.portfolio_res_total_in_ref = \
      71640.31289908

  ######################################
  def test(self):
    with self.assertRaises(AssertionError):
      Adjust_Simulated_Portfolio_Base.test(self)


################################################################################
if __name__ == '__main__':
  unittest.main()
