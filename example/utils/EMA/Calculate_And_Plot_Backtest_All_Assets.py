#%%
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')

get_ipython().magic(u'matplotlib inline')
import matplotlib.pylab as pylab
fig_height_in_init_conf = 4
pylab.rcParams['figure.figsize'] = int(fig_height_in_init_conf*1.6180339887), fig_height_in_init_conf
pylab.rcParams['figure.dpi'] = 150

#%config InlineBackend.figure_format = 'retina'

working_directory_init_conf = get_ipython().magic(u'pwd')


#%%
from os import path
ARTIFACTS_PATH = path.abspath( path.join( working_directory_init_conf
                                        , '../../services/artifacts'
                                        )
                             )
STATE_ARTIFACTS_PATH = path.join( ARTIFACTS_PATH, 'state' )
LOG_ARTIFACTS_PATH   = path.join( ARTIFACTS_PATH,   'log' )
DEBUG_ARTIFACTS_PATH = path.join( ARTIFACTS_PATH, 'debug' )


#%%
from matplotlib import style
style.use('default')


#%%
import sys, os
sys.path.append( os.path.join( working_directory_init_conf
                             , r'../..'
                             )
               )

from lib.trade_strategy.EMA.setup       import Setup
from lib.trade_strategy.EMA.backtest    import Calculate_And_Plot_Backtest_With_Util_Signals
from lib.trade_strategy.par_defs        import Convert_Par_Dict_To_Pars
from lib.trade_strategy.name_mangling   import Get_Name_Suffix,\
                                               Add_Suffix_To_Name

from atpop.lib.exchanger.exchange.utils import timeframes
from atpop.lib.data.price_data          import Price_Data
from atpop.lib.data.opt_par_data        import Opt_Par_Data

from services.config import Asset_Price_Setup
from services.config import Opt_Setup


#%%
opt_par_data = Opt_Par_Data( path = STATE_ARTIFACTS_PATH )
opt_par_data.Read_CSV()

price_data = Price_Data( path = STATE_ARTIFACTS_PATH )
price_data.Read_CSV()

opt_par_assets =\
    { Get_Name_Suffix( strategy
                     , Setup.strategy
                     )
      for strategy in opt_par_data.Get_Strategies()
    }

assets_price_time,\
assets_price_record =\
    price_data.Get_Prices_For_Assets_On_Same_Time_Scale( timeframes[Asset_Price_Setup.timeframe]
                                                       , history               = Asset_Price_Setup.history
                                                       , try_to_include_assets = opt_par_assets
                                                       )

for asset in opt_par_assets.intersection( price_data.Get_Assets() ):
    par_dict =\
        opt_par_data.Get_Latest_Par_Dict( Add_Suffix_To_Name( Setup.strategy
                                                            , asset
                                                            )
                                        )
    pars = Convert_Par_Dict_To_Pars( Setup.par_defs, par_dict )

    print( asset )
    Calculate_And_Plot_Backtest_With_Util_Signals( assets_price_time
                                                 , assets_price_record
                                                 , pars
                                                 , asset
                                                 , "reference"
                                                 , Opt_Setup.trade_fee
                                                 , Opt_Setup.half_life
                                                 , disp_hist = int(2*Opt_Setup.half_life)
                                                 , title     = asset
                                                 )
