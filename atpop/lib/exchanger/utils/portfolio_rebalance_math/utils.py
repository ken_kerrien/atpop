from ..names import EXCHANGE_RATE_NAME,\
                    TRADE_FEE_NAME


################################################################################
class Portfolio_Has_Unknown_Assets_Exception(Exception):
    pass

class Asset_Conversion_Exception(Exception):
    pass

class Portfolio_Rebalance_Exception(Exception):
    pass


###############################################################################
def Generate_Asset_Vector(portfolio_dict, ordered_asset_names):
    # WARNING: This will only extract the ordered assests from portfolio
    # If there are additional assets in the portfolio not among the ordered assets,
    # it will be ignored

    p = dict(portfolio_dict)

    asset_vector = []
    for asset_name in ordered_asset_names:
        if asset_name in p:
            asset_vector.append( p[asset_name] )
            del p[asset_name]
        else:
            asset_vector.append( 0. )

    if p: # Make sure we looked at all assets in our portfolio
        raise Portfolio_Has_Unknown_Assets_Exception

    return asset_vector


###############################################################################
def Generate_Gain_Vector( assets
                        , ref_assets
                        , asset_pairs
                        , qn, bn
                        ):
    if isinstance(ref_assets, str):
        ref_assets = [ref_assets]*len(assets)

    gain_vect = []

    for asset, ref_asset in zip(assets, ref_assets):

        gain = None

        if asset == ref_asset:
            gain = 1.
        else:
            for asset_pair in asset_pairs:
                if asset_pair[qn] == ref_asset and asset_pair[bn] == asset:
                    gain = 1. - asset_pair[TRADE_FEE_NAME]
                    break
                elif asset_pair[bn] == ref_asset and asset_pair[qn] == asset:
                    gain = 1. - asset_pair[TRADE_FEE_NAME]
                    break

            if gain is None:
                raise Asset_Conversion_Exception

        gain_vect.append( gain )

    return gain_vect


###############################################################################
def Generate_Conversion_Vector( assets
                              , ref_assets
                              , asset_pairs
                              , qn, bn
                              ):
    if isinstance(ref_assets, str):
        ref_assets = [ref_assets]*len(assets)

    conv_vect = []

    for asset, ref_asset in zip(assets, ref_assets):

        exchange_rate = None

        if asset == ref_asset:
            exchange_rate = 1.
        else:
            for asset_pair in asset_pairs:
                if asset_pair[qn] == ref_asset and asset_pair[bn] == asset:
                    exchange_rate = asset_pair[EXCHANGE_RATE_NAME]
                    break
                elif asset_pair[bn] == ref_asset and asset_pair[qn] == asset:
                    exchange_rate = 1./asset_pair[EXCHANGE_RATE_NAME]
                    break

            if exchange_rate is None:
                raise Asset_Conversion_Exception

        conv_vect.append( exchange_rate )

    return conv_vect


###############################################################################
def Generate_Connectivity_Matrix_And_Flow_Origin_Names_And_Flow_Asset_Pairs( asset_pairs
                                                                           , ordered_asset_names
                                                                           , qn, bn
                                                                           , fn, tn
                                                                           ):
    # Connectivity matrix
    # rows    -> ordered_asset_names
    # columns -> asset_pairs
    conn_matrix = []
    for asset_name in ordered_asset_names:
        flow_f = []
        flow_b = []
        for asset_pair in asset_pairs:
            if asset_pair[qn] == asset_name:
                flow_f.append(1.)
                flow_b.append( -(1. - asset_pair[TRADE_FEE_NAME]) )
            elif asset_pair[bn] == asset_name:
                flow_f.append( -(1. - asset_pair[TRADE_FEE_NAME]) )
                flow_b.append(1.)
            else:
                flow_f.append(0.)
                flow_b.append(0.)
        conn_matrix.append( flow_f + flow_b )

    assert conn_matrix

    # Flow start names
    on_f = []
    on_b = []
    for asset_pair in asset_pairs:
        on_f.append( asset_pair[qn] )
        on_b.append( asset_pair[bn] )
    on = on_f + on_b

    # Flow asset pairs
    fap_f = []
    fap_b = []
    for asset_pair in asset_pairs:
        fap_f.append( dict(asset_pair) )
        fap_f[-1][fn] = fap_f[-1][qn]
        fap_f[-1][tn] = fap_f[-1][bn]

        fap_b.append( dict(asset_pair) )
        fap_b[-1][fn] = fap_b[-1][bn]
        fap_b[-1][tn] = fap_b[-1][qn]

    flow_asset_pairs = fap_f + fap_b

    return conn_matrix, on, flow_asset_pairs
