from ..name_mangling import Split_Name, Add_Suffix_To_Name


################################################################################
def Consolidate_Single_Asset_Portfolio_Frac_Precursor_Strategies( portfolio_frac_prec_strategies
                                                                , strategy_prefix
                                                                , reference_asset
                                                                ):
    new_portfolio_frac_prec_strategies = {}
    for strategy, portfolio_frac_precursor in portfolio_frac_prec_strategies.items():
        strategy_base_name, asset = Split_Name(strategy)
        if strategy_base_name != strategy_prefix:
            continue

        assert len( portfolio_frac_precursor ) == 1
        asset     = portfolio_frac_precursor.keys()[0]
        frac_prec = portfolio_frac_precursor.values()[0]
        if asset == reference_asset:
            continue

        new_portfolio_frac_prec_strategies[ Add_Suffix_To_Name( strategy_prefix, asset) ] =\
            { asset: frac_prec }

    if not new_portfolio_frac_prec_strategies:
        new_portfolio_frac_prec_strategies[ Add_Suffix_To_Name( strategy_prefix, reference_asset) ] =\
            { reference_asset: 1. }

    return new_portfolio_frac_prec_strategies
