# Setup logging
from ...log_func import logger

from numpy import isnan
import time
from .names import EXCHANGE_RATE_NAME


################################################################################
class Asset_Pair_Handler():
    ############################################################################
    def __init__( self
                , bn, qn
                , asset_pairs = []
                ):
        self.bn = bn
        self.qn = qn

        self.asset_pairs = asset_pairs


    ############################################################################
    def Generate_Asset_Pairs( self
                            , symbol_list
                            ):
        # This function will take a symbol list, and return asset pairs
        # while checking for duplication and redundancy
        # So, for example, if markets have "ETH/BTC" and "BTC/ETH",
        # we'll throw a hissy fit

        self.asset_pairs = []
        for symbol in symbol_list:
            sym_split = symbol.split('/')
            if len(sym_split) != 2:
                logger.warning( "Symbol does not have a 'base/quote' format: %s"
                              , symbol
                              )
                continue

            base  = sym_split[0]
            quote = sym_split[1]

            if base == "" or quote == "":
                if base == "":
                    logger.warning( "Symbol base is empty: %s"
                                  , symbol
                                  )
                if quote == "":
                    logger.warning( "Symbol quote is empty: %s"
                                  , symbol
                                  )

                continue

            d_inv = { self.qn : base
                    , self.bn : quote
                    }

            assert not (d_inv in self.asset_pairs)

            d = { self.qn : quote
                , self.bn : base
                }

            assert not (d in self.asset_pairs)

            self.asset_pairs.append(d)


    ################################################################################
    def Filter_To_Contain_Asset( self
                               , reference_asset
                               , asset_list = None
                               ):
        def Check(one_name, other_name):
            if asset_pair[one_name] != reference_asset:
                return False

            if not asset_list is None and not asset_pair[other_name] in asset_list:
                return False

            return True


        asset_pairs_new = []

        for asset_pair in self.asset_pairs:
            if Check(self.bn, self.qn):
                asset_pairs_new.append( dict(asset_pair) )
                continue

            if Check(self.qn, self.bn):
                asset_pairs_new.append( dict(asset_pair) )
                continue

        self.asset_pairs = asset_pairs_new


    ############################################################################
    def Filter_Around_Asset( self
                           , reference_asset
                           ):
        associates = [reference_asset]

        for asset_pair in self.asset_pairs:
            if asset_pair[self.qn] == reference_asset:
                associates.append( asset_pair[self.bn] )
            if asset_pair[self.bn] == reference_asset:
                associates.append( asset_pair[self.qn] )

        asset_pairs_new = []

        for asset_pair in self.asset_pairs:
            if (asset_pair[self.qn] in associates) and (asset_pair[self.bn] in associates):
                asset_pairs_new.append( dict(asset_pair) )

        self.asset_pairs = asset_pairs_new


    ############################################################################
    def Filter_All_Possible_Flows( self
                                 , source_assets
                                 , sink_assets
                                 ):
        ################################
        def Depth_First_Search_For_Asset_Pair_Idxs_Leading_To_Target( asset
                                                                    , target_assets
                                                                    , asset_pair_idxs_leading_to_target = set()
                                                                    , asset_pair_idxs_current_path      = set()
                                                                    ):
            ############################
            def Get_Asset_Pair_Idxs_Including_Asset( asset ):
                filtered_asset_pair_idxs = []

                for idx, asset_pair in enumerate(self.asset_pairs):
                    if asset in asset_pair.values():
                        filtered_asset_pair_idxs.append( idx )

                return filtered_asset_pair_idxs

            ############################
            def Get_Other_Asset( asset_pair_idx
                               , asset
                               ):
                for other_asset in self.asset_pairs[asset_pair_idx].values():
                    if other_asset != asset:
                        return other_asset


            ############################
            new_asset_pair_idxs_leading_to_target = set()
            for asset_pair_idx in Get_Asset_Pair_Idxs_Including_Asset( asset ):
                # Let's prevent loops
                if asset_pair_idx in asset_pair_idxs_current_path:
                    continue

                # This asset pair is already known to lead to target eventually
                if asset_pair_idx in asset_pair_idxs_leading_to_target:
                    new_asset_pair_idxs_leading_to_target.add( asset_pair_idx )
                    continue

                # This asset pair directly leads to target
                next_asset = Get_Other_Asset( asset_pair_idx
                                            , asset
                                            )
                if next_asset in target_assets:
                    new_asset_pair_idxs_leading_to_target.add( asset_pair_idx )
                    continue

                # Let's see if this asset pair leads to anything eventually
                rec_asset_pair_idxs_leading_to_target = \
                    Depth_First_Search_For_Asset_Pair_Idxs_Leading_To_Target( next_asset
                                                                            , target_assets
                                                                            , asset_pair_idxs_leading_to_target | new_asset_pair_idxs_leading_to_target
                                                                            , asset_pair_idxs_current_path | { asset_pair_idx }
                                                                            )

                # This asset pair did not lead anywhere
                if not rec_asset_pair_idxs_leading_to_target:
                    continue

                # This asset pair does eventually lead to a target!
                new_asset_pair_idxs_leading_to_target |= \
                    { asset_pair_idx } | rec_asset_pair_idxs_leading_to_target

            return new_asset_pair_idxs_leading_to_target

        ################################
        new_asset_pair_idxs = set()
        for asset in source_assets:
            new_asset_pair_idxs |= Depth_First_Search_For_Asset_Pair_Idxs_Leading_To_Target( asset
                                                                                           , sink_assets
                                                                                           )

        self.asset_pairs = [ self.asset_pairs[idx] for idx in new_asset_pair_idxs ]


    ############################################################################
    def _Extract_Vals( self
                     , source
                     , tuple_list
                     ):
        for asset_pair in self.asset_pairs:
            symbol = asset_pair[self.bn] + '/' + asset_pair[self.qn]
            for t in tuple_list:
                asset_pair[ t[0] ] = source[symbol][ t[1] ]


    ############################################################################
    def _Extract_Float_Vals( self
                           , source
                           , field_list
                           , filter_flag = False
                           ):
        if filter_flag:
            asset_pairs_new = []

        for asset_pair in self.asset_pairs:
            symbol = asset_pair[self.bn] + '/' + asset_pair[self.qn]
            no_errors = True
            for field in field_list:
                try:
                    last_val = float( source[symbol][ field[1] ] )
                except (ValueError, TypeError):
                    no_errors = False
                    continue

                if isnan(last_val):
                    no_errors = False
                    continue

                asset_pair[ field[0] ] = last_val
            if no_errors and filter_flag:
                asset_pairs_new.append( dict(asset_pair) )

        if filter_flag:
            self.asset_pairs = asset_pairs_new


    ############################################################################
    def Extract_Trade_Fees( self
                          , markets
                          , filter_flag = False
                          ):
        self._Extract_Float_Vals( markets
                                , [ ('trade_fee', 'taker')
                                  ]
                                , filter_flag
                                )


    ############################################################################
    def Extract_Volumes( self
                       , tickers
                       , qvn, bvn
                       , filter_flag = False
                       ):
        self._Extract_Float_Vals( tickers
                                , [ (bvn, bvn)
                                  , (qvn, qvn)
                                  ]
                                , filter_flag
                                )


    ############################################################################
    def Extract_Exchange_Rate( self
                             , tickers
                             , filter_flag = False
                             ):
        if filter_flag:
            asset_pairs_new = []

        for asset_pair in self.asset_pairs:
            symbol = asset_pair[self.bn] + '/' + asset_pair[self.qn]
            try:
                last_val = float( tickers[symbol]["last"] )
            except (ValueError, TypeError):
                continue

            if isnan(last_val) or last_val <= 0:
                continue

            asset_pair[EXCHANGE_RATE_NAME] = last_val

            if filter_flag:
                asset_pairs_new.append( dict(asset_pair) )

        if filter_flag:
            self.asset_pairs = asset_pairs_new


    ############################################################################
    def Query_Exchange_Rate( self
                           , ex
                           , tickers     = None
                           , filter_flag = False
                           ):
        if tickers is None:
            tickers = ex.Fetch_Tickers()

        #####################
        def Get_From_Tickers(symbol):
            try:
                last_val = float( tickers[symbol]["last"] )
            except (ValueError, TypeError):
                return

            if isnan(last_val) or last_val <= 0:
                return

            return last_val

        #####################
        def Get_From_OHCLV(symbol):
            since_s = time.time() - 3 * 60 * 60. # 3 hours
            _, _, _, c, _, _ = ex.Fetch_OHCLV( symbol
                                             , '5m'
                                             , since_s
                                             )
            for ce in reversed(c):
                try:
                    last_val = float( ce )
                except (ValueError, TypeError):
                    continue

                if isnan( last_val ) or last_val <= 0:
                    continue

                break
            else:
                return

            return last_val

        #####################
        if filter_flag:
            asset_pairs_new = []

        for asset_pair in self.asset_pairs:
            symbol = asset_pair[self.bn] + '/' + asset_pair[self.qn]

            last_val = Get_From_Tickers(symbol)

            if last_val is None:
                last_val = Get_From_OHCLV(symbol)

                if last_val is None:
                    continue

            asset_pair[EXCHANGE_RATE_NAME] = last_val

            if filter_flag:
                asset_pairs_new.append( dict(asset_pair) )

        if filter_flag:
            self.asset_pairs = asset_pairs_new


################################################################################
def Check_Assets_Are_Connected( asset_pairs
                              , source_assets
                              , sink_assets
                              , bn, qn
                              ):
    asset_cluster = { source_assets[0] }
    while True:
        asset_cluster_changed = False

        for asset_pair in asset_pairs:
            if not asset_pair[bn] in asset_cluster and not asset_pair[qn] in asset_cluster:
                continue

            if asset_pair[bn] in asset_cluster and asset_pair[qn] in asset_cluster:
                continue

            asset_cluster_changed = True
            asset_cluster.add( asset_pair[qn] )
            asset_cluster.add( asset_pair[bn] )

        if asset_cluster_changed == False:
            break

    for asset in source_assets:
        if not asset in asset_cluster:
            print("%s is NOT in portfolio!"%asset)

    for asset in sink_assets:
        if not asset in asset_cluster:
            print("%s is NOT in portfolio_frac_target!"%asset)

    all_assets = source_assets + sink_assets
    new_asset_pairs = []
    for asset_pair in asset_pairs:
        if asset_pair[bn] in all_assets and asset_pair[qn] in all_assets:
            new_asset_pairs.append( asset_pair )
#        asset_pairs = new_asset_pairs

    return new_asset_pairs
