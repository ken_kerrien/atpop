# ATPOP – Automated Trade & Portfolio Optimizing Platform

A Python library implementing the fundamental infrastructure for automated (mostly) cryptocurrency trading and portfolio management - NOT including any actual trade decision making algorithms.

### [Install](#install) · [Usage](#usage) · [Manual](#manual) · [FAQ](#FAQ) · [Example](#example)

The **ATPOP** library is used to connect and trade with cryptocurrency exchanges in order to manage a portfolio of assets. It does NOT include any particular algorithms or methods to actually determine the right trades; It only executes them as provided by other software components utilizing this library.

The library provides a simplified approach to trading and portfolio management. It is intended for **coders, developers, technically-skilled traders, data-scientists and financial analysts** for developing trading algorithms without the hastle of having to develop the underlying infrastructure as well.

Current feature list:

- support for many cryptocurrency exchanges - thanks to the outstanding CCXT library
- fully automated data collection (asset prices and volumes) for local storage
- fully automated portfolio rebalancing - with the user providing the actual decision making algorithm
- continuous optimization of algorithm parameters in order to adapt to an ever changing environment
- simulated portfolio to test out multiple algorithms over time without the risk
- meticulous logging
- works in Python 3.5 and above

### Problem statement and motivation

This library will solve the following problem for you:

Let's assume you'd like to invest your hard-earned money in a portfolio of assets, like stocks or cryptocurrency, at some exchange. You would like to pick the assets that yield the most return, however that set of good investment assests constantly changes. History books are littered with companies that were a good investment at one point. So, in order to safeguard your investments, you always have to keep an eye on your portfolio and modify it according to market trends. But you have a life and you really don't want to spend every waking moment monitoring charts. At the same time, you have already developed a good understanding of what you have to be looking out for.

That's where atpop comes in to help you! If you can encode your trade decisions into algorithms that give simple portfolio objectives, like "75% BTC and 25% ETH", atpop will take care of the rest. Atpop will take all the necessary steps to achieve that portfolio the most efficient way instantaneously.

Furthermore, you realize that your algorithm needs to be fine-tuned given that markets change all the time. Probably there are a handful of parameters you have to optimize by backtesting on historical and most recent data.

Once again, atpop is there to save the day. You define your optimization approach, which can be really anything - even machine learning and AI. Atpop, on the other hand, will provide all the historical data and initiate the optimization when new data becomes available. Atpop will make sure your trading algorithm has the most up-to-date data and parameters to work with. 

---

## Install

Unfortunately, ATPOP can not be installed from a package manager just yet. You'll have to clone it to your system from the [atpop Bitbucket repository](https://bitbucket.org/ken_kerrien/atpop/src/master):

```shell
git clone https://bitbucket.org/ken_kerrien/atpop.git
```

Then you can run the install script within the atpop directory:

```shell
bash ./install.sh
```

OR just run the install command directly from within the atpop directory:

```shell
sudo python3 -m pip install -e .
```

---

## Usage

### Intro

The atpop library allows you to define three services. Each service is essentially a stand-alone Python application that you run from the terminal. These applications are only weakly linked via .csv files and nothing else, so you may chose to run any one of them alone as well. The .csv file names can be arbitrarily chosen or left as default.

- Exchanger: This service periodically updates historical price data (e.g.: price.csv) and watches the portfolio target file (e.g.: portfolio_frac_precursor.csv). If the target changes, it will communicate with the exchange online, and issue a sequence of trade commands that should achieve the targeted portfolio.

- Trader: This service watches the trade algorithm parameters (e.g.: opt_par.csv) and the historical price data (e.g.: price.csv). If any of those changes, it recalculates the optimal portfolio, and writes that to the portfolio target file (e.g.: portfolio_frac_precursor.csv).

- Trade strategy updater: This service watches the historical price data (e.g.: price.csv). If it changes, it recalculates the optimal parameters for the trade strategy, and writes that to the trade algorithm parameters (e.g.: opt_par.csv).

![ATPOP services block diagram](services_block_diagram.svg "ATPOP services block diagram")

### Use case 1: Exchanger alone

You may decide to run the Exchanger service alone.

![ATPOP services block diagram case 1](services_block_diagram_case_1.svg "ATPOP services block diagram case 1")

The advantage of this is that now you can trade by simply editing a .csv file. You can literally have your portfolio open in your favorite spreadhseet editor, put in a portfolio objective, like "75% BTC and 25% ETH", hit save, and the service itself will perform all the trading action on your behalf to achive this. Also, the service will download the relevant asset prices for you as well into another .csv file that you can open and plot with your favorite tools.

Let's have a look at the portfolio objective .csv file first, which is called "portfolio_frac_precursor.csv" by default, but it can be renamed.

WORK IN PROGRESS...

---

## Documentation
