from toposort import toposort, toposort_flatten


###############################################################################
def Topologically_Sort_Flows( flow_vals
                            , flow_asset_pairs
                            , fn, tn
                            ):
    # assert len(flow_vals) == len(flow_asset_pairs)
    # Filter flow asset pairs: only keep non-zero flows
    trade_asset_pairs = []
    flow_idxs         = []
    for ii, (flow_val, flow_asset_pair) in enumerate( zip(flow_vals, flow_asset_pairs) ):
        if flow_val <= 0:
            continue
        trade_asset_pairs.append( dict(flow_asset_pair) )
        flow_idxs.append( ii )

    # Generate DAG
    DAG = {}
    for ii, tr in enumerate( trade_asset_pairs ):
        deps = []
        for jj, dep_tr in enumerate( trade_asset_pairs ):
            if tr[fn] == dep_tr[tn]:
                deps.append(jj)
        DAG[ii] = set(deps)

    # Topological sort
    sorted_idxs = toposort_flatten(DAG)

    return [ flow_idxs[ii] for ii in sorted_idxs ]
