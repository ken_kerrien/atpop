import matplotlib.pylab as pylab
fig_height_in_init_conf = 4
pylab.rcParams['figure.figsize'] = int(fig_height_in_init_conf*1.6180339887), fig_height_in_init_conf
pylab.rcParams['figure.dpi'] = 150

#%config InlineBackend.figure_format = 'retina'

from os import path
ARTIFACTS_PATH = path.abspath( path.join( path.dirname( __file__ )
                                        , '../services/artifacts'
                                        )
                             )
STATE_ARTIFACTS_PATH = path.join( ARTIFACTS_PATH, 'state' )
LOG_ARTIFACTS_PATH   = path.join( ARTIFACTS_PATH,   'log' )
DEBUG_ARTIFACTS_PATH = path.join( ARTIFACTS_PATH, 'debug' )


import matplotlib.pyplot as plt
from matplotlib import style
style.use('default')
