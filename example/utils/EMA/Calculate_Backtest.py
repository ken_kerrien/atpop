import sys, os
sys.path.append( os.path.join( os.path.dirname(__file__)
                             , r'../..'
                             )
               )

from utils.common                       import *

from atpop.lib.exchanger.exchange.utils import timeframes

from atpop.lib.data.price_data          import Price_Data

from lib.trade_strategy.EMA.setup       import Setup
from lib.trade_strategy.EMA.backtest    import Calculate_Backtest_With_Util_Signals

from lib.trade_strategy.EMA.Backtest_Utils.Filter_compute  import Calculate_Score2

from services.config                    import Asset_Price_Setup,\
                                               Opt_Setup


################################################################################
if __name__ == '__main__':
    asset = 'BTC'

    price_data = Price_Data( path = STATE_ARTIFACTS_PATH )
    price_data.Read_CSV()

    assets_price_time, assets_price_record =\
        price_data.Get_Prices_For_Assets_On_Same_Time_Scale( timeframes[Asset_Price_Setup.timeframe]
                                                           , history = Asset_Price_Setup.history
                                                           )


    pars = \
    [  100
    ,  1000
    ]


    total_portfolio_val,\
    trade_idx_series,\
    portfolio_frac_precursor_series,\
    fast, slow, diff_rel,\
    util_scaled =\
        Calculate_Backtest_With_Util_Signals( assets_price_time
                                            , assets_price_record
                                            , pars
                                            , asset
                                            , "reference"
                                            , Opt_Setup.trade_fee
                                            , Opt_Setup.half_life
                                            )

    print( -Calculate_Score2(util_scaled) )
