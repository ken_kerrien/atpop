import unittest

import sys
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )

from atpop.lib.exchanger.utils.portfolio_rebalance_math import Rebalance_Portfolio
from atpop.lib.exchanger.utils.asset_pair_handler       import Asset_Pair_Handler
from atpop.lib.exchanger.utils.portfolio_handler        import Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs
from atpop.lib.exchanger.utils.names                    import QUOTE_NAME,\
                                                               BASE_NAME ,\
                                                               FROM_NAME ,\
                                                               TO_NAME

import numpy as np


################################################################################
class Rebalance_Portfolio_Base():
    def test(self):
        asset_pair_handler = Asset_Pair_Handler( BASE_NAME, QUOTE_NAME
                                               , asset_pairs = self.asset_pairs
                                               )
        asset_pair_handler.Filter_All_Possible_Flows( self.portfolio.keys()
                                                    , self.portfolio_frac_target.keys()
                                                    )

        _flow_vals, _flow_asset_pairs, _C,\
            (ordered_asset_vals, ordered_asset_names, _message) = \
            Rebalance_Portfolio( self.portfolio
                               , self.portfolio_frac_target
                               , asset_pair_handler.asset_pairs
                               , self.reference_asset
                               , self.portfolio_frac_tolerance
                               , QUOTE_NAME, BASE_NAME
                               , FROM_NAME, TO_NAME
                               , error_tolerance = self.error_tolerance
                               , save_debug_data = False
                               , debug_path      = '.'
                               )

        rebalanced_portfolio = {}
        for asset, val in zip(ordered_asset_names, ordered_asset_vals):
            if val <= 0.:
                continue

            rebalanced_portfolio[asset] = val

        rebalanced_portfolio_in_ref = \
            Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( rebalanced_portfolio
                                                                  , self.reference_asset
                                                                  , asset_pair_handler.asset_pairs
                                                                  , QUOTE_NAME, BASE_NAME
                                                                  )

        total_portfolio_in_ref = sum( rebalanced_portfolio_in_ref.values() )

        portfolio_frac_target_list     = []
        rebalanced_portfolio_frac_list = []
        for asset in set( ordered_asset_names ) | set ( self.portfolio_frac_target.keys() ):
            if asset in self.portfolio_frac_target:
                val = self.portfolio_frac_target[asset]
            else:
                val = 0.
            portfolio_frac_target_list.append(val)

            if asset in rebalanced_portfolio_in_ref:
                val = rebalanced_portfolio_in_ref[asset] / total_portfolio_in_ref
            else:
                val = 0.
            rebalanced_portfolio_frac_list.append(val)

        abs_diff_frac = np.abs( np.asarray(portfolio_frac_target_list) - np.asarray(rebalanced_portfolio_frac_list) )

        self.assertTrue( all( abs_diff_frac < self.portfolio_frac_tolerance * 1.01 ) )


#r = [0.1, 0.2, 0.4, 0.3] # This failed the optimizer without  options = {"bland":True}
#r = [0.3, 0.5, 0.2, 0] # This failed the optimizer


################################################################################
class Case_01( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            {u'QTUM': 176.01, u'USDT': 0.014417128823083658, u'TRX': 18986.000000000007, u'BTC': 0.06531, u'TUSD': 0.00659478512, u'ETH': 2.866e-05}
        self.portfolio_frac_target = \
            {'TRX': 0.3333333333333333, 'BTC': 0.3333333333333333, 'QTUM': 0.3333333333333333}
        self.asset_pairs = \
            [ {'exchange_rate': 0.0932032, 'trade_fee': 0.002, 'base': u'XEM', 'quote': u'USDT'}
            , {'exchange_rate': 0.0152116, 'trade_fee': 0.002, 'base': u'IHT', 'quote': u'USDT'}
            , {'exchange_rate': 0.09, 'trade_fee': 0.002, 'base': u'MG', 'quote': u'USDT'}
            , {'exchange_rate': 0.0189999, 'trade_fee': 0.002, 'base': u'WIZ', 'quote': u'USDT'}
            , {'exchange_rate': 7.99997, 'trade_fee': 0.002, 'base': u'ATOM', 'quote': u'USDT'}
            , {'exchange_rate': 0.000362379, 'trade_fee': 0.002, 'base': u'SLX', 'quote': u'USDT'}
            , {'exchange_rate': 0.00290001, 'trade_fee': 0.002, 'base': u'EKO', 'quote': u'USDT'}
            , {'exchange_rate': 0.0480201, 'trade_fee': 0.002, 'base': u'MITH', 'quote': u'USDT'}
            , {'exchange_rate': 141.326, 'trade_fee': 0.002, 'base': u'LTC', 'quote': u'USDT'}
            , {'exchange_rate': 2.56706, 'trade_fee': 0.002, 'base': u'XUC', 'quote': u'USDT'}
            , {'exchange_rate': 116.2645, 'trade_fee': 0.002, 'base': u'XMR', 'quote': u'USDT'}
            , {'exchange_rate': 0.480349, 'trade_fee': 0.002, 'base': u'XRP', 'quote': u'USDT'}
            , {'exchange_rate': 0.0298114, 'trade_fee': 0.002, 'base': u'SNT', 'quote': u'USDT'}
            , {'exchange_rate': 7.52218, 'trade_fee': 0.002, 'base': u'EOS', 'quote': u'USDT'}
            , {'exchange_rate': 3.9e-05, 'trade_fee': 0.002, 'base': u'VOCO', 'quote': u'USDT'}
            , {'exchange_rate': 0.993725, 'trade_fee': 0.002, 'base': u'USDT', 'quote': u'TUSD'}
            , {'exchange_rate': 13.25779, 'trade_fee': 0.002, 'base': u'XZC', 'quote': u'USDT'}
            , {'exchange_rate': 0.007, 'trade_fee': 0.002, 'base': u'VET', 'quote': u'USDT'}
            , {'exchange_rate': 1.14179, 'trade_fee': 0.002, 'base': u'EURS', 'quote': u'USDT'}
            , {'exchange_rate': 0.175717, 'trade_fee': 0.002, 'base': u'DAY', 'quote': u'USDT'}
            , {'exchange_rate': 1.871968, 'trade_fee': 0.002, 'base': u'ETP', 'quote': u'USDT'}
            , {'exchange_rate': 0.00275, 'trade_fee': 0.002, 'base': u'CLO', 'quote': u'USDT'}
            , {'exchange_rate': 0.072621, 'trade_fee': 0.002, 'base': u'EDG', 'quote': u'USDT'}
            , {'exchange_rate': 0.00125549, 'trade_fee': 0.002, 'base': u'PAT', 'quote': u'USDT'}
            , {'exchange_rate': 0.00180974, 'trade_fee': 0.002, 'base': u'DWS', 'quote': u'USDT'}
            , {'exchange_rate': 0.0370869, 'trade_fee': 0.002, 'base': u'TRX', 'quote': u'USDT'}
            , {'exchange_rate': 0.002282611, 'trade_fee': 0.002, 'base': u'SWFTC', 'quote': u'USDT'}
            , {'exchange_rate': 37.7449, 'trade_fee': 0.002, 'base': u'BNB', 'quote': u'USDT'}
            , {'exchange_rate': 487.579, 'trade_fee': 0.002, 'base': 'BCH', 'quote': 'USDT'}
            , {'exchange_rate': 0.00210159, 'trade_fee': 0.002, 'base': u'DRT', 'quote': u'USDT'}
            , {'exchange_rate': 0.00052116, 'trade_fee': 0.002, 'base': u'MRS', 'quote': u'USDT'}
            , {'exchange_rate': 241.0685, 'trade_fee': 0.002, 'base': 'BSV', 'quote': 'USDT'}
            , {'exchange_rate': 0.001428679, 'trade_fee': 0.002, 'base': u'SENT', 'quote': u'USDT'}
            , {'exchange_rate': 0.9985, 'trade_fee': 0.002, 'base': u'USDT', 'quote': u'GUSD'}
            , {'exchange_rate': 0.00275307, 'trade_fee': 0.002, 'base': u'BNK', 'quote': u'USDT'}
            , {'exchange_rate': 1.566241, 'trade_fee': 0.002, 'base': u'NANO', 'quote': u'USDT'}
            , {'exchange_rate': 0.04, 'trade_fee': 0.002, 'base': u'POA20', 'quote': u'USDT'}
            , {'exchange_rate': 0.00517809, 'trade_fee': 0.002, 'base': u'TDS', 'quote': u'USDT'}
            , {'exchange_rate': 0.0009959, 'trade_fee': 0.002, 'base': u'PLA', 'quote': u'USDT'}
            , {'exchange_rate': 0.334942, 'trade_fee': 0.002, 'base': u'BAT', 'quote': u'USDT'}
            , {'exchange_rate': 0.51426, 'trade_fee': 0.002, 'base': u'CSM', 'quote': u'USDT'}
            , {'exchange_rate': 0.032, 'trade_fee': 0.002, 'base': u'REX', 'quote': u'USDT'}
            , {'exchange_rate': 0.000748, 'trade_fee': 0.002, 'base': u'GST', 'quote': u'USDT'}
            , {'exchange_rate': 0.0969, 'trade_fee': 0.002, 'base': u'MAN', 'quote': u'USDT'}
            , {'exchange_rate': 0.283839, 'trade_fee': 0.002, 'base': u'STORJ', 'quote': u'USDT'}
            , {'exchange_rate': 0.00137288, 'trade_fee': 0.002, 'base': u'BTT', 'quote': u'USDT'}
            , {'exchange_rate': 0.01049, 'trade_fee': 0.002, 'base': u'INK', 'quote': u'USDT'}
            , {'exchange_rate': 0.00068001, 'trade_fee': 0.002, 'base': 'BitClave', 'quote': 'USDT'}
            , {'exchange_rate': 0.000199996, 'trade_fee': 0.002, 'base': u'FREC', 'quote': u'USDT'}
            , {'exchange_rate': 0.0093064, 'trade_fee': 0.002, 'base': u'XVG', 'quote': u'USDT'}
            , {'exchange_rate': 0.1613256, 'trade_fee': 0.002, 'base': u'BTM', 'quote': u'USDT'}
            , {'exchange_rate': 8.3e-05, 'trade_fee': 0.002, 'base': u'BANCA', 'quote': u'USDT'}
            , {'exchange_rate': 10725.7, 'trade_fee': 0.002, 'base': u'BTC', 'quote': u'USDT'}
            , {'exchange_rate': 0.0101564, 'trade_fee': 0.002, 'base': u'IPL', 'quote': u'USDT'}
            , {'exchange_rate': 0.00069, 'trade_fee': 0.002, 'base': u'STU', 'quote': u'USDT'}
            , {'exchange_rate': 1.303595, 'trade_fee': 0.002, 'base': u'BCD', 'quote': u'USDT'}
            , {'exchange_rate': 0.019513, 'trade_fee': 0.002, 'base': u'TIV', 'quote': u'USDT'}
            , {'exchange_rate': 0.846921, 'trade_fee': 0.002, 'base': u'EVX', 'quote': u'USDT'}
            , {'exchange_rate': 1.456233, 'trade_fee': 0.002, 'base': u'KMD', 'quote': u'USDT'}
            , {'exchange_rate': 1.10531, 'trade_fee': 0.002, 'base': u'XMC', 'quote': u'USDT'}
            , {'exchange_rate': 0.0992553, 'trade_fee': 0.002, 'base': u'GNT', 'quote': u'USDT'}
            , {'exchange_rate': 0.148404, 'trade_fee': 0.002, 'base': u'CUTE', 'quote': u'USDT'}
            , {'exchange_rate': 1.623075, 'trade_fee': 0.002, 'base': u'ONT', 'quote': u'USDT'}
            , {'exchange_rate': 0.01218091, 'trade_fee': 0.002, 'base': u'IOST', 'quote': u'USDT'}
            , {'exchange_rate': 0.1033996, 'trade_fee': 0.002, 'base': u'NEXO', 'quote': u'USDT'}
            , {'exchange_rate': 0.044716, 'trade_fee': 0.002, 'base': u'POA', 'quote': u'USDT'}
            , {'exchange_rate': 0.000319994, 'trade_fee': 0.002, 'base': u'QNTU', 'quote': u'USDT'}
            , {'exchange_rate': 0.0378524, 'trade_fee': 0.002, 'base': u'NXT', 'quote': u'USDT'}
            , {'exchange_rate': 0.2102351, 'trade_fee': 0.002, 'base': u'ELF', 'quote': u'USDT'}
            , {'exchange_rate': 0.24822, 'trade_fee': 0.002, 'base': u'EMC', 'quote': u'USDT'}
            , {'exchange_rate': 0.764162, 'trade_fee': 0.002, 'base': u'BTX', 'quote': u'USDT'}
            , {'exchange_rate': 0.9958785, 'trade_fee': 0.002, 'base': u'USDT', 'quote': u'USDC'}
            , {'exchange_rate': 0.0226532, 'trade_fee': 0.002, 'base': u'DATA', 'quote': u'USDT'}
            , {'exchange_rate': 6.146312, 'trade_fee': 0.002, 'base': u'MCO', 'quote': u'USDT'}
            , {'exchange_rate': 0.0549898, 'trade_fee': 0.002, 'base': u'MANA', 'quote': u'USDT'}
            , {'exchange_rate': 0.0011001, 'trade_fee': 0.002, 'base': u'BETR', 'quote': u'USDT'}
            , {'exchange_rate': 9.31873, 'trade_fee': 0.002, 'base': u'ETC', 'quote': u'USDT'}
            , {'exchange_rate': 5.72299, 'trade_fee': 0.002, 'base': u'GRIN', 'quote': u'USDT'}
            , {'exchange_rate': 0.0212309, 'trade_fee': 0.002, 'base': u'NTK', 'quote': u'USDT'}
            , {'exchange_rate': 0.014, 'trade_fee': 0.002, 'base': u'CDT', 'quote': u'USDT'}
            , {'exchange_rate': 0.000459115, 'trade_fee': 0.002, 'base': u'HBZ', 'quote': u'USDT'}
            , {'exchange_rate': 18.4872, 'trade_fee': 0.002, 'base': u'REP', 'quote': u'USDT'}
            , {'exchange_rate': 0.150434, 'trade_fee': 0.002, 'base': u'NEU', 'quote': u'USDT'}
            , {'exchange_rate': 177.2469, 'trade_fee': 0.002, 'base': u'DASH', 'quote': u'USDT'}
            , {'exchange_rate': 0.01018879, 'trade_fee': 0.002, 'base': u'SMART', 'quote': u'USDT'}
            , {'exchange_rate': 0.96909, 'trade_fee': 0.002, 'base': u'STRAT', 'quote': u'USDT'}
            , {'exchange_rate': 0.104578, 'trade_fee': 0.002, 'base': u'BQX', 'quote': u'USDT'}
            , {'exchange_rate': 0.00329115, 'trade_fee': 0.002, 'base': u'FDZ', 'quote': u'USDT'}
            , {'exchange_rate': 0.721594, 'trade_fee': 0.002, 'base': u'SUR', 'quote': u'USDT'}
            , {'exchange_rate': 0.00192731, 'trade_fee': 0.002, 'base': u'NCT', 'quote': u'USDT'}
            , {'exchange_rate': 18.33743, 'trade_fee': 0.002, 'base': u'NEO', 'quote': u'USDT'}
            , {'exchange_rate': 0.016046, 'trade_fee': 0.002, 'base': u'PHX', 'quote': u'USDT'}
            , {'exchange_rate': 0.057017, 'trade_fee': 0.002, 'base': u'CRO', 'quote': u'USDT'}
            , {'exchange_rate': 0.0545848, 'trade_fee': 0.002, 'base': u'BCPT', 'quote': u'USDT'}
            , {'exchange_rate': 0.347157, 'trade_fee': 0.002, 'base': u'UTT', 'quote': u'USDT'}
            , {'exchange_rate': 0.0208063, 'trade_fee': 0.002, 'base': u'ZIL', 'quote': u'USDT'}
            , {'exchange_rate': 2.33664, 'trade_fee': 0.002, 'base': u'OMG', 'quote': u'USDT'}
            , {'exchange_rate': 0.402307, 'trade_fee': 0.002, 'base': u'PPC', 'quote': u'USDT'}
            , {'exchange_rate': 0.001061096, 'trade_fee': 0.002, 'base': u'BCN', 'quote': u'USDT'}
            , {'exchange_rate': 0.025666, 'trade_fee': 0.002, 'base': u'STX', 'quote': u'USDT'}
            , {'exchange_rate': 0.228868, 'trade_fee': 0.002, 'base': u'CRPT', 'quote': u'USDT'}
            , {'exchange_rate': 0.0961511, 'trade_fee': 0.002, 'base': u'ADA', 'quote': u'USDT'}
            , {'exchange_rate': 5.46e-07, 'trade_fee': 0.002, 'base': u'HAND', 'quote': u'USDT'}
            , {'exchange_rate': 0.00058016, 'trade_fee': 0.002, 'base': u'BITS', 'quote': u'USDT'}
            , {'exchange_rate': 0.0343643, 'trade_fee': 0.002, 'base': u'RCN', 'quote': u'USDT'}
            , {'exchange_rate': 0.1382885, 'trade_fee': 0.002, 'base': u'ENJ', 'quote': u'USDT'}
            , {'exchange_rate': 0.000435299, 'trade_fee': 0.002, 'base': u'ACAT', 'quote': u'USDT'}
            , {'exchange_rate': 0.01433352, 'trade_fee': 0.002, 'base': u'DGB', 'quote': u'USDT'}
            , {'exchange_rate': 0.02, 'trade_fee': 0.002, 'base': u'ACT', 'quote': u'USDT'}
            , {'exchange_rate': 0.99065, 'trade_fee': 0.002, 'base': u'USDT', 'quote': u'DAI'}
            , {'exchange_rate': 0.529001, 'trade_fee': 0.002, 'base': u'LOC', 'quote': u'USDT'}
            , {'exchange_rate': 0.208143, 'trade_fee': 0.002, 'base': u'MAID', 'quote': u'USDT'}
            , {'exchange_rate': 0.130726, 'trade_fee': 0.002, 'base': u'XLM', 'quote': u'USDT'}
            , {'exchange_rate': 0.0274789, 'trade_fee': 0.002, 'base': u'SRN', 'quote': u'USDT'}
            , {'exchange_rate': 0.00154935, 'trade_fee': 0.002, 'base': u'ZSC', 'quote': u'USDT'}
            , {'exchange_rate': 0.00961073, 'trade_fee': 0.002, 'base': u'CVT', 'quote': u'USDT'}
            , {'exchange_rate': 3.981656, 'trade_fee': 0.002, 'base': u'QTUM', 'quote': u'USDT'}
            , {'exchange_rate': 0.0092714, 'trade_fee': 0.002, 'base': u'ZAP', 'quote': u'USDT'}
            , {'exchange_rate': 0.0120224, 'trade_fee': 0.002, 'base': u'FLP', 'quote': u'USDT'}
            , {'exchange_rate': 0.001236199, 'trade_fee': 0.002, 'base': u'XDN', 'quote': u'USDT'}
            , {'exchange_rate': 0.00049238, 'trade_fee': 0.002, 'base': u'NOAH', 'quote': u'USDT'}
            , {'exchange_rate': 3.80025, 'trade_fee': 0.002, 'base': u'HT', 'quote': u'USDT'}
            , {'exchange_rate': 0.994744, 'trade_fee': 0.002, 'base': u'USDT', 'quote': u'PAX'}
            , {'exchange_rate': 0.0043093, 'trade_fee': 0.002, 'base': u'CHAT', 'quote': u'USDT'}
            , {'exchange_rate': 0.00555633, 'trade_fee': 0.002, 'base': u'ETN', 'quote': u'USDT'}
            , {'exchange_rate': 0.000133273, 'trade_fee': 0.002, 'base': u'STQ', 'quote': u'USDT'}
            , {'exchange_rate': 28.0579, 'trade_fee': 0.002, 'base': u'DCR', 'quote': u'USDT'}
            , {'exchange_rate': 0.0288763, 'trade_fee': 0.002, 'base': u'PLR', 'quote': u'USDT'}
            , {'exchange_rate': 0.349509, 'trade_fee': 0.002, 'base': u'ICX', 'quote': u'USDT'}
            , {'exchange_rate': 0.0006, 'trade_fee': 0.002, 'base': u'CVH', 'quote': u'USDT'}
            , {'exchange_rate': 0.053, 'trade_fee': 0.002, 'base': u'NGC', 'quote': u'USDT'}
            , {'exchange_rate': 322.856, 'trade_fee': 0.002, 'base': u'PETH', 'quote': u'USDT'}
            , {'exchange_rate': 0.460297, 'trade_fee': 0.002, 'base': u'IOTA', 'quote': u'USDT'}
            , {'exchange_rate': 0.0400007, 'trade_fee': 0.002, 'base': u'UTK', 'quote': u'USDT'}
            , {'exchange_rate': 1.029999, 'trade_fee': 0.002, 'base': u'SBD', 'quote': u'USDT'}
            , {'exchange_rate': 0.00563172, 'trade_fee': 0.002, 'base': u'FUN', 'quote': u'USDT'}
            , {'exchange_rate': 0.00106326, 'trade_fee': 0.002, 'base': u'MESH', 'quote': u'USDT'}
            , {'exchange_rate': 113.5202, 'trade_fee': 0.002, 'base': u'ZEC', 'quote': u'USDT'}
            , {'exchange_rate': 0.0136196, 'trade_fee': 0.002, 'base': u'MESSE', 'quote': u'USDT'}
            , {'exchange_rate': 0.00278001, 'trade_fee': 0.002, 'base': u'ELEC', 'quote': u'USDT'}
            , {'exchange_rate': 0.25, 'trade_fee': 0.002, 'base': u'TV', 'quote': u'USDT'}
            , {'exchange_rate': 0.000563026, 'trade_fee': 0.002, 'base': u'PMA', 'quote': u'USDT'}
            , {'exchange_rate': 0.00057, 'trade_fee': 0.002, 'base': u'CCL', 'quote': u'USDT'}
            , {'exchange_rate': 0.0027642, 'trade_fee': 0.002, 'base': u'SPF', 'quote': u'USDT'}
            , {'exchange_rate': 0.00097007, 'trade_fee': 0.002, 'base': u'BRDG', 'quote': u'USDT'}
            , {'exchange_rate': 0.0123, 'trade_fee': 0.002, 'base': u'ZPT', 'quote': u'USDT'}
            , {'exchange_rate': 7.55e-05, 'trade_fee': 0.002, 'base': u'HTML', 'quote': u'USDT'}
            , {'exchange_rate': 0.0386501, 'trade_fee': 0.002, 'base': u'SNC', 'quote': u'USDT'}
            , {'exchange_rate': 0.0004, 'trade_fee': 0.002, 'base': u'KRM', 'quote': u'USDT'}
            , {'exchange_rate': 0.00611999, 'trade_fee': 0.002, 'base': u'SPC', 'quote': u'USDT'}
            , {'exchange_rate': 1.121151, 'trade_fee': 0.002, 'base': u'EDO', 'quote': u'USDT'}
            , {'exchange_rate': 3.75082, 'trade_fee': 0.002, 'base': u'B2G', 'quote': u'USDT'}
            , {'exchange_rate': 0.00328, 'trade_fee': 0.002, 'base': u'DOGE', 'quote': u'USDT'}
            , {'exchange_rate': 0.124948, 'trade_fee': 0.002, 'base': u'SWM', 'quote': u'USDT'}
            , {'exchange_rate': 0.0455, 'trade_fee': 0.002, 'base': u'VIB', 'quote': u'USDT'}
            , {'exchange_rate': 0.0940418, 'trade_fee': 0.002, 'base': u'WAX', 'quote': u'USDT'}
            , {'exchange_rate': 0.0891, 'trade_fee': 0.002, 'base': u'GBX', 'quote': u'USDT'}
            , {'exchange_rate': 10.76641, 'trade_fee': 0.002, 'base': u'ZEN', 'quote': u'USDT'}
            , {'exchange_rate': 0.0649988, 'trade_fee': 0.002, 'base': u'MTX', 'quote': u'USDT'}
            , {'exchange_rate': 0.00608261, 'trade_fee': 0.002, 'base': u'SMT', 'quote': u'USDT'}
            , {'exchange_rate': 0.01571043, 'trade_fee': 0.002, 'base': u'CND', 'quote': u'USDT'}
            , {'exchange_rate': 0.2, 'trade_fee': 0.002, 'base': u'CVCOIN', 'quote': u'USDT'}
            , {'exchange_rate': 0.288793, 'trade_fee': 0.002, 'base': u'BMC', 'quote': u'USDT'}
            , {'exchange_rate': 313.655, 'trade_fee': 0.002, 'base': u'ETH', 'quote': u'USDT'}
            , {'exchange_rate': 0.0500853, 'trade_fee': 0.002, 'base': u'DGTX', 'quote': u'USDT'}
            , {'exchange_rate': 1.00479, 'trade_fee': 0.002, 'base': u'USDT', 'quote': u'EOSDT'}
            , {'exchange_rate': 2.50002, 'trade_fee': 0.002, 'base': u'SBTC', 'quote': u'USDT'}
            , {'exchange_rate': 0.0039018, 'trade_fee': 0.002, 'base': u'ECOIN', 'quote': u'USDT'}
            , {'exchange_rate': 0.0872834, 'trade_fee': 0.002, 'base': u'CVC', 'quote': u'USDT'}
            , {'exchange_rate': 2.17776, 'trade_fee': 0.002, 'base': u'LSK', 'quote': u'USDT'}
            , {'exchange_rate': 0.000310026, 'trade_fee': 0.002, 'base': u'SVD', 'quote': u'USDT'}
            , {'exchange_rate': 0.1467979, 'trade_fee': 0.002, 'base': u'ADX', 'quote': u'USDT'}
            , {'exchange_rate': 0.0392771, 'trade_fee': 0.002, 'base': u'AMB', 'quote': u'USDT'}
            , {'exchange_rate': 0.588617, 'trade_fee': 0.002, 'base': u'MNX', 'quote': u'USDT'}
            , {'exchange_rate': 1.237308, 'trade_fee': 0.002, 'base': u'XTZ', 'quote': u'USDT'}
            , {'exchange_rate': 0.00998113, 'trade_fee': 0.002, 'base': u'ABA', 'quote': u'USDT'}
            , {'exchange_rate': 0.0415008, 'trade_fee': 0.002, 'base': u'TNT', 'quote': u'USDT'}
            , {'exchange_rate': 0.00160312, 'trade_fee': 0.002, 'base': u'CMCT', 'quote': u'USDT'}
            , {'exchange_rate': 0.0599989, 'trade_fee': 0.002, 'base': u'DADI', 'quote': u'USDT'}
            , {'exchange_rate': 0.0256385, 'trade_fee': 0.002, 'base': u'SUB', 'quote': u'USDT'}
            , {'exchange_rate': 757.824, 'trade_fee': 0.002, 'base': u'MKR', 'quote': u'USDT'}
            , {'exchange_rate': 0.796014, 'trade_fee': 0.002, 'base': u'BNT', 'quote': u'USDT'}
            , {'exchange_rate': 24.07553, 'trade_fee': 0.002, 'base': u'NUT', 'quote': u'USDT'}
            , {'exchange_rate': 0.089563, 'trade_fee': 0.002, 'base': u'APPC', 'quote': u'USDT'}
            , {'exchange_rate': 3.5, 'trade_fee': 0.002, 'base': u'GAS', 'quote': u'USDT'}
            , {'exchange_rate': 0.00053, 'trade_fee': 0.002, 'base': u'DIM', 'quote': u'USDT'}
            , {'exchange_rate': 742.79162, 'trade_fee': 0.002, 'base': 'Bitcoin Cash', 'quote': 'USDT'}
            , {'exchange_rate': 1.194074, 'trade_fee': 0.002, 'base': u'WIKI', 'quote': u'USDT'}
            , {'exchange_rate': 4.2e-05, 'trade_fee': 0.002, 'base': u'DCN', 'quote': u'USDT'}
            , {'exchange_rate': 30.56861, 'trade_fee': 0.002, 'base': u'BTG', 'quote': u'USDT'}
            , {'exchange_rate': 0.00099, 'trade_fee': 0.002, 'base': u'PXG', 'quote': u'USDT'}
            , {'exchange_rate': 0.0008141, 'trade_fee': 0.002, 'base': u'BERRY', 'quote': u'USDT'}
            , {'exchange_rate': 0.214894, 'trade_fee': 0.002, 'base': u'OAX', 'quote': u'USDT'}
            , {'exchange_rate': 0.0062835, 'trade_fee': 0.002, 'base': u'AMM', 'quote': u'USDT'}
            , {'exchange_rate': 0.269617, 'trade_fee': 0.002, 'base': u'KNC', 'quote': u'USDT'}
            , {'exchange_rate': 0.351716, 'trade_fee': 0.002, 'base': u'ZRX', 'quote': u'USDT'}
            , {'exchange_rate': 0.13, 'trade_fee': 0.002, 'base': u'ARDR', 'quote': u'USDT'}
            ]
        self.reference_asset = \
            'USDT'
        self.portfolio_frac_tolerance = \
            0.001
        self.error_tolerance = \
            1e-05

#        abs_diff_flow = \
#            abs( flow_vals - [ 0.0
#                             , 0.0
#                             , 0.0
#                             , 6.59478512e-3
#                             , 2.33432942e-1
#                             , 2.866e-5
#                             , 0.0
#                             , 5.49598219
#                             , 0.0
#                             , 0.0
#                             ]
#               )


################################################################################
class Case_02( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            {'LTC': 4.626, 'ETC': 90.03999999999999, 'EOS': 104.0, 'TRX': 22887.000000000007, 'TUSD': 0.00659478512, 'ETH': 2.866e-05, 'USDT': 212.63370005605503}
        self.portfolio_frac_target = \
            {'EOS': 0.2, 'TRX': 0.2, 'ETC': 0.2, 'LTC': 0.2, 'ZEC': 0.2}
        self.asset_pairs = \
            [ {'exchange_rate': 211.709, 'trade_fee': 0.002, 'base': 'ETH', 'quote': 'USDT'}
            , {'exchange_rate': 88.6464, 'trade_fee': 0.002, 'base': 'LTC', 'quote': 'USDT'}
            , {'exchange_rate': 72.9882, 'trade_fee': 0.002, 'base': 'ZEC', 'quote': 'USDT'}
            , {'exchange_rate': 3.96505, 'trade_fee': 0.002, 'base': 'EOS', 'quote': 'USDT'}
            , {'exchange_rate': 1.000692, 'trade_fee': 0.002, 'base': 'USDT', 'quote': 'TUSD'}
            , {'exchange_rate': 5.68243, 'trade_fee': 0.002, 'base': 'ETC', 'quote': 'USDT'}
            , {'exchange_rate': 0.0226109, 'trade_fee': 0.002, 'base': 'TRX', 'quote': 'USDT'}
            ]
        self.reference_asset = \
            'USDT'
        self.portfolio_frac_tolerance = \
            0.001
        self.error_tolerance = \
            1e-05

#        abs_diff = \
#            abs( flow_vals - [ 0.574160239
#                             , 1.97846135
#                             , 412.942834768
#                             , 0.429043743
#                             , 0.558689712
#                             , 0.301621114
#                             , 1.00342872
#                             , 0.00273563992
#                             , 2.29206457e-06
#                             , 3.54463916e-03
#                             , 0.178504335
#                             , 0.552818781
#                             , 17.5056154
#                             , 4598.729677946102
#                             ]
#               )


################################################################################
class Case_03( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            {'EOS': 93.08000000000003, 'DASH': 3.4650000000000003, 'LTC': 4.7139999999999995, 'ETC': 64.63, 'TUSD': 0.00659478512, 'ETH': 2.866000000034674e-05, 'USDT': 292.95747147146034}
        self.portfolio_frac_target = \
            {'EOS': 0.25, 'DASH': 0.25, 'LTC': 0.25, 'ETC': 0.25}
        self.asset_pairs = \
            [ {'exchange_rate': 225.012, 'trade_fee': 0.002, 'base': 'ETH', 'quote': 'USDT'}
            , {'exchange_rate': 101.5073, 'trade_fee': 0.002, 'base': 'LTC', 'quote': 'USDT'}
            , {'exchange_rate': 5.95462, 'trade_fee': 0.002, 'base': 'ETC', 'quote': 'USDT'}
            , {'exchange_rate': 1.001943, 'trade_fee': 0.002, 'base': 'USDT', 'quote': 'TUSD'}
            , {'exchange_rate': 110.7939, 'trade_fee': 0.002, 'base': 'DASH', 'quote': 'USDT'}
            , {'exchange_rate': 4.11935, 'trade_fee': 0.002, 'base': 'EOS', 'quote': 'USDT'}
            ]
        self.reference_asset = \
            'USDT'
        self.portfolio_frac_tolerance = \
            0.001
        self.error_tolerance = \
            0.001


################################################################################
class Case_04( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            {'reference': 0.0, 'EOS': 0.0, 'BCH': 0.0, 'ETC': 0.0, 'NEO': 0.0, 'LTC': 0.0, 'BTC': 0.0, 'ZEC': 0.0, 'TRX': 0.0, 'ADA': 0.0, 'BSV': 0.0, 'ETH': 0.0, 'XLM': 0.0, 'ONT': 0.0, 'QTUM': 0.0, 'XRP': 1.8554509111301472e-05, 'ZRX': 0.0, 'XTZ': 0.0, 'XMR': 0.0, 'ICX': 0.0, 'OMG': 0.0, 'XEM': 0.0, 'DASH': 0.0}
        self.portfolio_frac_target = \
            {'ADA': 1.0}
        self.asset_pairs = \
            [ {'quote': 'reference', 'base': 'EOS', 'exchange_rate': 0.721574670438813, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BCH', 'exchange_rate': 0.7649431937723543, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ETC', 'exchange_rate': 0.6305075768104237, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'NEO', 'exchange_rate': 1.111150225895268, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'LTC', 'exchange_rate': 0.8204851092754362, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BTC', 'exchange_rate': 1.2044566637560583, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ZEC', 'exchange_rate': 1.2532369480310963, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'TRX', 'exchange_rate': 1.0477953424392548, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ADA', 'exchange_rate': 2.5813177912644907, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BSV', 'exchange_rate': 0.7892356170865569, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ETH', 'exchange_rate': 2.127800688043047, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XLM', 'exchange_rate': 1.7122902759505592, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ONT', 'exchange_rate': 0.9285459752844785, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'QTUM', 'exchange_rate': 1.189902311563757, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XRP', 'exchange_rate': 1.2573315133650431, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ZRX', 'exchange_rate': 1.670700703489587, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XTZ', 'exchange_rate': 1.979673012620481, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XMR', 'exchange_rate': 1.193610973869888, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ICX', 'exchange_rate': 1.4199150485893477, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'OMG', 'exchange_rate': 1.7430913680540345, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XEM', 'exchange_rate': 1.1590997291803458, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'DASH', 'exchange_rate': 0.7495939540730823, 'trade_fee': 0.001}
            ]
        self.reference_asset = \
            'reference'
        self.portfolio_frac_tolerance = \
            0.0001
        self.error_tolerance = \
            0.001


################################################################################
class Case_05( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            {'reference': 2.2559010174283513e-05, 'EOS': 0.0, 'BCH': 0.0, 'ETC': 0.0, 'NEO': 0.0, 'LTC': 0.0, 'BTC': 0.0, 'ZEC': 0.0, 'TRX': 0.0, 'ADA': 0.0, 'BSV': 0.0, 'ETH': 0.0, 'XLM': 0.0, 'ONT': 0.0, 'QTUM': 0.0, 'XRP': 0.0, 'ZRX': 0.0, 'XTZ': 0.0, 'XMR': 0.0, 'ICX': 0.0, 'OMG': 0.0, 'XEM': 0.0, 'DASH': 0.0}
        self.portfolio_frac_target = \
            {'ADA': 1.0}
        self.asset_pairs = \
            [ {'quote': 'reference', 'base': 'EOS', 'exchange_rate': 0.609845301872028, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BCH', 'exchange_rate': 0.6021118241110878, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ETC', 'exchange_rate': 0.5444902161818679, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'NEO', 'exchange_rate': 0.9307771772623887, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'LTC', 'exchange_rate': 0.6174158126227386, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BTC', 'exchange_rate': 0.9872348002943506, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ZEC', 'exchange_rate': 0.8959098478794983, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'TRX', 'exchange_rate': 0.930421372766058, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ADA', 'exchange_rate': 2.3231305847311488, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BSV', 'exchange_rate': 0.6434011842587576, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ETH', 'exchange_rate': 1.3166045516693865, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XLM', 'exchange_rate': 1.4343906836232516, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ONT', 'exchange_rate': 0.8518318578658064, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'QTUM', 'exchange_rate': 1.006437712487195, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XRP', 'exchange_rate': 0.825115562403698, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ZRX', 'exchange_rate': 1.6996282301316432, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XTZ', 'exchange_rate': 1.801365743149064, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XMR', 'exchange_rate': 0.9414959169965083, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ICX', 'exchange_rate': 1.5263864800093305, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'OMG', 'exchange_rate': 1.6726079463371144, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XEM', 'exchange_rate': 0.9612503435067267, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'DASH', 'exchange_rate': 0.6116417352313449, 'trade_fee': 0.001}
            ]
        self.reference_asset = \
            'reference'
        self.portfolio_frac_tolerance = \
            0.0001
        self.error_tolerance = \
            0.001


################################################################################
class Case_06( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            {'reference': 0.0, 'EOS': 0.0, 'BCH': 0.0, 'ETC': 0.0, 'NEO': 0.0, 'LTC': 0.0, 'BTC': 0.0, 'ZEC': 0.0, 'TRX': 0.0, 'ADA': 0.0, 'BSV': 0.0, 'ETH': 1.532472038847835e-05, 'XLM': 0.0, 'ONT': 0.0, 'QTUM': 0.0, 'XRP': 0.0, 'ZRX': 0.0, 'XTZ': 0.0, 'XMR': 0.0, 'ICX': 0.0, 'OMG': 0.0, 'XEM': 0.0, 'DASH': 0.0}
        self.portfolio_frac_target = \
            {'ADA': 1.0}
        self.asset_pairs = \
            [ {'quote': 'reference', 'base': 'EOS', 'exchange_rate': 0.6584433877084211, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BCH', 'exchange_rate': 0.6621844098464128, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ETC', 'exchange_rate': 0.5857179942364321, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'NEO', 'exchange_rate': 1.029955619006238, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'LTC', 'exchange_rate': 0.7018371128361842, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BTC', 'exchange_rate': 1.0397652061418847, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ZEC', 'exchange_rate': 0.9724764202020804, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'TRX', 'exchange_rate': 1.0085865044651972, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ADA', 'exchange_rate': 2.600394523697549, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'BSV', 'exchange_rate': 0.6786377829021699, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ETH', 'exchange_rate': 1.6710790367397348, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XLM', 'exchange_rate': 1.6328509477171949, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ONT', 'exchange_rate': 0.9488736540624776, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'QTUM', 'exchange_rate': 1.118781269949671, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XRP', 'exchange_rate': 0.9010434112681717, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ZRX', 'exchange_rate': 1.7090443686006827, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XTZ', 'exchange_rate': 2.0976366483828053, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XMR', 'exchange_rate': 1.0236708727246486, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'ICX', 'exchange_rate': 1.4964917099010908, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'OMG', 'exchange_rate': 1.813390058118569, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'XEM', 'exchange_rate': 1.064287677181294, 'trade_fee': 0.001}
            , {'quote': 'reference', 'base': 'DASH', 'exchange_rate': 0.6436480630855355, 'trade_fee': 0.001}
            ]
        self.reference_asset = \
            'reference'
        self.portfolio_frac_tolerance = \
            0.0001
        self.error_tolerance = \
            0.001


################################################################################
class Case_07( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            { 'TUSD': 0.30078959835998376
            , 'EURS': 0.2686923475999947
            , 'ETH' : 0.8003705160147474
            , 'DAI' : 0.806861168599994
            , 'USDT': 45.26826929692684
            , 'BTC' : 0.17017
            , 'ZIL' : 152270.2
            , 'EOS' : 0.4357721643663979
            , 'LTC' : 0.7041984682587197
            }
        self.portfolio_frac_target = \
            { 'ZIL' : 0.666667
            , 'USDT': 0.000798
            , 'ETH' : 0.135277
            , 'BTC' : 0.104771
            , 'EOS' : 0.02434
            , 'LTC' : 0.068148
            }
        self.asset_pairs = \
            [ {'quote': 'USDT', 'base': 'BTC' , 'exchange_rate': 23155.87 , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'LTC' , 'exchange_rate': 103.2333 , 'trade_fee': 0.002}
            , {'quote': 'DAI' , 'base': 'USDT', 'exchange_rate': 0.99487  , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'ETH' , 'exchange_rate': 579.627  , 'trade_fee': 0.002}
            , {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.000225 , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'ZIL' , 'exchange_rate': 0.0550011, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'EOS' , 'exchange_rate': 2.46945  , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.20849  , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'XLM' , 'exchange_rate': 0.156926 , 'trade_fee': 0.002}
            ]
        self.reference_asset = \
            'USDT'
        self.portfolio_frac_tolerance = \
            0.001
        self.error_tolerance = \
            1e-05


################################################################################
class Case_08( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            { 'DAI' : 0.806861168599994
            , 'TUSD': 0.30078959835998376
            , 'USDT': 501.456217688994
            , 'ETH' : 0.8003705160147465
            , 'ZIL' : 136102.19999999987
            , 'EOS' : 0.4357721643664263
            , 'LTC' : 0.7041984682587206
            , 'BTC' : 0.17017
            , 'EURS': 0.2686923475999947
            }
        self.portfolio_frac_target = \
            { 'BTC': 0.333333
            , 'ETH': 0.13919
            , 'ZIL': 0.527477
            }
        self.asset_pairs = \
            [ {'quote': 'USDT', 'base': 'BTC' , 'exchange_rate': 29115.53 , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'LTC' , 'exchange_rate': 127.0152 , 'trade_fee': 0.002}
            , {'quote': 'DAI' , 'base': 'USDT', 'exchange_rate': 0.99632  , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'ETH' , 'exchange_rate': 743.453  , 'trade_fee': 0.002}
            , {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 0.999252 , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'ZIL' , 'exchange_rate': 0.0740195, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'EOS' , 'exchange_rate': 2.61677  , 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.21076  , 'trade_fee': 0.002}
            ]
        self.reference_asset = \
            'USDT'
        self.portfolio_frac_tolerance = \
            0.001
        self.error_tolerance = \
            1e-06


################################################################################
class Case_09( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            { 'NANO': 0.9772372209554305
            , 'ZEC': 48.88730312730511
            , 'ETH': 0.8003705160147474
            , 'BTC': 0.17017
            , 'DAI': 0.806861168599994
            , 'LTC': 0.7041984682587206
            , 'EURS': 0.2686923475999947
            , 'IOST': 318519.4179656985
            , 'ZIL': 0.0999999999839929
            , 'TUSD': 0.30078959835998376
            , 'USDT': 45.10323949704498
            , 'EOS': 0.4357721643664263
            }
        self.portfolio_frac_target = \
            { 'NANO': 0.333333
            , 'IOST': 0.333333
            , 'ZEC': 0.333333
            }
        self.asset_pairs = \
            [ {'quote': 'USDT', 'base': 'ZEC', 'exchange_rate': 103.0043, 'trade_fee': 0.002}
            , {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 0.9973, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 138.5995, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'IOST', 'exchange_rate': 0.01131076, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 1067.059, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'ZIL', 'exchange_rate': 0.0632662, 'trade_fee': 0.002}
            , {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.003679, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 2.70299, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.20442, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'NANO', 'exchange_rate': 3.086139, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 34688.64, 'trade_fee': 0.002}
            ]
        self.reference_asset = \
            'USDT'
        self.portfolio_frac_tolerance = \
            0.001
        self.error_tolerance = \
            9.999999999999999e-05 / 10


################################################################################
class Case_10( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            { 'NANO': 0.9772372209554305
            , 'ZEC': 48.88730312730511
            , 'ETH': 0.8003705160147474
            , 'BTC': 0.17017
            , 'DAI': 0.806861168599994
            , 'LTC': 0.7041984682587206
            , 'EURS': 0.2686923475999947
            , 'IOST': 318519.4179656985
            , 'ZIL': 0.0999999999839929
            , 'TUSD': 0.30078959835998376
            , 'USDT': 45.10323949704498
            , 'EOS': 0.4357721643664263
            }
        self.portfolio_frac_target = \
            { 'NANO': 0.333333
            , 'IOST': 0.333333
            , 'ZEC': 0.333333
            }
        self.asset_pairs = \
            [ {'quote': 'USDT', 'base': 'ZEC', 'exchange_rate': 103.0043, 'trade_fee': 0.002}
            , {'quote': 'DAI', 'base': 'USDT', 'exchange_rate': 0.9973, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'LTC', 'exchange_rate': 138.5995, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'IOST', 'exchange_rate': 0.01131076, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 1067.059, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'ZIL', 'exchange_rate': 0.0632662, 'trade_fee': 0.002}
            , {'quote': 'TUSD', 'base': 'USDT', 'exchange_rate': 1.003679, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'EOS', 'exchange_rate': 2.70299, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'EURS', 'exchange_rate': 1.20442, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'NANO', 'exchange_rate': 3.086139, 'trade_fee': 0.002}
            , {'quote': 'USDT', 'base': 'BTC', 'exchange_rate': 34688.64, 'trade_fee': 0.002}
            ]
        self.reference_asset = \
            'USDT'
        self.portfolio_frac_tolerance = \
            0.001
        self.error_tolerance = \
            0.001 /100


################################################################################
class Case_11( Rebalance_Portfolio_Base
             , unittest.TestCase
             ):
    ####################################
    def setUp(self):
        self.portfolio = \
            { 'ETH': 100
            }
        self.portfolio_frac_target = \
            { 'USDT': 1.0
            }
        self.asset_pairs = \
            [ {'quote': 'USDT', 'base': 'ETH', 'exchange_rate': 1067.059, 'trade_fee': 0.002}
            ]
        self.reference_asset = \
            'USDT'
        self.portfolio_frac_tolerance = \
            0.001
        self.error_tolerance = \
            0.001 /100


################################################################################
if __name__ == '__main__':
    unittest.main()
