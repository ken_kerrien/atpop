################################################################################
def Extract_Par( par_defs
               , pars
               , par_name
               ):
    return pars[ list( par_defs.keys() ).index(par_name) ]


################################################################################
def Convert_Pars_To_Par_Dict( par_defs
                            , pars
                            ):
    return dict( zip( par_defs.keys()
                    , pars
                    )
               )


################################################################################
def Convert_Par_Dict_To_Pars( par_defs
                            , par_dict
                            ):
    return [ par_dict[k] for k in par_defs.keys() ]


################################################################################
def Convert_Par_Dict_Strategies_To_Pars_Strategies( par_defs
                                                  , par_dict_strategies
                                                  ):
    return \
        { strategy: Convert_Par_Dict_To_Pars( par_defs, par_dict )
          for (strategy, par_dict) in par_dict_strategies.items()
        }


################################################################################
def Convert_Pars_Strategies_To_Par_Dict_Strategies( par_defs
                                                  , pars_strategies
                                                  ):
    return \
        { strategy: Convert_Pars_To_Par_Dict( par_defs, pars )
          for (strategy, pars) in pars_strategies.items()
        }
