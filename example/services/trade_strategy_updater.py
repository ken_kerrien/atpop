from os import path, makedirs
ARTIFACTS_PATH = path.abspath( path.join( path.dirname( __file__ )
                                        , 'artifacts'
                                        )
                             )
STATE_ARTIFACTS_PATH = path.join( ARTIFACTS_PATH, 'state' )
LOG_ARTIFACTS_PATH   = path.join( ARTIFACTS_PATH,   'log' )
DEBUG_ARTIFACTS_PATH = path.join( ARTIFACTS_PATH, 'debug' )

# Setup logging
import logging
makedirs( path.abspath(LOG_ARTIFACTS_PATH)
        , exist_ok = True
        )
log_format_str = '%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(funcName)s - %(lineno)d - %(message)s'
logging.basicConfig( filename = path.join( LOG_ARTIFACTS_PATH
                                         , path.basename( __file__ ) + '.log'
                                         )
                   , level    = logging.INFO
                   , format   = log_format_str
                   )

logger = logging.getLogger(__name__)
error_log_file_handler = logging.FileHandler( path.join( LOG_ARTIFACTS_PATH
                                                       , path.basename( __file__ ) + '_ERROR.log'
                                                       )
                                            )
error_log_file_handler.setLevel( logging.ERROR )
error_log_file_handler.setFormatter( logging.Formatter( log_format_str ) )
logger.addHandler( error_log_file_handler )

import sys
sys.path.append( path.dirname( path.dirname( __file__ ) ) )

########################################
from atpop.service.trade_strategy_updater import Trade_Strategy_Updater

from config                               import Asset_Price_Setup ,\
                                                 Opt_Setup         ,\
                                                 Param_Opter


################################################################################
if __name__ == '__main__':
    t = Trade_Strategy_Updater( Asset_Price_Setup
                              , Opt_Setup
                              , STATE_ARTIFACTS_PATH
                              , DEBUG_ARTIFACTS_PATH
                              , Param_Opter
                              )

    t.Wait_For_Event()
