# Setup logging
from ..log_func import logger


########################################
import numpy as np
import portalocker
import os
import time
import random


################################################################################
class Data():
    ####################################
    def __init__( self
                , setup
                , path = '.'
                ):
        self.setup = setup
        self.path  = path


    ####################################
    def Is_Empty( self ):
        return self.sd.size == 0


    ####################################
    def Get_CSV_File_Name( self ):
        return self.setup.ID + ".csv"


    ####################################
    def Get_CSV_File_Path( self ):
        return os.path.join( self.path
                           , self.Get_CSV_File_Name()
                           )


    ####################################
    def Read_CSV( self
                , file_name = None
                ):
        file_path = file_name
        if file_name is None:
            file_path = self.Get_CSV_File_Path()

        while True:
            try:
                logger.debug( "Trying to lock data file %s"
                            , file_path
                            )
                with portalocker.Lock( file_path
                                     , mode    = 'r'
                                     , timeout = 10
                                     ) as _fh:
                    logger.debug( "Locked data file %s, reading"
                                , file_path
                                )
                    self.sd = np.genfromtxt( file_path
                                           , delimiter      = ','
                                           , missing_values = np.nan
                                           , names          = True
                                           , encoding       = None
                                           )
                    logger.debug( "Read data file %s finished"
                                , file_path
                                )
                logger.debug( "Unlocked data file %s"
                            , file_path
                            )
                break
            except portalocker.exceptions.LockException:
                logger.debug( "Data file %s already locked, retrying later"
                            , file_path
                            )
                time.sleep( random.random() )


    ####################################
    def To_CSV( self
              , file_name = None
              ):
        os.makedirs( os.path.abspath(self.path)
                   , exist_ok = True
                   )

        file_path = file_name
        if file_name is None:
            file_path = self.Get_CSV_File_Path()

        while True:
            try:
                logger.debug( "Trying to lock data file %s"
                            , file_path
                            )
                with portalocker.Lock( file_path
                                     , mode    = 'w'
                                     , timeout = 10
#                                     , newline = ''
                                     ) as fh:
                    logger.debug( "Locked data file %s, writing"
                                , file_path
                                )

                    fh.write( ','.join( self.sd.dtype.names ) )
                    fh.write( '\n' )
                    for row in self.sd:
                        fh.write( ','.join( [ '' if np.isnan(num) else ('%.15f'%num).rstrip('0').rstrip('.')
                                              for num in row
                                            ]
                                          )
                                )
                        fh.write( '\n' )

                    # flush and sync to filesystem
                    logger.debug( "Writing data file %s finished, flushing started"
                                , file_path
                                )
                    fh.flush()
                    logger.debug( "Flushing data file %s finished, fsync started"
                                , file_path
                                )
                    os.fsync( fh.fileno() )
                    logger.debug( "Fsync data file %s finished"
                                , file_path
                                )
                logger.debug( "Unlocked data file %s"
                            , file_path
                            )
                break
            except portalocker.exceptions.LockException:
                logger.debug( "Data file %s already locked, retrying later"
                            , file_path
                            )
                time.sleep( random.random() )
