from .setup                 import Setup
from ..par_defs             import Extract_Par,\
                                   Convert_Pars_Strategies_To_Par_Dict_Strategies
from ..name_mangling        import Add_Suffix_To_Name
from .compute               import Generate_Signals                   ,\
                                   Calculate_Portfolio_Frac_Precursor

from atpop.backtest.compute import Calculate_Backtest
from atpop.backtest.plot    import Plot_Portfolio_Value_Backtest_And_Par_Dict_Strategies_Series

from ..Filter_plot          import Plot_Backtest_Filter,\
                                   Shift_Trade_Signal
from ..Filter_compute       import Calculate_Utility_Over_Series    ,\
                                   Calculate_Score2
import numpy as np

# Exponential
# Moving
# Average


################################################################################
def Calculate_Portfolio_Frac_Precursor_Series( strategy
                                             , pars # [N_fast, N_slow]
                                             , assets_price_record # {asset1: [p1, p2, ...], asset2: [], ...}
                                             , up_to_but_not_including_idx = None
                                             , asset                       = "BTC_OR_WHATEVER"
                                             , reference_asset             = "LIKELY_USD_BUT_DOES_NOT_REALLY_MATTER"
                                             , setup                       = Setup
                                             ):
    assert strategy.startswith( setup.strategy )
    assert asset in assets_price_record
    N_fast = Extract_Par( setup.par_defs, pars, "N_fast" )
    N_slow = Extract_Par( setup.par_defs, pars, "N_slow" )

    skip_first_samples = 3
    scaling_factor     = 1e2

    fast, slow, diff_rel = Generate_Signals( assets_price_record[asset]
                                           , N_fast
                                           , N_slow
                                           , skip_first_samples
                                           , scaling_factor
                                           )
    trade_idxs                      = []
    portfolio_frac_precursor_series = []
    for ii, dr in enumerate( diff_rel[skip_first_samples:], skip_first_samples ):
        portfolio_frac_precursor = \
            Calculate_Portfolio_Frac_Precursor( dr
                                              , asset
                                              , reference_asset
                                              )
        if portfolio_frac_precursor_series \
           and                       \
           portfolio_frac_precursor_series[-1] == portfolio_frac_precursor:
            continue

        trade_idxs.append( ii )
        portfolio_frac_precursor_series.append( portfolio_frac_precursor )

    return trade_idxs, portfolio_frac_precursor_series, ( fast, slow, diff_rel )


################################################################################
def Calculate_Backtest_With_Util_Signals( assets_price_time
                                        , assets_price_record
                                        , pars
                                        , asset
                                        , reference_asset
                                        , trade_fee
                                        , half_life
                                        , portfolio_frac_tolerance = 0.03  # %3 percent
                                        , initial_value            = 1.
                                        , setup                    = Setup
                                        ):
    prices = assets_price_record[asset]
    if assets_price_time is None:
        assets_price_time = list( range( len(prices) ) )


    ####################################
    # Wrapper function
    fast, slow, diff_rel = [],[],[]
    portfolio_frac_precursor_series = []

    def f( strategy
         , pars # [N_fast, N_slow]
         , assets_price_record # {asset1: [p1, p2, ...], asset2: [], ...}
         , up_to_but_not_including_idx = None
         ):
        nonlocal fast, slow, diff_rel
        nonlocal portfolio_frac_precursor_series

        trade_idxs,\
        portfolio_frac_precursor_series,\
        ( _fast, _slow, _diff_rel ) = \
            Calculate_Portfolio_Frac_Precursor_Series( strategy
                                                     , pars
                                                     , assets_price_record
                                                     , up_to_but_not_including_idx
                                                     , asset
                                                     , reference_asset
                                                     , setup
                                                     )
        return trade_idxs, portfolio_frac_precursor_series


    ####################################
    # Actually calculate the portfolio value
    pars_strategies_time   = [ assets_price_time[0] - 1 ]
    pars_strategies_series = [ { Add_Suffix_To_Name( setup.strategy, asset ) : pars } ]

    portfolio_val, trade_idx_series, _initial_price_idx =\
        Calculate_Backtest( assets_price_time
                          , assets_price_record
                          , pars_strategies_time
                          , pars_strategies_series
                          , f
                          , reference_asset
                          , trade_fee
                          , portfolio_frac_tolerance
                          , initial_value
                          )

    total_portfolio_val = sum( map( np.asarray, portfolio_val.values() ) )


    ####################################
    # Calculate utility signal
    util_scaled = \
        Calculate_Utility_Over_Series( prices
                                     , trade_idx_series
                                     , portfolio_frac_precursor_series
                                     , asset
                                     , reference_asset
                                     , trade_fee = trade_fee
                                     , half_life = half_life
                                     )
    return total_portfolio_val,\
           trade_idx_series,\
           portfolio_frac_precursor_series,\
           fast, slow, diff_rel,\
           util_scaled


################################################################################
def Calculate_And_Plot_Backtest_With_Util_Signals( assets_price_time
                                                 , assets_price_record
                                                 , pars
                                                 , asset
                                                 , reference_asset
                                                 , trade_fee
                                                 , half_life
                                                 , portfolio_frac_tolerance = 0.03  # %3 percent
                                                 , initial_value            = 1.
                                                 , disp_hist                = 0
                                                 , reference_util_scaled    = None
                                                 , title                    = None
                                                 , setup                    = Setup
                                                 ):
    prices = assets_price_record[asset]

    total_portfolio_val,\
    trade_idx_series,\
    portfolio_frac_precursor_series,\
    fast, slow, diff_rel,\
    util_scaled =\
        Calculate_Backtest_With_Util_Signals( assets_price_time
                                            , assets_price_record
                                            , pars
                                            , asset
                                            , reference_asset
                                            , trade_fee
                                            , half_life
                                            , portfolio_frac_tolerance
                                            , initial_value
                                            , setup
                                            )


    ####################################
    # Prepare for plotting
    if disp_hist == 0:
        disp_hist = len(assets_price_time)
    hist = min( disp_hist, len(assets_price_time) )
    total_portfolio_val = total_portfolio_val[-disp_hist:]
    total_portfolio_val /= total_portfolio_val[0]/prices[-hist]

#    print( -Calculate_Score2(util_scaled) )

    shifted_trade_idx_series, shifted_portfolio_frac_precursor_series =\
        Shift_Trade_Signal( trade_idx_series
                          , portfolio_frac_precursor_series
                          , assets_price_time
                          , hist
                          )
    buy_sell_series = [ "b" if portfolio_frac_precursor == {asset: 1.} else "s"
                        for portfolio_frac_precursor in shifted_portfolio_frac_precursor_series
                      ]

    assets_price_time = np.array(assets_price_time)
    assets_price_time -= assets_price_time[-1]
    assets_price_time /= 24.*60.*60.

    xlim = [ -np.mean( assets_price_time[1:] - assets_price_time[:-1] ) * disp_hist, 0 ]


    ####################################
    # Plot
    Plot_Backtest_Filter( assets_price_time[-disp_hist:]
                        , prices[-disp_hist:]
                        , fast[-disp_hist:]
                        , slow[-disp_hist:]
                        , diff_rel[-disp_hist:]
                        , shifted_trade_idx_series
                        , buy_sell_series
                        , total_portfolio_val[-disp_hist:]
                        , reference_util_scaled[-disp_hist:] if not reference_util_scaled is None else None
                        , util_scaled[-disp_hist:]
                        , title  = title
                        , xlim   = xlim
                        , xlabel = 'time [month]'
                        )


################################################################################
def Calculate_And_Plot_Backtest_For_Pars_Series( assets_price_time
                                               , assets_price_record
                                               , pars_time
                                               , pars_series
                                               , asset
                                               , reference_asset
                                               , trade_fee
                                               , portfolio_frac_tolerance = 0.03  # %3 percent
                                               , initial_value            = 1.
                                               , disp_hist                = 0
                                               , title                    = None
                                               , setup                    = Setup
                                               ):
    ####################################
    # Wrapper function
    portfolio_frac_precursor_series = []

    def f( strategy
         , pars # [N_fast, N_slow]
         , assets_price_record # {asset1: [p1, p2, ...], asset2: [], ...}
         , up_to_but_not_including_idx = None
         ):
        nonlocal portfolio_frac_precursor_series

        trade_idxs,\
        portfolio_frac_precursor_series,\
        _ = \
            Calculate_Portfolio_Frac_Precursor_Series( strategy
                                                     , pars
                                                     , assets_price_record
                                                     , up_to_but_not_including_idx
                                                     , asset
                                                     , reference_asset
                                                     , setup
                                                     )
        return trade_idxs, portfolio_frac_precursor_series


    ####################################
    # Actually calculate the portfolio value
    pars_strategies_series =\
        [ { Add_Suffix_To_Name( setup.strategy, asset ) : pars }
          for pars in pars_series
        ]

    portfolio_val, _trade_idx_series, initial_price_idx =\
        Calculate_Backtest( assets_price_time
                          , assets_price_record
                          , pars_time
                          , pars_strategies_series
                          , f
                          , reference_asset
                          , trade_fee
                          , portfolio_frac_tolerance
                          , initial_value
                          )


    ####################################
    # Plot
    par_dict_strategies_series =\
        [ Convert_Pars_Strategies_To_Par_Dict_Strategies( setup.par_defs, pars_strategies )
          for pars_strategies in pars_strategies_series
        ]

    Plot_Portfolio_Value_Backtest_And_Par_Dict_Strategies_Series( assets_price_time
                                                                , portfolio_val
                                                                , pars_time
                                                                , par_dict_strategies_series
                                                                , initial_value
                                                                , initial_price_idx
                                                                , disp_hist
                                                                )
