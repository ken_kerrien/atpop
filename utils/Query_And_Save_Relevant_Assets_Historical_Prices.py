from atpop.lib.exchanger.price              import Exchanger_Price
from atpop.lib.exchanger.exchange.utils     import timeframes
week_to_samp = lambda x, samp_sec : int(x*7*24*60*60./samp_sec)


################################################################################
class Exchange_Setup():
    name                      = 'hitbtc'
    # We need at least 10k trade vol in 5m on average to consider an asset
    # refvol/day: refvol/5m * (12 * 5m)/h * (24 * h)/day
    th_val_in_ref             = 10e3 * 12 * 24
    OHLCV_page_sample_limit   = 1000


################################################################################
class Asset_Price_Setup():
    reference_asset      = 'USDT'
    timeframe            = '5m'
    # Time series total length, anything before that don't matter
    history              = week_to_samp( 30.
                                       , timeframes[timeframe]
                                       )
    # Disregard any asset for which the most recent price is this many seconds old
#    max_recent_price_age = 60*60
    max_recent_price_age = 60*60*1e6


################################################################################
if __name__ == '__main__':
    ex = Exchanger_Price( Exchange_Setup       = Exchange_Setup
                        , Asset_Price_Setup    = Asset_Price_Setup
                        , STATE_ARTIFACTS_PATH = '.'
                        , DEBUG_ARTIFACTS_PATH = '.'
                        )

    ex._Update_Price_Data()

    ex.price_data.To_CSV( 'data/asset_historical_prices_test2.csv' )
