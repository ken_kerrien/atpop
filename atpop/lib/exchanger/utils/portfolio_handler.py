from .names import EXCHANGE_RATE_NAME
from ccxt import InsufficientFunds


################################################################################
class No_Exchange_Rate_Available(Exception):
    def __init__(self, message):    
        # Call the base class constructor with the parameters it needs
        super(No_Exchange_Rate_Available, self).__init__(message)


################################################################################
def Add_Portfolios( portfolio1
                  , portfolio2
                  ):
    portfolio = dict(portfolio1)

    for asset, val in portfolio2.items():
        if not asset in portfolio:
            portfolio[asset] = 0
        portfolio[asset] += val

    return portfolio  


################################################################################
def Exlude_Assets_From_Portfolio( portfolio
                                , exclude_assets
                                ):
    portfolio_safe     = {}
    portfolio_excluded = {}

    for asset, val in portfolio.items():
        if asset not in exclude_assets:
            portfolio_safe[asset] = val
            continue

        if exclude_assets[asset] <= 0: # TODO: Log this as a warning maybe?
            continue

        portfolio_excluded[asset] = min( val, exclude_assets[asset] )

        val -= exclude_assets[asset]

        if val <= 0:
            continue

        portfolio_safe[asset] = val

    return portfolio_safe, portfolio_excluded


################################################################################
def Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( portfolio
                                                          , reference_asset
                                                          , asset_pairs
                                                          , qn, bn
                                                          ):
    def Get_Exchange_Rate( base, quote ):
        for asset_pair in asset_pairs:
            if asset_pair[bn] == base and asset_pair[qn] == quote:
                return asset_pair[EXCHANGE_RATE_NAME]

    portfolio_conv = {}

    for asset, val in portfolio.items():
        if asset == reference_asset:
            portfolio_conv[asset] = val
            continue

        exchange_rate = Get_Exchange_Rate( asset, reference_asset )
        if not exchange_rate is None:
            portfolio_conv[asset] = val * exchange_rate
            continue

        exchange_rate = Get_Exchange_Rate( reference_asset, asset )
        if not exchange_rate is None:
            portfolio_conv[asset] = val / exchange_rate
            continue

        raise No_Exchange_Rate_Available("No exchange rate available for asset %s"%asset)

    return portfolio_conv


################################################################################
def Check_Portfolio_Frac_Target_Within_Tolerance( p_frac_target
                                                , p_frac
                                                , frac_tolerance
                                                ):
#    assert Portfolio_Sums_To_One( p_frac_target )
#    assert Portfolio_Sums_To_One( p_frac )

    for asset, frac in p_frac.items():
        frac_target = 0.
        if asset in p_frac_target:
            frac_target = p_frac_target[asset]

        if frac > 0 and frac_target == 0:
            return False

        if np.abs( frac_target - frac ) > frac_tolerance:
            return False

    return True


################################################################################
from .portfolio_rebalance_math.utils  import Generate_Asset_Vector,\
                                             Generate_Conversion_Vector

import time
import numpy as np


################################################################################
def Adjust_Simulated_Portfolio( portfolio
                              , symbol
                              , amount_in_base
                              , buy_base_not_sell
                              , res
                              , reference_asset
                              , asset_pairs
                              , save_debug_data = False
                              , debug_path      = '.'
                              , qn = "qn"
                              , bn = "bn"
                              , fn = "fn"
                              , tn = "tn"
                              ):
    base, quote = symbol.split('/')

    if buy_base_not_sell:
        asset_to_sell = quote
    else:
        asset_to_sell = base

    if not asset_to_sell in portfolio:
        raise InsufficientFunds(f"{'Buy' if buy_base_not_sell else 'Sell'} {amount_in_base} {base} needs {asset_to_sell}, but it's not in the portfolio")


    # Find the index of the flow that this order came from
    for idx, flow_asset_pair in enumerate( res.flow_asset_pairs ):
        if flow_asset_pair[bn] != base:
            continue
        if flow_asset_pair[qn] != quote:
            continue

        if buy_base_not_sell and flow_asset_pair[fn] == flow_asset_pair[qn] and flow_asset_pair[tn] == flow_asset_pair[bn]:
            break

        if not buy_base_not_sell and flow_asset_pair[fn] == flow_asset_pair[bn] and flow_asset_pair[tn] == flow_asset_pair[qn]:
            break

    amount_in_ref = \
      amount_in_base * Generate_Conversion_Vector( [ base ]
                                                 , reference_asset
                                                 , asset_pairs
                                                 , qn, bn
                                                 )[0]

    actual_amount_in_ref = amount_in_ref / res.gain_to_base_array[ idx ]

    portfolio_in_ref = \
      Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( portfolio
                                                            , reference_asset
                                                            , asset_pairs
                                                            , qn, bn
                                                            )
    
    if portfolio_in_ref[ asset_to_sell ] < actual_amount_in_ref:
        raise InsufficientFunds( f"{'Buy' if buy_base_not_sell else 'Sell'} {amount_in_base} {base} needs {asset_to_sell} worth {actual_amount_in_ref} {reference_asset}, but we only have {portfolio[asset_to_sell]} {asset_to_sell} worth {portfolio_in_ref[asset_to_sell]} {reference_asset}" )


    flow_array        = np.zeros( len( res.conn_matrix[0] ) )
    flow_array[ idx ] = actual_amount_in_ref

    portfolio_in_ref_vec = \
      Generate_Asset_Vector( portfolio_in_ref
                           , res.ordered_asset_names
                           )

    portfolio_res_in_ref_vec = portfolio_in_ref_vec - np.inner(res.conn_matrix, flow_array)

    portfolio_res_vec = \
      portfolio_res_in_ref_vec / Generate_Conversion_Vector( res.ordered_asset_names
                                                           , reference_asset
                                                           , asset_pairs
                                                           , qn, bn
                                                           )

    portfolio_res = { key: val for key, val, val_in_ref
                      in zip( res.ordered_asset_names
                            , portfolio_res_vec
                            , portfolio_res_in_ref_vec
                            )
                      if val_in_ref > 1e-2
                    }


    #################################
    if save_debug_data:
        portfolio_in_ref = \
            Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( portfolio
                                                                  , reference_asset
                                                                  , asset_pairs
                                                                  , qn, bn
                                                                  )

        portfolio_total_in_ref = sum( portfolio_in_ref.values() )

        portfolio_res_in_ref = \
            Convert_Portfolio_To_Reference_Asset_Using_Asset_Pairs( portfolio_res
                                                                  , reference_asset
                                                                  , asset_pairs
                                                                  , qn, bn
                                                                  )

        portfolio_res_total_in_ref = sum( portfolio_res_in_ref.values() )

        from os       import path
        from datetime import datetime

        with open( path.join( debug_path, 'adjust_simulated_portfolio.log' )
                 , 'a'
                 ) as f:
            def log_local_var(var_name, vars_dict=locals()):
                f.write("#" + var_name + " = \\\n" )
                f.write( str( vars_dict[var_name] ) + "\n" )

            f.write("#"*20 + "\n")
            f.write( str(                       time.time() ) + "\n" )
            f.write( str(datetime.fromtimestamp(time.time())) + "\n" )
            log_local_var("portfolio")
            log_local_var("portfolio_in_ref")
            log_local_var("portfolio_total_in_ref")
            log_local_var("symbol")
            log_local_var("amount_in_base")
            log_local_var("buy_base_not_sell")
            for k,v in res.__dict__.items():
                f.write("#res." + k + " = \\\n" )
                f.write( str( v ) + "\n" )
            log_local_var("reference_asset")
            log_local_var("asset_pairs")
            log_local_var("portfolio_res")
            log_local_var("portfolio_res_in_ref")
            log_local_var("portfolio_res_total_in_ref")

    return portfolio_res
