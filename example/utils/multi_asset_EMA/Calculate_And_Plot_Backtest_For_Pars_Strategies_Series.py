import sys, os
sys.path.append( os.path.join( os.path.dirname(__file__)
                             , r'..'
                             )
               )

from utils.common                                import *

from atpop.lib.exchanger.exchange.utils          import timeframes

from atpop.lib.data.price_data                   import Price_Data
from atpop.lib.data.opt_par_data                 import Opt_Par_Data ,\
                                                        Opt_Par_Setup

from lib.trade_strategy.multi_asset_EMA.setup    import Setup
from lib.trade_strategy.multi_asset_EMA.backtest import Calculate_And_Plot_Backtest_For_Pars_Strategies_Series
from lib.trade_strategy.par_defs                 import Convert_Par_Dict_Strategies_To_Pars_Strategies

from services.config                             import Asset_Price_Setup ,\
                                                        Opt_Setup


################################################################################
if __name__ == '__main__':
    opt_par_data = Opt_Par_Data( path = STATE_ARTIFACTS_PATH  )
    opt_par_data.Read_CSV()

    price_data = Price_Data( path = STATE_ARTIFACTS_PATH )
    price_data.Read_CSV()

    par_dict_strategies_series, time_series =\
        opt_par_data.Get_Par_Dict_Strategies_Series_And_Time_Series( )

    pars_strategies_series = [ Convert_Par_Dict_Strategies_To_Pars_Strategies( Setup.par_defs, par_dict_strategies )
                               for par_dict_strategies in par_dict_strategies_series
                             ]

    assets_price_time,\
    assets_price_record =\
        price_data.Get_Prices_For_Assets_On_Same_Time_Scale( timeframes[Asset_Price_Setup.timeframe]
                                                           , history = Asset_Price_Setup.history
                                                           )

    Calculate_And_Plot_Backtest_For_Pars_Strategies_Series( assets_price_time
                                                          , assets_price_record
                                                          , time_series
                                                          , pars_strategies_series
                                                          , "reference"
                                                          , Opt_Setup.trade_fee
                                                          , portfolio_frac_tolerance = 0.03  # %3 percent
#                                                          , initial_value            = 1.
                                                          , disp_hist                = int(2*Opt_Setup.half_life)
                                                          )
