import logging
logger = logging.getLogger("__main__")

def log_exception_info(e):
    logger.info( str(e.__class__) + ' : ' + str(e) )

def log_exception_error(e):
    logger.error( str(e.__class__) + ' : ' + str(e) )

def log_exception_error_with_backtrace(e):
#    logger.error( str( sys.exc_info() ) )
    logger.error( 'Exception'
                , exc_info = True
                )
