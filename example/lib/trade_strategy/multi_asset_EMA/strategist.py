from .setup           import Setup
from .name_mangling   import Get_Name_Suffix, Add_Suffix_To_Name
from .compute         import Consolidate_Single_Asset_Portfolio_Frac_Precursor_Strategies

from ..EMA.strategist import Strategist as Filter_Sub_Strategist
from ..EMA.setup      import Setup as Filter_Sub_Setup


################################################################################
class Strategist():
    ####################################
    def __init__( self
                , price_data
                , opt_par_data
                , history
                , timeframe_sec
                , max_price_age
                , reference_asset
                , setup          = Setup
                , sub_strategist = Filter_Sub_Strategist
                , sub_setup      = Filter_Sub_Setup
                ):

        self.price_data      = price_data
        self.opt_par_data    = opt_par_data

        self.history         = int( history )
        self.timeframe_sec   = timeframe_sec

        self.max_price_age   = max_price_age

        self.reference_asset = reference_asset

        self.setup           = setup

        self.sub_strategist  = sub_strategist
        self.sub_setup       = sub_setup


    ####################################
    def Get_Portfolio_Frac_Precursor_Strategies( self ):
        # Get asset strategies from optimal parameters
        par_dict_strategies, _price_time_strategies =\
            self.opt_par_data.Get_Latest_Par_Dict_Strategies_And_Price_Time_Strategies( self.setup.strategy )

        if par_dict_strategies is None or not par_dict_strategies:
            return

        # Get asset strategies from prices
        # Weed out the low volume entries and the old ones
        price_assets = self.price_data.Get_Relevant_Assets( self.max_price_age )

        if not price_assets:
            return

        # Take intersection and iterate through it
        portfolio_frac_prec_strategies = {}

        opt_par_assets = [ Get_Name_Suffix( strategy, self.setup.strategy )
                           for strategy in par_dict_strategies.keys()
                         ]

        for asset in set(price_assets).intersection(opt_par_assets):
            fss = self.sub_setup()
            fss.strategy = self.setup.strategy
            fss.asset    = asset

            sub_trader = self.sub_strategist( self.price_data
                                            , self.opt_par_data
                                            , self.history
                                            , self.timeframe_sec
                                            , max_price_age = self.max_price_age
                                            , setup   = fss
                                            )
            sub_portfolio_frac_prec_strategies = sub_trader.Get_Portfolio_Frac_Precursor_Strategies()

            if sub_portfolio_frac_prec_strategies is None:
                continue
            # This means that either the optimal params or
            # the price is not available for a given asset.
            # This can happen at a fresh startup or when an
            # asset goes out of relevance by not being
            # traded enough.
            # Generally speaking, we want a precursor for
            # all assets.

            strategy = Add_Suffix_To_Name( self.setup.strategy
                                         , asset
                                         )

            portfolio_frac_prec_strategies[ strategy ] =\
                sub_portfolio_frac_prec_strategies[ strategy ]

        if not portfolio_frac_prec_strategies:
            return

        # It is possible that all precursor values are neutral
        # That is still valuable information, also it's
        # possible that all assets are neutral, but there are
        # less assets now than before!

        portfolio_frac_prec_strategies =\
            Consolidate_Single_Asset_Portfolio_Frac_Precursor_Strategies( portfolio_frac_prec_strategies
                                                                        , self.setup.strategy
                                                                        , self.reference_asset
                                                                        )

        # It is implicitly understood that whatever is not in the portfolio has to be zero
        return portfolio_frac_prec_strategies
