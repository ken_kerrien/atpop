"""
This service watches the trade algorithm parameters and the historical price
data. If any of those changes, it recalculates the optimal portfolio,
and writes that to the portfolio target file.
"""

# Setup logging
from ..lib.log_func import logger
from ..lib.log_func import log_exception_info
from ..lib.log_func import log_exception_error


########################################
from ..lib.wait_for_objects            import Wait_For_Files
from ..lib.exchanger.exchange.utils    import timeframes

from ..lib.data.portfolio_frac_data    import Portfolio_Frac_Precursor_Data
from ..lib.data.price_data             import Price_Data
from ..lib.data.opt_par_data           import Opt_Par_Data


################################################################################
class Trader( ):
    ############################################################################
    def __init__( self
                , Asset_Price_Setup
                , STATE_ARTIFACTS_PATH
                , DEBUG_ARTIFACTS_PATH
                , Trade_Strategy
                ):
        ###################################
        # Input
        self.price_data = \
            Price_Data( path = STATE_ARTIFACTS_PATH )

        self.opt_par_data = \
            Opt_Par_Data( path = STATE_ARTIFACTS_PATH )

        ###################################
        # Output
        self.portfolio_frac_precursor_data = \
            Portfolio_Frac_Precursor_Data( path = STATE_ARTIFACTS_PATH )
        try:
            self.portfolio_frac_precursor_data.Read_CSV()
        except IOError as e:
            log_exception_info(e)

        ###################################
        self.trade_strategist = Trade_Strategy( self.price_data
                                              , self.opt_par_data
                                              , Asset_Price_Setup.history
                                              , timeframes[Asset_Price_Setup.timeframe]
                                              , Asset_Price_Setup.max_recent_price_age
                                              , Asset_Price_Setup.reference_asset
                                              , DEBUG_ARTIFACTS_PATH = DEBUG_ARTIFACTS_PATH
                                              )


    ############################################################################
    def Wait_For_Event(self):
        wo = Wait_For_Files( [ self.price_data.Get_CSV_File_Path()   # historical prices
                             , self.opt_par_data.Get_CSV_File_Path() # optimal parameters for the trade method
                             ]
                           )
        wo.Wait( self._Trade )


    ############################################################################
    def _Trade(self, mod_file_paths):
        ##############################
        # Read in optimized variables
        opt_par_changed = False
        if self.opt_par_data.Get_CSV_File_Path() in mod_file_paths:
            logger.info( "Change detected with %s"
                       , self.opt_par_data.Get_CSV_File_Path()
                       )

            try:
                self.opt_par_data.Read_CSV()
                opt_par_changed = True
            except IOError as e:
                log_exception_info(e)
    #            return

            logger.info( "Trade method optimized parameters updated" )


        ##############################
        # Read in historical price data
        price_changed = False
        if self.price_data.Get_CSV_File_Path() in mod_file_paths:
            logger.info( "Change detected with %s"
                       , self.price_data.Get_CSV_File_Path()
                       )

            try:
                self.price_data.Read_CSV()
                price_changed = True
            except IOError as e:
                log_exception_info(e)
    #            return

            logger.info( "Historical price data updated" )


        ##############################
        # Check that inputs changed
        if self.opt_par_data.Is_Empty() or self.price_data.Is_Empty():
            logger.info( "We don't have all the data yet" )
            return

        if not opt_par_changed and not price_changed:
            logger.info( "Input data didn't actually change" )
            return

        ##############################
        # Get new portfolio target
        new_portfolio_frac_strategies = self.trade_strategist.Get_Portfolio_Frac_Precursor_Strategies()
        if new_portfolio_frac_strategies is None:
            logger.info( "Couldn't calculate new portfolio fraction strategies" )
            return

        logger.info( "New portfolio fraction strategies calculated" )

        ##############################
        # Write new desired portfolio
        if not self.portfolio_frac_precursor_data.Add_Entries_If_Different( new_portfolio_frac_strategies ):
            logger.info( "New portfolio fraction strategies same as previous" )
            return

        try:
            self.portfolio_frac_precursor_data.To_CSV()
            logger.info( "New portfolio fraction strategies written to %s"
                       , self.portfolio_frac_precursor_data.Get_CSV_File_Path()
                       )
        except IOError as e:
            log_exception_error(e)
