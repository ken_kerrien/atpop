from collections import OrderedDict


################################################################################
class Setup:
    strategy = "EMA"
    asset    = "BTC"

    par_defs = \
        OrderedDict( [ ( "N_fast", ( 1., 6048*3. ) )
                     , ( "N_slow", ( 1., 6048*3. ) )
                     ]
                   )

#    N_fast_min_min        = 5.
#    N_fast_max_half_life  = 3.
#    N_slow_min_min        = 5.
#    N_slow_max_half_life  = 3.
