#!/usr/bin/env python3
from setuptools import setup

setup( name             = 'atpop'
     , version          = '2.0'
     , description      = 'Automated Trade & Portfolio Optimizing Platform'
     , author           = 'Ken Kerrien'
     , author_email     = 'ken.kerrien@gmail.com'
     , packages         = ['atpop']
     , python_requires  = '>=3'
     , install_requires = [ 'numpy>=1.16.1'
                          , 'ccxt>=1.18.578'
                          , 'pandas>=0.22.0'
                          , 'scipy>=1.2.1'
                          , 'toposort>=1.5'
                          , 'portalocker>=1.4.0'
                          ]
     , license          = 'AFPL'
#     , url              = 'http://atpop.com/'
     )
