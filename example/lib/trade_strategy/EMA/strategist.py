from .compute         import Generate_Signals, Calculate_Portfolio_Frac_Precursor
from .setup           import Setup
from ..name_mangling  import Add_Suffix_To_Name 


################################################################################
class Strategist():
    ####################################
    def __init__( self
                , price_data
                , opt_par_data
                , history
                , timeframe_sec
                , max_price_age
                , reference_asset
                , setup         = Setup
                ):

        self.par_dict_strategies = None

        self.assets_price_record = None

        self.price_data          = price_data
        self.opt_par_data        = opt_par_data

        self.history             = int( history )
        self.timeframe_sec       = timeframe_sec

        self.max_price_age       = max_price_age

        self.reference_asset     = reference_asset

        self.setup               = setup


    ####################################
    def _Update_Params( self
                      , par_dict_strategies
                      ):
        if par_dict_strategies is None:
            return

        self.par_dict_strategies = par_dict_strategies


    ####################################
    def _Update_Prices( self
                      , assets_price_record
                      ):
        if assets_price_record is None:
            return

        self.assets_price_record = assets_price_record


    ####################################
    def _Update( self ):
        par_dict_strategies, _price_time_strategies =\
            self.opt_par_data.Get_Latest_Par_Dict_Strategies_And_Price_Time_Strategies( Add_Suffix_To_Name( self.setup.strategy
                                                                                                          , self.setup.asset
                                                                                                          )
                                                                                      )
        self._Update_Params( par_dict_strategies )

        # Get asset list from prices
        # Weed out the low volume entries and the old ones
        relevant_assets = self.price_data.Get_Relevant_Assets( self.max_price_age )
        if not relevant_assets:
            return True

        self._Update_Prices( self.price_data.Get_Prices_For_Assets_On_Same_Time_Scale( self.timeframe_sec
                                                                                     , history                 = self.history
                                                                                     , try_to_include_assets   = relevant_assets
                                                                                     , extend_to_max_stop_time = True
                                                                                     )[1]
                           )
        return self.assets_price_record is None or self.par_dict_strategies is None


    ####################################
    def Get_Portfolio_Frac_Precursor_Strategies( self ):
        if self._Update():
            return

        startegy = Add_Suffix_To_Name( self.setup.strategy, self.setup.asset )
        par_dict = self.par_dict_strategies[ startegy ]

        _fast, _slow, diff_rel = \
            Generate_Signals( self.assets_price_record[self.setup.asset]
                            , par_dict["N_fast"]
                            , par_dict["N_slow"]
                            )

        portfolio_frac_prec_strategies =\
            { startegy : Calculate_Portfolio_Frac_Precursor( diff_rel[-1]
                                                           , self.setup.asset
                                                           , self.reference_asset
                                                           )
            }

        return portfolio_frac_prec_strategies
