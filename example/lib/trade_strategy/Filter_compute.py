import numpy as np


################################################################################
def Calculate_Scaled_Diff( price_diff
                         , price
                         , half_life = np.nan
                         ):
    # Take the difference and normalize it by the price
    # Also, scale down by half_life
    price_diff = np.asarray(price_diff)
    price      = np.asarray(price)

    price_diff_relative_to_price = price_diff / price[:-1]

    if not np.isnan(half_life):
        price_diff_relative_to_price *= 2 ** ( np.arange( -len(price_diff_relative_to_price)+1, 1. ) / half_life )

    scaled_price_diff = price_diff_relative_to_price * price[:-1]

    return scaled_price_diff


################################################################################
def Calculate_Utility_Over_Series( prices
                                 , trade_idxs
                                 , portfolio_frac_precursor_series
                                 , asset
                                 , reference_asset
                                 , initial_value          = np.nan
                                 , initial_state_in_asset = False
                                 , trade_fee              = 0.001
                                 , half_life              = np.nan
                                 ):
#    assert not np.isnan(np.asarray(price).min())
    # This function calculates a utility price difference value, where 
    # if we own an asset and asset rises, then utility rises
    # if we own an asset and asset fall, then utility falls
    # if we don't own an asset and asset rises, then utility falls
    # if we don't own an asset and asset falls, then utility rises

    if np.isnan(initial_value):
        initial_value = prices[0]

    price_diff     = np.zeros( len(prices)-1 )

    reference      = initial_value
    state_in_asset = initial_state_in_asset

    idx = 0

    for ii in range( 1, len(prices) ):
        while idx < len(trade_idxs) and trade_idxs[idx] < ii:
            if portfolio_frac_precursor_series[idx][          asset] >= 1. \
               and                                                         \
               not state_in_asset:
                state_in_asset = True
                price_diff[ii-1] -= 2 * reference * trade_fee

            if portfolio_frac_precursor_series[idx][reference_asset] >= 1. \
               and                                                         \
               state_in_asset:
                state_in_asset = False
                reference = prices[ii-1] * (1. - trade_fee)
                price_diff[ii-1] -= 2 * prices[ii-1] * trade_fee

            idx += 1

        price_diff[ii-1] += ( prices[ii] - prices[ii-1] ) * (1. if state_in_asset else -1.)

    scaled_price_diff = Calculate_Scaled_Diff( price_diff
                                             , prices
                                             , half_life
                                             )

    # We return the scaled growth,
    # so we'll likely start around zero
    # and we can dip into negative in the end
    return np.cumsum( scaled_price_diff )


################################################################################
def Calculate_Score(price, asset_val):
    return np.mean( np.asarray(asset_val) / np.asarray(price) - 1. )


################################################################################
def Calculate_Score2(util):
    return util[-1]
