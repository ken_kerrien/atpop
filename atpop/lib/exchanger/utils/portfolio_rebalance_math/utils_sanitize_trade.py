import numpy as np
from math import floor, ceil


###############################################################################
def Calculate_Direct_Effects_Of_Flow_Adjustments( flow_adjustments
                                                , flows
                                                , C
                                                ):
  # The situation is simple:
  # 1. I have a reduction in flow(s) (due to min, max, or precision)
  # 2. That leads to a reduction in the destination asset(s)
  # 3. That leads to a reduction in all the flows originating from the destination asset(s)

  ##########################
  # Here's an example:
  ##########################
  #   +------> asset2
  #   |  f1      |
  # asset1       |f2
  #   ^          v
  #   +------- asset3
  #      f3
  #
  #   +------- asset2
  #   v  f4      ^
  # asset1       |f5
  #   |          |
  #   +------> asset3
  #      f6

  # KCL
  #                                              [ f1 ]
  # asset 1: [    1    0 -g*c | -g*c    0    1 ] [ f2 ]
  # asset 2: [ -g*c    1    0 |    1 -g*c    0 ] [ f3 ] = before - after
  # asset 3: [    0 -g*c    1 |    0    1 -g*c ] [ f4 ]
  #                                              [ f5 ]
  #                                              [ f6 ]

  ##########################
  # 1. A reduction in flow
  # 2. Leads to a reduction in desination asset
  ##########################
  # Let's decrease f2 by adding D, which is negative
  # KCL
  #                                              [ 0 ]
  # asset 1: [    1    0 -g*c | -g*c    0    1 ] [ D ]   [      0 ]
  # asset 2: [ -g*c    1    0 |    1 -g*c    0 ] [ 0 ] = [      D ] = before - after
  # asset 3: [    0 -g*c    1 |    0    1 -g*c ] [ 0 ]   [ -g*c*D ]
  #                                              [ 0 ]
  #                                              [ 0 ]

  # Flipping the signs
  # [     0 ]
  # [    -D ] = after - before
  # [ g*c*D ]

  # Remember, D is negative, so there is a reduction in asset 3,
  # so everything originating from asset 3 has to be reduced as well.
  # In fact, we are only interested in the reduction, right?
  # [     0 ]
  # [     0 ]
  # [ g*c*D ]


  ##########################
  # 3. Leads to a reduction in all the flows originating from the destination asset(s)
  ##########################
  # Let's figure out all the outward flows
  # KCL
  #                                  [ f1 ]
  # asset 1: [  1  0  0 |  0  0  1 ] [ f2 ]   [ f1+f6 ]
  # asset 2: [  0  1  0 |  1  0  0 ] [ f3 ] = [ f2+f4 ]
  # asset 3: [  0  0  1 |  0  1  0 ] [ f4 ]   [ f3+f5 ]
  #                                  [ f5 ]
  #                                  [ f6 ]

  # Remember all outgoing flows are measured in the origin asset

  # Let's scale all outward flows by the total of outwards flows for the same origin
  # [ 1 0 0 ]                   [ 1/(f1 + f6) ]
  # [ 0 1 0 ] [ 1/(f1 + f6) ]   [ 1/(f2 + f4) ]
  # [ 0 0 1 ] [ 1/(f2 + f4) ] = [ 1/(f3 + f5) ]
  # [ 0 1 0 ] [ 1/(f3 + f5) ]   [ 1/(f2 + f4) ]
  # [ 0 0 1 ]                   [ 1/(f3 + f5) ]
  # [ 1 0 0 ]                   [ 1/(f1 + f6) ]
  #
  # Or rather
  # [ 1 0 0 ]                                           [ 1/(f1 + f6)           0           0 ]
  # [ 0 1 0 ] [ 1/(f1 + f6)           0           0 ]   [           0 1/(f2 + f4)           0 ]
  # [ 0 0 1 ] [           0 1/(f2 + f4)           0 ] = [           0           0 1/(f3 + f5) ]
  # [ 0 1 0 ] [           0           0 1/(f3 + f5) ]   [           0 1/(f2 + f4)           0 ]
  # [ 0 0 1 ]                                           [           0           0 1/(f3 + f5) ]
  # [ 1 0 0 ]                                           [ 1/(f1 + f6)           0           0 ]
  #
  # Hence
  # [f1               ] [ 1 0 0 ]                                           [ f1/(f1 + f6)            0            0 ]
  # [   f2            ] [ 0 1 0 ] [ 1/(f1 + f6)           0           0 ]   [            0 f2/(f2 + f4)            0 ]
  # [      f3         ] [ 0 0 1 ] [           0 1/(f2 + f4)           0 ] = [            0            0 f3/(f3 + f5) ]
  # [         f4      ] [ 0 1 0 ] [           0           0 1/(f3 + f5) ]   [            0 f4/(f2 + f4)            0 ]
  # [            f5   ] [ 0 0 1 ]                                           [            0            0 f5/(f3 + f5) ]
  # [               f6] [ 1 0 0 ]                                           [ f6/(f1 + f6)            0            0 ]
  #
  # Cool. Now let's take into account the decrease in asset.
  # [f1               ] [ 1 0 0 ]                                                     [                    0 ]
  # [   f2            ] [ 0 1 0 ] [ 1/(f1 + f6)           0           0 ] [     0 ]   [                    0 ]
  # [      f3         ] [ 0 0 1 ] [           0 1/(f2 + f4)           0 ] [     0 ] = [ g*c*D * f3/(f3 + f5) ]
  # [         f4      ] [ 0 1 0 ] [           0           0 1/(f3 + f5) ] [ g*c*D ]   [                    0 ]
  # [            f5   ] [ 0 0 1 ]                                                     [ g*c*D * f5/(f3 + f5) ]
  # [               f6] [ 1 0 0 ]                                                     [                    0 ]
  #

  # And that's it!

  #######################################################
  ##########################
  # Calculate the change in all assets
  # Negative flow means
  #      origin asset increased : -C*f = after - before -> positive
  # destination asset decreased : -C*f = after - before -> negative
  # We are only interested in cases, where the destination asset decreased
  asset_deltas = -1 * np.inner(C, flow_adjustments)
  asset_deltas = np.minimum( asset_deltas
                           , 0
                           )
  #print ordered_asset_names
  #print a

  ##########################
  # Connectivity matrix with only outward flows
  # All outgoing flows are positive
  CO = np.maximum( C, 0 )

  ##########################
  # Total outward flows
  total_out_flows = np.inner( CO, flows )
  np.seterr(divide = 'ignore') # HACK: There will be zero divisions here, it's OK
  inv_total_out_flows = 1./total_out_flows
  np.seterr(divide = 'warn')
  inv_total_out_flows[np.isnan(inv_total_out_flows)] = 0
  inv_total_out_flows[np.isinf(inv_total_out_flows)] = 0
  #print b

  return np.inner( np.inner( np.diag(flows)
                           , CO
                           )
                 , np.inner( np.diag(inv_total_out_flows)
                           , asset_deltas
                           )
                 )


################################################################################
def Sanitize_Amount( amount
                   , precision
                   , min_val
                   , max_val
                   ):
#    assert isinstance(precision, int), "Precision must be an integer giving decimal places"

#    print amount
#    print precision
#    print min_val
#    print max_val
#    print "####"
  if max_val is not None and amount > max_val:
    amount =  max_val

  # Ensure proper resolution for base currency
  # rounds down a float to the specified precision: round_down(12.3556, 3) -> 12.355

  round_down = lambda n, p: floor( n/p )*p
#    round_down = lambda n, p: floor( n*(10.**p) )/(10.**p)
  # rounds up a float to the specified precision: round_up(12.3543, 3) -> 12.355
#    round_up   = lambda n, p: ceil( n*(10.**p) )/(10.**p)

#    print amount
  amount = round_down(amount, precision)
#    print amount

  if min_val is not None and amount < min_val:
    amount = 0

#   print amount
  return amount


###############################################################################
def Sanitize_Flow_At_IDX( res
                        , idx
                        ):
  # Take the next flow that needs adjustment
  adjusted_flow_base = Sanitize_Amount( res.adjusted_flow_array_base[ idx ]
                                      , res.precision_array[ idx ]
                                      , res.min_array[ idx ]
                                      , res.max_array[ idx ]
                                      )

  # We have adjusted a flow, but that in turn will affect all the flows
  # outgoing from its target asset
  flow_delta_base = adjusted_flow_base - res.adjusted_flow_array_base[ idx ]
  flow_delta = flow_delta_base / res.conversion_to_base_array[ idx ] / res.gain_to_base_array[ idx ]
  flow_delta_array = np.zeros( len(res.flows) )
  flow_delta_array[ idx ] = flow_delta

  flow_direct_effect_delta_array = \
    Calculate_Direct_Effects_Of_Flow_Adjustments( flow_delta_array
                                                , res.flows
                                                , res.conn_matrix
                                                )

  result = res.adjusted_flow_array_base + flow_direct_effect_delta_array * res.conversion_to_base_array * res.gain_to_base_array
  result[ idx ] = adjusted_flow_base

  return result
