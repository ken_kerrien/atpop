import sys, os
sys.path.append( os.path.join( os.path.dirname(__file__)
                             , r'../..'
                             )
               )

from utils.common                       import *

from atpop.lib.exchanger.exchange.utils import timeframes

from atpop.lib.data.price_data          import Price_Data
from atpop.lib.data.opt_par_data        import Opt_Par_Data

from lib.trade_strategy.EMA.param_opter import Param_Opter
from lib.trade_strategy.EMA.backtest    import Calculate_And_Plot_Backtest_With_Util_Signals
from lib.trade_strategy.EMA.setup       import Setup

from services.config                    import Asset_Price_Setup
from services.config                    import Opt_Setup


################################################################################
if __name__ == '__main__':
    basic_setup = Setup()
    basic_setup.asset = 'LTC'

    opt_par_data = Opt_Par_Data( path = STATE_ARTIFACTS_PATH )
    opt_par_data.Read_CSV()

    price_data = Price_Data( path = STATE_ARTIFACTS_PATH )
    price_data.Read_CSV()

    assets_price_time, assets_price_record =\
        price_data.Get_Prices_For_Assets_On_Same_Time_Scale( timeframes[Asset_Price_Setup.timeframe]
                                                           , history = Asset_Price_Setup.history
                                                           )

    opter = Param_Opter( price_data
                       , Asset_Price_Setup.history
                       , Opt_Setup
                       , timeframes[Asset_Price_Setup.timeframe]
                       , opt_par_data
                       , basic_setup = basic_setup
                       )

    opter._Update()

    #[ N_fast
    #, N_slow
    #]

    pars = [ 100
           , 1000
           ]

    #pars = opter.Get_Optimal_Params()


    print("[ " , pars[0])
    print(", " , pars[1])
    print("]")

    Calculate_And_Plot_Backtest_With_Util_Signals( assets_price_time
                                                 , assets_price_record
                                                 , pars
                                                 , asset
                                                 , "reference"
                                                 , Opt_Setup.trade_fee
                                                 , Opt_Setup.half_life
                                                 #, disp_hist = int(2*opter.half_life)
                                                 , title     = asset
                                                 )
